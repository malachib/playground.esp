extern "C" {

#include <FreeRTOS.h>
#include <task.h>
#include <esp/uart.h>

void testTask(void* p);

}

extern "C" void user_init(void)
{
    //uart_set_baud(0, 115200);
    
    xTaskCreate(testTask, "testTask", 3000, NULL, 2, NULL);
}