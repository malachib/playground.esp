#ifdef CONFIG_BLINK_GPIO
#if defined(ESP32)
#define _HELPER(val) GPIO_NUM_##val
#define HELPER(val) _HELPER(val)
#define GPIO_LED    HELPER(CONFIG_BLINK_GPIO)
#else
#define GPIO_LED    CONFIG_BLINK_GPIO
#endif
#elif defined(ESP_PLATFORM) && defined(ESP32)
#define GPIO_LED    GPIO_NUM_2
#else
#define GPIO_LED    2
#endif

