#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "blink.h"

#include <frab/gpio.h>

namespace frab = framework_abstraction;


#ifndef CONFIG_BLINK_DELAY
#define CONFIG_BLINK_DELAY 500
#endif

extern "C" void blinkenTask(void *pvParameters)
{
    frab::layer1::digital_out<GPIO_LED> led;

    while(1) {
        led.write(1);
        vTaskDelay(CONFIG_BLINK_DELAY / portTICK_PERIOD_MS);
        led.write(0);
        vTaskDelay(CONFIG_BLINK_DELAY / portTICK_PERIOD_MS);
    }
}

