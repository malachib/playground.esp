#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "lwip/sockets.h"
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "multicast_config.h"

/******************************************************************************
 * FunctionName : igmp_send_task
 * Description  : send multicast packet task
 * Parameters   : none
 * Returns      : none
 *******************************************************************************/
void igmp_send_task(void *pvParameters)
{
    struct sockaddr_in addr;
    int fd, cnt;
    struct ip_mreq mreq;
    char *message = "Hello, World!\n";
    /* create what looks like an ordinary UDP socket */
    if ((fd=socket(AF_INET,SOCK_DGRAM,0)) < 0) {
        printf("socket failed\n");
        vTaskDelete(NULL);
    }
    /* set up destination address */
        memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(HELLO_GROUP);
    addr.sin_port = htons(HELLO_PORT);
    /* now just sendto() our destination! */
    while (1) {
        if (sendto(fd,message, strlen(message), 0, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
            printf("sendto failed\n");
            vTaskDelete(NULL);
        }
        vTaskDelay(10);
    }
}

/******************************************************************************
 * FunctionName : igmp_recv_task
 * Description  : recv multicast packet task
 * Parameters   : none
 * Returns      : none
 *******************************************************************************/
void igmp_recv_task(void *pvParameters)
{
    struct sockaddr_in addr;
    int fd, nbytes, addrlen;
    struct ip_mreq mreq;
    char msgbuf[MSGBUFSIZE] = { 0 };

    if ((fd=socket(AF_INET,SOCK_DGRAM,0)) < 0)
    {
        printf("socket error\n");
        vTaskDelete(NULL);
    }

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(HELLO_PORT);

    if (bind(fd,(struct sockaddr *) &addr,sizeof(addr)) < 0)
    {
        printf("bind failed\n");
        vTaskDelete(NULL);
    }

    mreq.imr_multiaddr.s_addr = inet_addr(HELLO_GROUP);
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(fd,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) < 0)
    {
        printf("setsockopt failed\n");
        vTaskDelete(NULL);
    }

    while (1) {
        //ssize_t recvfrom(int s, void *buf, size_t len, int flags, struct sockaddr *from, socklen_t *fromlen);
        addrlen = sizeof(addr);
        if ((nbytes=recvfrom(fd, msgbuf, MSGBUFSIZE, 0, (struct sockaddr *) &addr, (socklen_t *)&addrlen)) < 0)
        {
            printf("recvfrom failed\n");
            vTaskDelete(NULL);
        }
        printf("recv massage:\n");
        printf(msgbuf);
        memset(msgbuf, 0, MSGBUFSIZE);
    }
}