#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include <chrono>

extern "C" void testTask(void* p)
{
    // neither of these link
    //auto start = std::chrono::system_clock::now();
    //auto start = std::chrono::steady_clock::now();

    // this compiles though, because headers are there & linking
    // not needed.  So probably we can make our own 'steady_clock'
    auto start = std::chrono::milliseconds(1000);
    auto start2 = std::chrono::seconds(5);

    bool excess = start2 > start;

    for(;;)
    {
        taskYIELD();
    }
}