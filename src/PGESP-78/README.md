# 1. RESERVED

# 2. Infrastructure

## POC1

Works pretty well!

## POC2

Looks like FFT may be required for resampling [1]
Smells like an important source comes from a 1986 article by J.W. Adams [1.1]

I'm not above stealing ADF's algorithm [1.3].

Some good reading appears to be at [1.2] [1.4] [1.5] [1.6]

## POC3

# 3. Observations

## 3.1. i2s_channel_write

Although this guy does output to a DMA buffer,

1. Internally does a regular memcpy into the DMA buffer of choice (I guess they want you to use callback
for true-blue DMA access)
2. If you write a buffer larger than internal DMA buffer size, not only does it block but it doesn't unblock quickly enough to do anything meaningful (odd)

#2 Not a HGUE deal but at higher frequency and word size, you get close to time slice intervals which is uncomfortable.

## 3.2. FFT

A breakdown of FFT seems approachable here [1.2]

# 4. Conclusions

# 5. Journal

## 08MAR25

poc1 works-ish but you can hear some phasing-ish sounds from sin wave here and there
poc2 resists us.  turns out really was buffer underrun

ESP-ADF `esp_resample_run` appears to be closed source [2.2] doesn't reveal a definition for the function.
Attempts to yank it in [2.2] directly fail since it's not truly a standalone component it seems

## 10MAR25

Interesting FFT reading [3]
Paydirt - esp-dsp has lots of relevant helpers in the IIR category.  See [4.1] and [4.2]
Also stumbled on `dsp_get_cpu_cycle_count` macro - I wonder who he wraps?

## 11MAR25

It's feeling like this code should actually live in a real library it's becoming so robust.

Waiting for two factors:

1. Inspiring client code to also be realized a bit more
2. poc1-poc3 reach a mininum viability

Neither of these are truly necessary, but I am struggling with availability questions:

1. nestled in embr or embr-gl?
2. open source?
3. its own embr-audio?

# References

1. https://stackoverflow.com/questions/69759128/how-to-resample-audio
    1. https://ieeexplore.ieee.org/document/1169161
    2. https://dsp.stackexchange.com/questions/2962/how-to-resample-audio-using-fft-or-dft
    3. https://github.com/espressif/esp-adf/blob/master/examples/audio_processing/pipeline_resample/README.md
    4. https://christianfloisand.wordpress.com/2012/12/05/audio-resampling-part-1/
    5. https://christianfloisand.wordpress.com/2013/01/28/audio-resampling-part-2/
    6. https://therationalaudiophile.wordpress.com/2018/02/22/how-to-re-sample-a-signal/
        1. https://www.dsprelated.com/freebooks/pasp/Windowed_Sinc_Interpolation.html
2. https://docs.espressif.com/projects/esp-adf/en/latest/index.html
    1. https://github.com/espressif/esp-adf
    2. https://github.com/espressif/esp-adf-libs/tree/4610cd794152ed4a6e4417e92385150a1e32ddeb
3. https://www.reddit.com/r/learnprogramming/comments/t7ux0e/how_do_you_actually_perform_an_fft_on_audio_c/
4. https://github.com/espressif/esp-dsp
    1. https://github.com/espressif/esp-dsp/tree/v1.5.2/applications/lyrat_board_app
    2. https://github.com/espressif/esp-dsp/blob/v1.5.2/examples/iir/main/dsps_iir_main.c
    3. https://docs.espressif.com/projects/esp-dsp/en/latest/esp32/esp-dsp-apis.html