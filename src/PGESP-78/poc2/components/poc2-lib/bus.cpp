#include <cstring>

#include <esp_log.h>
//#include <estd/chrono.h>
#include <estd/type_traits.h>

#include <embr/profiler.h>

#include <audio/config.h>

#include "audio/bus.h"
#include "audio/mix.h"
#include "audio/traits.h"

audio::mixer<8> bus;

static const char* TAG = "PGESP-78::poc2::bus";

template <typename Word>
void mix();

using clock_type = estd::chrono::esp_clock;
using time_point = typename clock_type::time_point;
using duration = typename clock_type::duration;

static void mixdown_task(void* arg)
{
    using traits = audio::channel_traits<PGESP_78_HZ>;
    using word_type = audio_word_type;

    auto tx_handle = (i2s_chan_handle_t) arg;

    constexpr unsigned buffer_sz = PGESP_78_BUFSIZE;
    constexpr unsigned buffer_byte_sz = buffer_sz * sizeof(word_type);
    constexpr unsigned timeout_ticks = traits::timeout_ticks;

    float send_volume = 1;

    auto buffer = (word_type*)heap_caps_malloc(buffer_byte_sz, MALLOC_CAP_INTERNAL);
    auto send_buffer = (word_type*)heap_caps_malloc(buffer_byte_sz, MALLOC_CAP_INTERNAL);

    assert(buffer);
    assert(send_buffer);

    ESP_LOGI(TAG, "mixdown_task: online buffer_sz=%u, time_slice_ms=%f, timeout_ticks=%u",
        buffer_sz, traits::time_slice_ms, timeout_ticks);

    for(;;)
    {
        // DEBT: Add memset https://github.com/malachi-iot/estdlib/issues/90
        std::memset(buffer, 0, buffer_byte_sz);
        std::memset(send_buffer, 0, buffer_byte_sz);

        embr::Profiler<clock_type> profiler;

        EventBits_t pending = bus.on_queued(timeout_ticks + 1, [&](int i)
        {
            const audio::bus_channel& channel = bus[i];
            auto in = (const word_type*) channel.buffer;

            if(in != nullptr)
            {
                //std::transform(to_mix, to_mix_end, buffer, [](word_type lhs,))
                // FIX: Need clipping and other post processing options

                // mix in this channel to bus buffer
                audio::add_with_vol(in, in + buffer_sz, channel.volume, buffer);

                // mix in this channel to dedicated send buffer
                audio::add_with_vol(in, in + buffer_sz, channel.send_volume, send_buffer);

                // DEBT: More strict checking of formats... maybe even compile time?
                if(channel.unit != nullptr)
                {
                    channel.unit->in_place(channel.buffer, buffer_sz);
                }
            }
        });

        // mix send channel back into main mix
        audio::add_with_vol(send_buffer, send_buffer + buffer_sz, send_volume, buffer);

        [[maybe_unused]]
        duration channels_acquired_d = profiler.mark();

        if(pending)
        {
            ESP_LOGW(TAG, "mixdown_task: buffer underrun - pending=0x%x", (int)pending);
        }

        size_t bytes_written;

        // FIX: Concerning we need such a high timeout here
        esp_err_t ret = i2s_channel_write(tx_handle,
            buffer, buffer_byte_sz, &bytes_written, timeout_ticks * 8);

        [[maybe_unused]]
        duration i2s_written_d = profiler.mark();

        if(ret != ESP_OK)
            ESP_LOGW(TAG, "mixdown_task: buffer overrun");
    }
}


void init_mixdown(i2s_chan_handle_t tx_handle)
{
    static_assert(std::is_pointer<i2s_chan_handle_t>::value, "");
    
    // Default affinity is CONFIG_ESP_MAIN_TASK_AFFINITY_CPU0, so put audio task on CPU1
    xTaskCreatePinnedToCore(mixdown_task, "audio mixdown", 4096, tx_handle, 7, nullptr, 1);
}

namespace audio::internal {

freertos_bus::freertos_bus() :
    queued_(xEventGroupCreateStatic(&storage1_)),
    dequeued_(xEventGroupCreateStatic(&storage2_))
{
    // Prime the pump, create a fake status that data was sent so that outside
    // channel producers don't wait
    mark_dequeued();
}

freertos_bus::~freertos_bus()
{
    // TODO: Delete event group
}

}