#pragma once

#include <driver/i2s_std.h>

#include "fwd.h"
#include "bus.h"

namespace audio {

template <class InIt, class OutIt>
inline void add(InIt in, InIt in_end, OutIt out)
{
    for(; in < in_end; ++in, ++out) *out += *in;
}

template <class InIt, class OutIt, class F>
inline void add(InIt in, InIt in_end, OutIt out, F&& f)
{
    for(; in < in_end; ++in, ++out) f(in, out);
}

template <class InIt, class OutIt, class Numeric>
inline void add_with_vol(InIt in, InIt in_end, Numeric vol, OutIt out)
{
    if(vol == 1)
        add(in, in_end, out);
    else if(vol > 0)
        for(; in < in_end; ++in, ++out) *out += *in * vol;
}


// DEBT: All mono through and through at this time
template <unsigned N>
class mixer : public bus<N>
{
    // destination format
    sample_formats format_ = SAMPLE_SYSTEM;
    channel main_;

public:
    constexpr sample_formats format() const
    {
        return format_;
    }
};

}

extern audio::mixer<8> bus;

void init_mixdown(i2s_chan_handle_t tx_handle);
