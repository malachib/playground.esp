#pragma once

#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>

#include <audio/config.h>

#include "channel.h"
#include "fwd.h"

namespace audio {

struct channel : embr::bus::channel
{
    float volume = 0;
    sample_formats format = SAMPLE_SYSTEM;
};

struct bus_channel : channel
{
    // For separate send channel
    float send_volume = 0;
};


struct channel_handle
{
    bus_channel* channel;
    int idx;
};

namespace internal {

// DEBT: This bus/feeder paradigm *really* feels like it belongs in embr,
// but can't quite quantify where or how to organize it just yet.  Have a
// look at DUKU project since we do something similar, but not with audio
class freertos_bus
{
protected:
    StaticEventGroup_t storage1_, storage2_;

    // queued = outside channel producers set this when their data is ready
    // dequeued = we signal to outside channel producers that their data was sent/consumed
    const EventGroupHandle_t queued_, dequeued_;

    freertos_bus();
    ~freertos_bus();

public:
// 
    void mark_queued(unsigned i) const
    {
        const EventBits_t bits = 1 << i;

        xEventGroupSetBits(queued_, bits);
    }

    // Mark a channel as dequeued, meaning this particular channel
    // is now free to queue up again
    void mark_dequeued(unsigned i) const
    {
        const EventBits_t bits = 1 << i;

        xEventGroupSetBits(dequeued_, bits);
    }

    // Mark all channels dequeued
    void mark_dequeued() const
    {
        xEventGroupSetBits(dequeued_, 0xFF);
    }

    // Wait for ANY to queue
    EventBits_t wait_queued(TickType_t timeout) const
    {
        constexpr EventBits_t bits = 0xFF;

        // Wait for any bit (OR), clear when found
        return xEventGroupWaitBits(queued_, bits, pdTRUE, pdFALSE, timeout);
    }

    // Wait for particular channel to dequeue
    bool wait_dequeued(unsigned i, TickType_t timeout) const
    {
        const EventBits_t bits = 1 << i;

        // Wait for specific bit, clear when found
        return xEventGroupWaitBits(dequeued_, bits, pdTRUE, pdTRUE, timeout) & bits;
    }
};

// Just as an idea
class spinwait_bus;

//using bus = freertos_bus;

}

template <unsigned N> //, class Channel = bus_channel>
class bus : public internal::freertos_bus
{
    using channel_type = bus_channel;

    // See https://freertos.org/Documentation/02-Kernel/04-API-references/12-Event-groups-or-flags/02-xEventGroupCreateStatic
#if configUSE_16_BIT_TICKS
    static_assert(N <= 8, "Maximum 8 channels");
#else
    static_assert(N <= 24, "Maximum 24 channels");
#endif

    channel_type channels_[N];

public:
    const channel_type* channels() const { return channels_; }

    const channel_type& operator[](int index) const
    {
        return channels_[index];
    }

    channel_type& operator[](int index)
    {
        return channels_[index];
    }

    EventBits_t eventbits_active_channels() const
    {
        EventBits_t active = 0;

        for(int i = 0; i < N; i++)
        {
            if(channels_[i].volume > 0)
            {
                active |= 1 << i;
            }
        }

        return active;
    }

    // Call functor when particular channels finally queue up
    template <class F>
    EventBits_t on_queued(int timeout, F&& f) const
    {
        EventBits_t active = eventbits_active_channels();
        int end_time = xTaskGetTickCount() + timeout;

        while(active && timeout >= 0)
        {
            EventBits_t which_dequeued = wait_queued(timeout);

            if(which_dequeued == 0) return active;

            active &= ~which_dequeued;

            for(int i = 0; i < N; i++)
            {
                if(which_dequeued & 1)
                {
                    f(i);

                    mark_dequeued(i);
                }

                which_dequeued >>= 1;
            }

            timeout = end_time - (int)xTaskGetTickCount();
        }

        return active;
    }
};

}

