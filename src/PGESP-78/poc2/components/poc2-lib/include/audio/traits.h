#pragma once

#include <audio/config.h>

namespace audio {

template <unsigned hz_, typename Word = audio_word_type, unsigned bufsz = CONFIG_I2S_BUFFER_SIZE>
struct channel_traits
{
    using word_type = Word;
    static constexpr unsigned hz = hz_;
    static constexpr unsigned buffer_sz = bufsz;
    static constexpr unsigned buffer_byte_sz = buffer_sz * sizeof(word_type);

    static constexpr float samples_per_second = static_cast<float>(hz) / buffer_sz;
    static constexpr float time_slice_ms = 1000 / samples_per_second;
    static constexpr float time_slice_us = 1000000 / samples_per_second;
    // DEBT: 10ms per tick, switch to use proper freertos macro
    static constexpr unsigned timeout_ticks = time_slice_ms / 10;
};


}
