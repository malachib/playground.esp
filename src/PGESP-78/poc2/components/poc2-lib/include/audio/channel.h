#pragma once

#include <audio/units/unit.h>

namespace embr::bus {

struct channel
{
    void* buffer;
    // whether to allow in place processing of "buffer" by unit, otherwise
    // a copy is expected
    bool in_place = true;

    embr::dsp::unit* unit = nullptr;
};

}
