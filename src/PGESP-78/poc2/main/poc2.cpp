#include <esp_log.h>

#include <esp_dsp.h>

#include <embr/profiler.h>
#include <embr/dsp/precalc.h>

#include <audio/bus.h>
#include <audio/i2s.h>
#include <audio/mix.h>
#include <audio/sine.h>
#include <audio/traits.h>


static const char* TAG = "PGESP-78::poc2";

// Guidance from https://github.com/espressif/esp-dsp/blob/v1.5.2/examples/iir/main/dsps_iir_main.c
//__attribute__((aligned(16)))
//static float d[CONFIG_I2S_BUFFER_SIZE];

static float coeffs_lpf[5];
static float w_lpf[5] = {0, 0};

using clock_type = estd::chrono::esp_clock;
using time_point = typename clock_type::time_point;
using duration = typename clock_type::duration;

#define ENABLE_FLOAT_CHANNEL 0

// Sine osc specifically
template <class Traits = audio::channel_traits<PGESP_78_HZ>>
class oscillator
{
    using traits = Traits;

protected:
    audio::osc_info osc_;

public:
    constexpr oscillator(audio::scalar_type freq) :
        osc_(freq)
    {

    }

    template <class It, class F>
    void gen(It out, F&& f)
    {
        audio::detail::gen_sine_precalc(osc_, traits::buffer_sz, out, std::forward<F>(f));
    }
};

// Sine monosynth specifically
// Combination of buffer, oscillator generator
// Has bus-friendly gen output method
template <class Traits = audio::channel_traits<PGESP_78_HZ>>
class monosynth : public oscillator<Traits>
{
    using base_type = oscillator<Traits>;
    using clock_type = estd::chrono::esp_clock;
    using traits = Traits;
    using word_type = typename traits::word_type;
    using base_type::gen;
    using base_type::osc_;

    word_type* buffer_;

public:
    monosynth(float freq) :
        base_type{freq},
        buffer_{(word_type*)heap_caps_malloc(traits::buffer_byte_sz, MALLOC_CAP_INTERNAL)}
    {

    }

    word_type* buffer() { return buffer_; }
    const word_type* buffer() const { return buffer_; }

    template <unsigned N>
    duration gen(audio::bus<N>& bus, int index, float vol)
    {
        assert(bus.wait_dequeued(index, traits::timeout_ticks));
        embr::Profiler<clock_type> profiler;
        audio::gen_sine_precalc(base_type::osc_, traits::buffer_sz, vol, buffer_);
        duration osc_d = profiler.mark();
        bus.mark_queued(index);
        return osc_d;
    }

    template <unsigned N>
    duration gen(audio::bus<N>& bus, int index)
    {
        assert(bus.wait_dequeued(index, traits::timeout_ticks));
        audio::bus_channel& channel = bus[index];
        embr::Profiler<clock_type> profiler;
        if(channel.format == audio::SAMPLE_FLOAT)
        {
            gen((float*)channel.buffer, [&](float v, float* out)
            {
                *out = v;
            });
        }
        else
        {
            using limits = std::numeric_limits<word_type>;

            gen((word_type*)channel.buffer, [&](float v, word_type* out)
            {
                *out = limits::max() * v;
            });
        }

        duration osc_d = profiler.mark();
        bus.mark_queued(index);
        return osc_d;
    }
};

void lpf(const float* in, float* out, float freq, float qFactor)
{
    // Calculate iir filter coefficients
    ESP_ERROR_CHECK(dsps_biquad_gen_lpf_f32(coeffs_lpf, freq, qFactor));
    // NOTE: Looks like we can call this over and over without above guy, just doing
    // this all inline temporarily.  Also, first parameter is input, 2nd is output
    ESP_ERROR_CHECK(dsps_biquad_f32(in, out, CONFIG_I2S_BUFFER_SIZE, coeffs_lpf, w_lpf));
}

extern "C" void app_main(void)
{
    using traits = audio::channel_traits<PGESP_78_HZ>;

    init_mixdown(init_i2s());

    // Guidance from https://github.com/espressif/esp-dsp/blob/v1.5.2/examples/iir/main/dsps_iir_main.c
    ESP_ERROR_CHECK(dsps_fft2r_init_fc32(NULL, CONFIG_DSP_MAX_FFT_SIZE));
    //ESP_ERROR_CHECK(dsps_d_gen_f32(d, CONFIG_I2S_BUFFER_SIZE, 0));

    audio::sin_precalc_opt();
    audio::sin_precalc();
    embr::dsp::init_sin_table();

    // https://en.wikipedia.org/wiki/Dial_tone
    audio::osc_info osc_a(440), osc_350(350);
    monosynth<traits> synth_350(350);

    using word_type = audio_word_type;

    constexpr unsigned timeout_ticks = traits::timeout_ticks;

    auto osc_a_buffer = (word_type*)heap_caps_malloc(traits::buffer_byte_sz, MALLOC_CAP_INTERNAL);
    auto osc_350_buffer = (word_type*)heap_caps_malloc(traits::buffer_byte_sz, MALLOC_CAP_INTERNAL);

    assert(osc_a_buffer);
    assert(osc_350_buffer);

    bus[0].buffer = osc_a_buffer;
    bus[0].volume = 1;
    //bus[1].buffer = osc_350_buffer;
    //bus[1].active = true;
    bus[2].buffer = synth_350.buffer();
    bus[2].volume = 0.1;
#if ENABLE_FLOAT_CHANNEL
    bus[3].format = audio::SAMPLE_FLOAT;
#endif

    embr::Profiler<clock_type> profiler;

    time_point last_reported = clock_type::now();
    unsigned counter = 0;

    for(;;)
    {
        assert(bus.wait_dequeued(0, timeout_ticks));
        profiler.mark();
        audio::gen_sine(osc_a, traits::buffer_sz, 0.1, osc_a_buffer);
        duration osc_a_d = profiler.mark();
        bus.mark_queued(0);

        // runs, but channel is inactive
        assert(bus.wait_dequeued(1, timeout_ticks));
        profiler.mark();
        audio::gen_sine_precalc(osc_350, traits::buffer_sz, 0.1, osc_350_buffer);
        duration osc_350_d = profiler.mark();
        bus.mark_queued(1);

#if ENABLE_FLOAT_CHANNEL
        duration synth_350_d = synth_350.gen(bus, 2);
#else
        // volume-oriented one only supports int buffer at this time
        duration synth_350_d = synth_350.gen(bus, 2, 1);
#endif

        //vTaskDelay(1);

        assert(osc_a_d.count() < traits::time_slice_us);
        assert(osc_350_d.count() < traits::time_slice_us);
        assert(osc_350_d.count() + osc_a_d.count() < traits::time_slice_us);

        const time_point now = clock_type::now();

        ++counter;

        if(now - last_reported > estd::chrono::seconds(2))
        {
            ESP_LOGI(TAG, "app_main: osc_a_d=%" PRIu64 "us, osc_350_d=%" PRIu64 "us, syn_350_d=%" PRIu64 "us",
                osc_a_d.count(),
                osc_350_d.count(),
                synth_350_d.count());
            // TODO
            counter = 0;
            last_reported = now;
        }
    }
}
