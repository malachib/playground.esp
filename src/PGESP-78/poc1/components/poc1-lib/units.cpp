#include <esp_log.h>
#include <esp_system.h>

#include <cassert>

#include <esp_dsp.h>

#include "audio/units/drc.h"
#include "audio/units/eq.h"

namespace embr::dsp {

void esp_dsp_unit::in_place(void* buf, unsigned sz)
{
    assert(format_ == audio::SAMPLE_FLOAT);

    float* inout = (float*) buf;

    ESP_ERROR_CHECK(dsps_biquad_f32(inout, inout, sz, coeffs_, w_));
}

}

namespace audio::units {

void eq::biquad_gen()
{
    ESP_ERROR_CHECK(dsps_biquad_gen_peakingEQ_f32(coeffs_, f_, q_factor_));
}

}
