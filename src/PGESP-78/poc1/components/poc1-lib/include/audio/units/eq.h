#pragma once

// https://stackoverflow.com/questions/36496191/audio-equalization-resolution
// https://www.reddit.com/r/esp32/comments/1bs8af1/esp32_audio_processing/

#include "unit.h"

namespace audio::units {

class eq : public embr::dsp::esp_dsp_unit
{
    float q_factor_;

    void biquad_gen();

public:
    // DEBT: This is "normalized sample frequency" [4.3] but really
    // feels like it wants to be plain old frequency in hz 
    void freq(float v)
    {
        f_ = v;
        biquad_gen();
    }

    void q_factor(float v)
    {
        q_factor_ = v;
        biquad_gen();
    }
};
    
}
