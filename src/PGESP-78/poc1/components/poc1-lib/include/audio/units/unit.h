#pragma once

#include "fwd.h"
#include "../config.h"
#include "../enum.h"

namespace embr::dsp {

class unit
{
protected:
    // DEBT: Removing coupling to audio
    using formats = audio::sample_formats;

    formats format_;

public:
    constexpr unit(formats format) :
        format_{format}
    {}

    // sz is in words
    virtual void in_place(void*, unsigned sz = CONFIG_I2S_BUFFER_SIZE) = 0;
};

class esp_dsp_unit : public unit
{
protected:
    float coeffs_[5];
    float w_[5] {};

    // "filter notch frequency in range of 0..0.5 (normalized to sample frequency)" [4.3]
    float f_;

    constexpr esp_dsp_unit(formats format = audio::SAMPLE_FLOAT) :
        unit(format)
    {}

public:
    void in_place(void*, unsigned sz) override;
};

}
