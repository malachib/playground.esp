#pragma once

#include "sdkconfig.h"

#if CONFIG_I2S_WORD8
#define I2S_DATA_BIT_WIDTH  I2S_DATA_BIT_WIDTH_8BIT
using audio_word_type = int8_t;
#elif CONFIG_I2S_WORD16
#define I2S_DATA_BIT_WIDTH  I2S_DATA_BIT_WIDTH_16BIT
using audio_word_type = int16_t;
#elif CONFIG_I2S_WORD32
#define I2S_DATA_BIT_WIDTH  I2S_DATA_BIT_WIDTH_32BIT
using audio_word_type = int32_t;
#else
// 24-bit support someday, but not today
#error Unsupported width
#endif

// Use embr vs our own precalc (temporary, eventually only embr one)
// At the moment embr one is way slow though, so disabling until embr perf code
// comes online
#define EMBR_SINTABLE       1
// Use precalc vs std::sin directly
#define PGESP_78_PRECALC    1

#define PGESP_78_HZ 48000

// In audio words
#define PGESP_78_BUFSIZE    CONFIG_I2S_BUFFER_SIZE