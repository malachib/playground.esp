#pragma once

#include <cmath>
#include <limits>

#include <embr/dsp/precalc.h>

#include "config.h"
#include "osc.h"

namespace audio {

// Guidance from
// https://thecodeinn.blogspot.com/2014/02/developing-digital-synthesizer-in-c_2.html

// Rewriting just because I want to use Iterator C++ pattern anyway

constexpr double twoPI = 2 * M_PI; // 2*pi = 360˚ = one full cycle
constexpr unsigned sin_table_size = CONFIG_COS_PRECALC_SIZE;
extern scalar_type* sin_table;

void sin_precalc();
void sin_precalc_opt();


// FIX: Keep these guys because curiously they are 5-10% faster than their embr counterparts
constexpr scalar_type sin_lookup(scalar_type v)
{
    static constexpr scalar_type adjuster = sin_table_size / audio::twoPI;
    static constexpr unsigned mask = sin_table_size - 1;
    const unsigned index = static_cast<unsigned>(std::round(adjuster * v)) & mask;

    return sin_table[index];
}


constexpr scalar_type sin_lookup_opt(scalar_type v)
{
    static constexpr scalar_type adjuster = sin_table_size / M_PI;
    static constexpr unsigned mask = sin_table_size - 1;
    const unsigned index = static_cast<unsigned>(std::round(adjuster * v)) & mask;

    if(index > sin_table_size / 2)
        return -sin_table[index - (sin_table_size / 2)];
    else
        return sin_table[index];
}


namespace detail {

// Technically these guys can exist outside of detail, but diagnosing compile-time
// call failures becomes very hard

template <class It, class F>
inline void gen_sine(osc_info& info, unsigned total_samples, It out, F&& f)
{
    info.phase().gen(total_samples, [&](scalar_type phase)
    {
        f(sin(phase), out++);
    });
}

template <class It, class F>
inline void gen_sine_precalc(osc_info& info, unsigned total_samples, It out, F&& f)
{
    info.phase().gen(total_samples, [&](scalar_type phase)
    {
#if EMBR_SINTABLE
        f(embr::dsp::sin_lookup(phase), out++);
#else
        f(sin_lookup(phase), out++);
#endif
    });
}

}


// Benchmarks show this at 14500us on ESP32S3
// DEBT: I think I want a pointer for info, not a reference
template <class Word>
inline void gen_sine(
    osc_info& info, unsigned total_samples,
    const scalar_type vol, Word* buf)
{
    using limits = std::numeric_limits<Word>;

    detail::gen_sine(info, total_samples, buf, [vol](scalar_type v, Word* out)
    {
        *out = vol * limits::max() * v;
    });
}


// Initial benchmarks show this flavor is ~20x faster at ~730us on ESP32S3
// Now is 770us using phase_generator.  I miss you 40us faster :(
// Now is 810us using embr precalc. I miss you 80us faster :(
template <class Word>
inline void gen_sine_precalc(
    osc_info& info, unsigned total_samples,
    const scalar_type vol, Word* buf)
{
    using limits = std::numeric_limits<Word>;

    detail::gen_sine_precalc(info, total_samples, buf, [vol](scalar_type v, Word* out)
    {
        *out = vol * limits::max() * v;
    });
}


}
