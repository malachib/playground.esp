#pragma once

#include "fwd.h"
#include "phase.h"

namespace audio {

// Was done as an optimization (phaseincr was calculated inline before), but doesn't
// really seem to speed things up.  Still, kind of convenient, but just not so necessary
class osc_info
{
    using phase_gen_type = embr::dsp::phase_generator<scalar_type, PGESP_78_HZ>;
    phase_gen_type phase_;
    scalar_type freq_;

public:
    void freq(scalar_type v)
    {
        phase_.freq(v);
        freq_ = v;
    }

    phase_gen_type& phase() { return phase_; }

    osc_info() = default;
    constexpr osc_info(scalar_type freq) :
        phase_{freq},
        freq_{freq}
    {}
};

}
