#pragma once

#include "config.h"

namespace audio {

// DEBT: A more unified RTTI preferred
enum sample_formats
{
    SAMPLE_FLOAT,
    SAMPLE_DOUBLE,
    SAMPLE_INT8,
    SAMPLE_INT16,
    SAMPLE_INT24,
    SAMPLE_INT32,

#if CONFIG_I2S_WORD8
    SAMPLE_SYSTEM = SAMPLE_INT8,
#elif CONFIG_I2S_WORD32
    SAMPLE_SYSTEM = SAMPLE_INT32,
#endif
};

}