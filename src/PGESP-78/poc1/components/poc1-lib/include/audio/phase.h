#pragma once

#include "fwd.h"

namespace embr::dsp {

template <class Numeric, unsigned system_hz_>
class phase_generator
{
    using scalar_type = Numeric;
    using this_type = phase_generator;

    // The phase increment is the phase value the phasor increases by
    // per sample.  (2xNyquist?)
    scalar_type incr_;
    scalar_type phase_ = 0;

    static constexpr double twoPI = 2 * M_PI; // 2*pi = 360˚ = one full cycle

public:
    static constexpr unsigned hz = system_hz_;

    phase_generator() = default;

    constexpr phase_generator(scalar_type freq) :
        incr_{static_cast<scalar_type>(twoPI / hz * freq)}
    {
    }

    constexpr scalar_type operator*() const { return phase_; }

    // DEBT: Prefer not to do this
    scalar_type& operator*() { return phase_; }

    constexpr scalar_type incr() const { return incr_; }

    this_type& operator++()
    {
        phase_ += incr_;
    }

    this_type operator++(int) const
    {
        const scalar_type phase = phase_;

        phase_ += incr_;

        return { incr_, phase };
    }

    template <class F>
    void gen(unsigned count, F&& f)
    {
        for (int i = count; i > 0; i--)
        {
            f(phase_);
            phase_ += incr_;
            
            // 2Pi/360˚/full circle we have to reset the phase
            if (phase_ >= static_cast<scalar_type>(twoPI))
                phase_ -= static_cast<scalar_type>(twoPI);
        }
    }

    void freq(scalar_type v)
    {
        incr_ = twoPI / hz * v;
    }
};


}