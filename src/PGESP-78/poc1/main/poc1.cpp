#include <esp_log.h>

#include <driver/i2s_std.h>

#include <embr/dsp/precalc.h>
#include <embr/profiler.h>

#include <poc1-lib.h>
#include <audio/sine.h>

static const char* TAG = "PGESP-78";

using scalar_type = audio::scalar_type;
using word_type = audio_word_type;

using clock_type = estd::chrono::esp_clock;
using time_point = typename clock_type::time_point;
using duration = typename clock_type::duration;

void sin_benchmark(word_type* buf, unsigned total_samples)
{
    audio::osc_info osc_info;

    osc_info.freq(440);

    embr::Profiler<clock_type> profiler;
    uint64_t sum = 0, sum_dummy = 0, sum_precalc = 0;
    scalar_type dummy = 0;

    for(int i = 0; i < total_samples; i++)
    {
        dummy *= i;
    }

    sum_dummy = profiler.mark().count();
    //sum_dummy /= total_samples;

    audio::detail::gen_sine_precalc(osc_info, total_samples, buf, [&](scalar_type, word_type*)
    {
        if(sum_precalc == 0)
        {
            profiler.reset();      // avoid false measurement of non-sin things in gen_sine
            sum_precalc = 1;
        }

    });

    sum_precalc = profiler.mark().count();

    audio::detail::gen_sine(osc_info, total_samples, buf, [&](scalar_type, word_type*)
    {
        if(sum == 0)
        {
            profiler.reset();      // avoid false measurement of non-sin things in gen_sine
            sum = 1;
        }

        //sum += profiler.mark().count();
    });

    sum = profiler.mark().count();
    //sum /= total_samples;

    ESP_LOGI(TAG, "sin_benchmark: sin=%" PRIu64 "us precalc=%" PRIu64 "us mult=%" PRIu64 "us",
        sum, sum_precalc, sum_dummy);
}

extern "C" void app_main(void)
{
    i2s_chan_handle_t tx_handle = init_i2s();

    //constexpr unsigned count = PGESP_78_HZ;
    // As long as this guy is under total_dma_buf_size, we effectively get a writeback
    // cache here.  If he exceeds it, DMA system seems to be too busy to effectively cache
    // and we can't comfortably generate more sin wav
    constexpr unsigned count = PGESP_78_BUFSIZE;
    static word_type buf[count];
    i2s_chan_info_t chan_info;

    ESP_ERROR_CHECK(i2s_channel_get_info(tx_handle, &chan_info));

    ESP_LOGI(TAG, "app_main: sizeof(buf)=%u, dma_buffer_size=%" PRIu32,
        sizeof(buf),
        chan_info.total_dma_buf_size
    );

    const scalar_type vol = 0.15;

    embr::dsp::v1::init_sin_table();
    audio::sin_precalc();
    sin_benchmark(buf, count);

    time_point last_reported = clock_type::now();
    //using ms = estd::chrono::duration<unsigned, estd::milli>;

    audio::osc_info osc_info(440);

    duration write_d = duration::zero();
    duration gen_d = duration::zero();
    unsigned counter = 0;

    for(;;)
    {
        embr::Profiler<clock_type> profiler;

#if PGESP_78_PRECALC
        audio::gen_sine_precalc(osc_info, count, vol, buf);
#else
        audio::gen_sine(osc_info, count, vol, buf);
#endif

        size_t bytes_written;

        gen_d += profiler.mark();

        ESP_ERROR_CHECK(i2s_channel_write(tx_handle, buf, sizeof(buf), &bytes_written, 50));

        write_d += profiler.mark();

        const time_point now = clock_type::now();

        ++counter;

        if(now - last_reported > estd::chrono::seconds(2))
        {
            // DEBT: estd doesn't have this yet
            //write_d /= counter;
            uint64_t write_davg = write_d.count() / counter;
            uint64_t gen_davg = gen_d.count() / counter;

            ESP_LOGI(TAG, "main: write_d=%" PRIu64 "us gen_d=%" PRIu64 "us",
                write_davg,
                gen_davg);
            last_reported = now;

            counter = 0;
            gen_d = duration::zero();
            write_d = duration::zero();
        }
    }
}
