#include <esp_log.h>
#include <esp_gap_ble_api.h>

#include <embr/platform/esp-idf/ble/gatt/service.h>

// Fun fact: without including this, externs to primary_service_uuid and friends die in link phase
#include "gatt/gatt.h"
#include "gatt/device_info.h"

#include "adv.h"
#include "pm.h"

static const char* TAG = "PGESP-49.1::gatt";

const uint16_t primary_service_uuid          = ESP_GATT_UUID_PRI_SERVICE;
const uint16_t character_declaration_uuid    = ESP_GATT_UUID_CHAR_DECLARE;
const uint16_t character_client_config_uuid  = ESP_GATT_UUID_CHAR_CLIENT_CONFIG;

// DEBT: Unclear how this really is used
constexpr int ESP_APP_ID = 0x55;

enum Services
{
    SVC_DEVICE_INFO
};

static constexpr embr::esp_idf::gatt::v1::Service dis_service(
    SVC_DEVICE_INFO,
    gatt::device_info::db,
    gatt::device_info::handles);

//static esp_gatt_if_t gatts_if ESP_GATT_IF_NONE

static void gatts_profile_event_handler(esp_gatts_cb_event_t event,
    esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t* param)
{
    const char* s = to_string(event, "null");

    ESP_LOGI(TAG, "event_handler: %s (%d)", s, event);

    switch(event)
    {
        case ESP_GATTS_REG_EVT:
            if(param->reg.status != ESP_GATT_OK)
            {
                ESP_LOGE(TAG, "Registration failed: app_id %04x, status %d",
                    param->reg.app_id,
                    param->reg.status);
                return;
            }

            // FIX: Something about this dies - my best guess atm is that adv data has nullptrs
            // in its p_* maybe that's not supported?
            init_adv();

            // Consider using esp_ble_gap_set_device_name, though it's not in ADV
            // and I don't THINK it's the same as the DIS name

            ESP_ERROR_CHECK_WITHOUT_ABORT(dis_service.create_attr_tab(
                gatts_if,
                gatt::device_info::MAX));
            break;

        case ESP_GATTS_CREAT_ATTR_TAB_EVT:
            dis_service.on_create_attr_tab_event(param->add_attr_tab);
            break;

        case ESP_GATTS_CONNECT_EVT:
        {
            esp_ble_conn_update_params_t conn_params{};
            memcpy(conn_params.bda, param->connect.remote_bda, sizeof(esp_bd_addr_t));
            conn_params.latency = CONFIG_PGESP_BLE_ADV_LATENCY;
            conn_params.min_int = min_interval;     // x1.25ms
            conn_params.max_int = max_interval;    
            //conn_params.timeout = 200;              // timeout = 200*10ms = 2000ms
            conn_params.timeout = max_interval / 2;   // timeout = ?*10ms
            //conn_params.timeout = PGESP_BLE_ADV_TIMEOUT / 10    // // timeout = ?*10ms
            //start sent the update connection parameters to the peer device.
            ESP_ERROR_CHECK_WITHOUT_ABORT(esp_ble_gap_update_conn_params(&conn_params));

            // See README 4.1.
            //set_light_sleep(true);
            break;
        }

        case ESP_GATTS_DISCONNECT_EVT:
            //set_light_sleep(false);
            start_advertising();
            break;

        default:    break;
    }
}


static void gatts_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if,
    esp_ble_gatts_cb_param_t *param)
{
    // DEBT: Cut down version of multi profile (?) approach
    gatts_profile_event_handler(event, gatts_if, param);
}


void init_gatt()
{
    gatt::device_info::init();

    // NOTE: gap currently handled in ble.cpp and is iBeacon specific-ish
    ESP_ERROR_CHECK(esp_ble_gatts_register_callback(gatts_event_handler));
    ESP_ERROR_CHECK(esp_ble_gatts_app_register(ESP_APP_ID));
}
