#include <esp_bt_device.h>
#include <esp_bt_main.h>
#include <esp_bt.h>

#include <esp_gap_ble_api.h>
#include <esp_gatts_api.h>

#include <esp_log.h>

#include "adv.h"

static const char* TAG = TAG_PREFIX "adv";

// Some guidance from [5.1] though it still crashes

static uint8_t service_uuid[16]
{
    /* LSB <--------------------------------------------------------------------------------> MSB */
    //first uuid, 16bit, [12],[13] is the value
    0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00,
};

// DEBT: Can't do const here... but I don't think adv_data truly
// needs this writable... does it?
static esp_ble_adv_data_t adv_data
{
    .set_scan_rsp = false,
    .include_name = true,
    .include_txpower = false,

    // Preferred connection intervals
    // * 1.25ms
    .min_interval = min_interval,
    .max_interval = max_interval,

    .appearance = 0,
    .manufacturer_len = 0,
    .p_manufacturer_data = nullptr,
    .service_data_len = 0,
    .p_service_data = nullptr,
    .service_uuid_len = sizeof(service_uuid),
    .p_service_uuid = service_uuid,
    .flag = ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT
};


// scan response data
static esp_ble_adv_data_t scan_rsp_data
{
    .set_scan_rsp        = true,
    .include_name        = true,
    .include_txpower     = true,
    .min_interval        = min_interval,
    .max_interval        = max_interval,
    .appearance          = 0x00,
    .manufacturer_len    = 0, //TEST_MANUFACTURER_DATA_LEN,
    .p_manufacturer_data = NULL, //&test_manufacturer[0],
    .service_data_len    = 0,
    .p_service_data      = NULL,
    .service_uuid_len    = sizeof(service_uuid),
    .p_service_uuid      = service_uuid,
    .flag = (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT),
};


static esp_ble_adv_params_t ble_adv_params {
    .adv_int_min        = min_interval,
    .adv_int_max        = max_interval,
    .adv_type           = ADV_TYPE_IND,
    .own_addr_type      = BLE_ADDR_TYPE_PUBLIC,
    .peer_addr          {},
    .peer_addr_type     = BLE_ADDR_TYPE_PUBLIC,
    .channel_map        = ADV_CHNL_ALL,
    .adv_filter_policy = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY,
};


void start_advertising()
{
    ESP_ERROR_CHECK_WITHOUT_ABORT(esp_ble_gap_start_advertising(&ble_adv_params));
}


void init_adv()
{
    ESP_LOGV(TAG, "init_adv");

    ESP_ERROR_CHECK(esp_ble_gap_set_device_name(TAG_PREFIX));
    ESP_ERROR_CHECK(esp_ble_gap_config_adv_data(&adv_data));
    ESP_ERROR_CHECK(esp_ble_gap_config_adv_data(&scan_rsp_data));
}
