#include <esp_bt_device.h>
#include <esp_bt_main.h>
#include <esp_bt.h>

#include <esp_gap_ble_api.h>
#include <esp_gatts_api.h>

#include <esp_log.h>

#include "adv.h"

static const char* TAG = TAG_PREFIX "ble";

void init_gatt();

static const char* to_string(esp_gap_ble_cb_event_t eventType)
{
    switch(eventType)
    {
        case ESP_GAP_BLE_ADV_DATA_SET_COMPLETE_EVT:
            return "ESP_GAP_BLE_ADV_DATA_SET_COMPLETE_EVT";
        case ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT:
            return "ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT";
        case ESP_GAP_BLE_SCAN_RESULT_EVT:
            return "ESP_GAP_BLE_SCAN_RESULT_EVT";
        case ESP_GAP_BLE_SCAN_RSP_DATA_SET_COMPLETE_EVT:
            return "ESP_GAP_BLE_SCAN_RSP_DATA_SET_COMPLETE_EVT";
        case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
            return "ESP_GAP_BLE_ADV_START_COMPLETE_EVT";
        default:
            return "Unknown event type";
    }
}



static void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
    ESP_LOGD(TAG, "esp_gap_cb: event=%s (%d)", to_string(event), event);

    switch(event)
    {
        case ESP_GAP_BLE_ADV_DATA_SET_COMPLETE_EVT:
            start_advertising();
            break;

        case ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT:
            start_advertising();
            break;

        case ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT:
            start_advertising();
            break;

        case ESP_GAP_BLE_SCAN_RSP_DATA_SET_COMPLETE_EVT:
            //start_advertising();
            break;

         case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
            /* advertising start complete event to indicate advertising start successfully or failed */
            if (param->adv_start_cmpl.status != ESP_BT_STATUS_SUCCESS)
                ESP_LOGE(TAG, "advertising start failed");
            else
                ESP_LOGI(TAG, "advertising start successfully");

            break;
            
        default:    break;
    }
}

void init_bt()
{
    ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT));
    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
#if CONFIG_IDF_TARGET_ESP32C6
    bt_cfg.cca_drop_mode = 0;
    bt_cfg.cca_low_tx_pwr = 0;
#endif
    ESP_ERROR_CHECK(esp_bt_controller_init(&bt_cfg));
    ESP_ERROR_CHECK(esp_bt_controller_enable(ESP_BT_MODE_BLE));
    
    
    // TODO: ESP-IDF v5.2.1 Indicates to use 'esp_bluedroid_init_with_cfg' instead
#if TO_BE_ACTIVATED
    static const esp_bluedroid_config_t bd_cfg = BT_BLUEDROID_INIT_CONFIG_DEFAULT();
    esp_bluedroid_init_with_cfg(&bd_cfg);
#else
    ESP_ERROR_CHECK(esp_bluedroid_init());
#endif
    ESP_ERROR_CHECK(esp_bluedroid_enable());

    ESP_ERROR_CHECK(esp_ble_gap_register_callback(esp_gap_cb));

    const uint8_t* mac = esp_bt_dev_get_address();

    if(mac != nullptr)
    {
        ESP_LOGI(TAG, "BLE MAC: %02X:%02X:%02X:%02X:%02X:%02X",
            (int)mac[0], (int)mac[1], (int)mac[2],
            (int)mac[3], (int)mac[4], (int)mac[5]);
    }

#if CONFIG_PGESP_BLE_GATT_ENABLED
    init_gatt();
#else
    init_adv();
#endif
}
