#pragma once

constexpr int min_interval = 0x100;
constexpr int max_interval = 0x200;

void init_adv();
void start_advertising();