#pragma once

#include <esp_gatts_api.h>

namespace gatt::device_info {

enum
{
    SVC,

    SERIAL_NUMBER_CHAR,
    SERIAL_NUMBER_VALUE,

    FIRMWARE_REVISION_CHAR,
    FIRMWARE_REVISION_VALUE,

    MAX
};

extern const esp_gatts_attr_db_t db[];
extern uint16_t handles[MAX];

void init();

}
