#pragma once

#include <esp_gatts_api.h>

extern const uint16_t primary_service_uuid;
extern const uint16_t character_declaration_uuid;
extern const uint16_t character_client_config_uuid;

#define CHAR_DECLARATION_SIZE       (sizeof(uint8_t))

