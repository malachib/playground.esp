#include <esp_log.h>
#include <esp_system.h>
#include <esp_pm.h>
//#include <esp_sleep.h>
#include <nvs_flash.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

//#include <embr/platform/esp-idf/allocator.h>

#include "pm.h"

const char* TAG = TAG_PREFIX "main";

void init_bt();
void init_button();
void init_gpio();
void init_pm();

extern "C" void app_main(void)
{
    ESP_LOGI(TAG, "entry");

    ESP_ERROR_CHECK(nvs_flash_init());

    // Actually stops running
    //ESP_ERROR_CHECK(esp_light_sleep_start());

#if CONFIG_PGESP_LP_MODE > 1
    set_light_sleep(true);
#elif CONFIG_PGESP_LP_MODE > 0
    set_light_sleep(false);
#endif

#if CONFIG_PGESP_BUTTON_ENABLED
    init_button();
#endif

#if CONFIG_PGESP_LED_ENABLED
    init_gpio();
#endif

#if CONFIG_PGESP_BLE_ENABLED
    init_bt();
#endif

    for(;;)
    {
        ESP_LOGD(TAG, "app_main: blip");
        vTaskDelay(100);
    }
}
