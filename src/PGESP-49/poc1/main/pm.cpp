#include <esp_pm.h>
#include <esp_system.h>

#define MIN_MHZ (CONFIG_ESP_DEFAULT_CPU_FREQ_MHZ / 4)

void set_light_sleep(bool enabled)
{
    const esp_pm_config_t pm_config
    {
        // Doesn't make a sizeable difference shifting these around
#if CONFIG_IDF_TARGET_ESP32C6_DISABLED
        .max_freq_mhz = 160,
        .min_freq_mhz = 40,
#else
        .max_freq_mhz = CONFIG_ESP_DEFAULT_CPU_FREQ_MHZ,
        .min_freq_mhz = MIN_MHZ,
#endif
#if CONFIG_FREERTOS_USE_TICKLESS_IDLE
        .light_sleep_enable = enabled
#else
#warning "Probably you want tickless idle enabled"
#endif
    };

    ESP_ERROR_CHECK(esp_pm_configure(&pm_config));
}