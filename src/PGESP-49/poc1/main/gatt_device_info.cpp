#include <esp_app_desc.h>
#include <esp_log.h>

// ESP_GATT_UUID_DEVICE_INFO_SVC
// See DIS v1.2

#include "gatt/gatt.h"
#include "gatt/device_info.h"

// NOTE: Fun fact, nimble implementation appears to have this implemented already,
// see "nimble/host/services/dis" and "nimble/host/services/dis/include/services/dis"
// Also, esp_ble_mesh does it too

static const char* TAG = "PGESP-49.1::gatt::device_info";

namespace gatt::device_info {

static const uint16_t service_uuid              = ESP_GATT_UUID_DEVICE_INFO_SVC;
static const uint16_t firmware_revision         = 0x2A26;
static const uint16_t manufacturer_name         = 0x2A29;
static const uint16_t model_number              = 0x2A24;

static const uint8_t char_prop_read             = ESP_GATT_CHAR_PROP_BIT_READ;


//static const char ver[] = "synthetic 0.0";
//static const esp_app_desc_t* app_desc;
// DEBT: Use pointer here
static esp_app_desc_t app_desc;
static const char manufacturer[] = "PGESP";

// See gatt_battery for why this is extern
extern const esp_gatts_attr_db_t db[]
{
    {
        { ESP_GATT_AUTO_RSP },
        {
            ESP_UUID_LEN_16, (uint8_t*) &primary_service_uuid,
            ESP_GATT_PERM_READ,
            sizeof(uint16_t),
            sizeof(service_uuid), (uint8_t*) &service_uuid
        },
    },
    {   // Characteristic / Description
        { ESP_GATT_AUTO_RSP },
        {
            ESP_UUID_LEN_16, (uint8_t*)&character_declaration_uuid,
            ESP_GATT_PERM_READ,
            CHAR_DECLARATION_SIZE,
            CHAR_DECLARATION_SIZE,
            (uint8_t*)&char_prop_read
        }
    },
    {
        // Characteristic value actual variable we can read
        { ESP_GATT_AUTO_RSP },
        {
            ESP_UUID_LEN_16, (uint8_t*)&firmware_revision,
            ESP_GATT_PERM_READ,
            //sizeof(ver), sizeof(ver), (uint8_t*)&ver
            // DEBT: Adjust size instead of pushing down a max size null terminated string
            // that will require fixing above esp_app_desc_t pointer debt and changing
            // this here semi-dynamically
            sizeof(esp_app_desc_t::version), sizeof(esp_app_desc_t::version), (uint8_t*)app_desc.version
        }
    },
    {   // Characteristic / Description
        { ESP_GATT_AUTO_RSP },
        {
            ESP_UUID_LEN_16, (uint8_t*)&character_declaration_uuid,
            ESP_GATT_PERM_READ,
            CHAR_DECLARATION_SIZE,
            CHAR_DECLARATION_SIZE,
            (uint8_t*)&char_prop_read
        }
    },
    {
        // Characteristic value actual variable we can read
        { ESP_GATT_AUTO_RSP },
        {
            ESP_UUID_LEN_16, (uint8_t*)&manufacturer_name,
            ESP_GATT_PERM_READ,
            sizeof(manufacturer), sizeof(manufacturer), (uint8_t*)manufacturer
        }
    },
};


uint16_t handles[MAX];

void init()
{
    // NOTE: I think annotated tags that are pushed to upstream are a must
    // before version picks them up
    app_desc = *esp_app_get_description();
    ESP_LOGI(TAG, "version = %s", app_desc.version);
}


}