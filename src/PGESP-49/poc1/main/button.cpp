#include <esp_log.h>
#include <iot_button.h>

static const char* TAG = "PGESP-49.1::button";

void set_led(bool state)
{
#if CONFIG_PGESP_LED_ENABLED
    // DEBT: Make this LED configurable
    gpio_set_level((gpio_num_t)4, state);
#endif
}

static void button_cb(void *arg, void *usr_data)
{
    int mode = (int) usr_data;

    switch(mode)
    {
        case BUTTON_PRESS_UP:
            ESP_LOGD(TAG, "Up");
            set_led(false);
            break;

        case BUTTON_PRESS_DOWN:
            ESP_LOGD(TAG, "Down");
            set_led(true);
            break;

        default:    break;
    }
}

void init_button()
{
    ESP_LOGI(TAG, "init");

    // create gpio button
    static const button_config_t gpio_btn_cfg =
    {
        .type = BUTTON_TYPE_GPIO,
        .long_press_time = CONFIG_BUTTON_LONG_PRESS_TIME_MS,
        .short_press_time = CONFIG_BUTTON_SHORT_PRESS_TIME_MS,
        .gpio_button_config =
        {
            // DEBT: Hang this off board/target and/or special config
            .gpio_num = 9,
            .active_level = 0,
            .enable_power_save = true
        },
    };

    button_handle_t gpio_btn = iot_button_create(&gpio_btn_cfg);
    if(NULL == gpio_btn)
    {
        ESP_LOGE(TAG, "Button create failed");
    }

    iot_button_register_cb(gpio_btn, BUTTON_PRESS_DOWN, button_cb, (void*) BUTTON_PRESS_DOWN);
    iot_button_register_cb(gpio_btn, BUTTON_PRESS_UP, button_cb, (void*) BUTTON_PRESS_UP);
}


void init_gpio()
{
    const int pin = 4;
    const uint64_t pin_mask = 1ULL << pin;

    const gpio_config_t config = {
        .pin_bit_mask = pin_mask,
        .mode = GPIO_MODE_OUTPUT,
        .pull_up_en = GPIO_PULLUP_DISABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .intr_type = GPIO_INTR_DISABLE
    };

    ESP_ERROR_CHECK(gpio_config(&config));
}

