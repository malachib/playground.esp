# Overview

Document v0.2

# 1. Goals

Lowest energy we can get!

## 1.1. Requirements

We need a bunch of services to properly simulate a real discovery phase.  4 ought to be enough.

### 1.1.1. Prerequisites

#### 1.1.1.1. Turn off peripherals

TBD
* Keep ADC and GPIO on

#### 1.1.1.2. IoT button

## 1.2. Things to try

### 1.2.1. Connection Interval Tuning

### 1.2.2. Tickless Mode

I get the feeling this is the big one.
Start with this with radios off to see how far we can get CPU down.  Establish this as a baseline

### 1.2.3. Slave Latency

### 1.2.4. ADV interval

Less important only because it's a simpler known quantity.  Still significant though.

### 1.2.5. TX power max

### 1.2.6. ADV payload size

As per [4] apparently this makes a difference too.

## 1.3. Tests

### 1.3.1. Power Draw

Variant A1: CP2102 used - poc1
Variant B1: USB/JTAG used - poc1
Variant A2: CP2102 used - poc2
Variant B2: USB/JTAG used - poc2

#### 1.3.1.1. LED

Due to 3.2. USB/JTAG behavior, a blinking LED is needed.

On startup, LED will flash quickly 3 times
On nominal run, LED will blip slowly - on for maximum 1/4s, off for 5s

Button press momentarily illuminates LED to full

#### 1.3.1.2. Log Output

Consider inhibiting log output too

#### 1.3.1.3. ADV

BLE Advertising itself

320-640ms min/max interval

#### 1.3.1.4. Connected

BLE seems to draw a little less power while connected

#### 1.3.1.5. Baseline

Least possible activity, lowest possible consumption while still being awake.
BLE not on

## 1.4. Variants

### 1.4.1. poc1

Bluedroid low energy tests

### 1.4.2. poc2

NimBLE low energy tests

Starts life as a copy/paste of PGESP-37

### 1.4.3. poc3

Cut-down poc1 in service of Section 4.1.

### 1.4.4. poc4

Cut-down poc2 in service of Section 4.3.

# 2. Infrastructure

## 2.1. Boards

### 2.1.1. ESP32C6 DevKit-C1

MAC: 40:4C:CA:44:9F:2E

# 3. Observations & Opinions

## 3.1. Silicon Labs guidance

OK how dare you consume so little power !! [1]
That complain aside, very clear guidance here.  We definitely need to increase our intervals judiciously
(see if we can raise the intervals AFTER service discovery)

Looks like someone just repackaged their guidance, fooled me [2]

## 3.2. CP2102 vs USB/JTAG

As expected:

* low power affects usability here.
* CP2102 activity incurs more power usage.
* USB/JTAG goes totally offline during sleep (just like AVR 32u4 does)

This means with USB/JTAG as primary connection force re-entry into bootloader is required

## 3.3. Power Draw

### 3.3.1. Journal

#### 31MAR24 - poc1

Was hoping for less than ~3ma.  Not disappointed or surprised.
Surprised that CP2102 active uses ~8ma more!

Noteworthy is 2.1.1. board tested uses LDOs AND has an always-on power LED.

#### 01APR24 - poc1

iot-button performs nicely, casual measurement is it pushes avg consumption up by 250uA at most, and maybe not really at all.

Noteworthy is during button press ESP is fully awake.  Noticing a ~24ma delta between low power and full power.

Something about the GPIO/LED configuration seems to occupy another 1mA even when LED is off.  I think some pulldowns may be activated

#### 08APR24 - poc1

Although ADV issue 4.1. is frustrating, we could do other testing by keeping
PM off while advertising then activate it for a connection.  I registered this
as a bug [5.2] with Espressif

#### 10APR24 - poc1

Holy smokes!  Running ESP32C6 with Espressif' suggested changes not only works, but power consumption is incredibly low!

Idling at ~550uA (not using FTDI and with expected spikes) and device is still responding to BLE commands.  Wow!

Whoops, I read it wrong.  It's 5.5mA.  OK, not amazing numbers.  Workeable though.
Numbers went up from 01APR and that is sensible since now ADV is fully on and real BLE 2 way communocation can happen

These numbers are based on:

* min_interval = 0x100 (320ms)
* max_interval = 0x200 (640ms)
* timeout = 200 (2000ms)

Higher numbers produce favorable results, as expected

#### 11APR24 - poc2

Surprisingly, NimBLE out of the gate draws noticeably more power.  ~5ma more.
I think connection intervals are affecting this a lot.  Yes, it does.  Adjusting
intervals to match poc1 last state produces similar power draw numbers

Not able to suss out how to adjust connection parameters yet, so unsurprisingly
once connection is made, power usage jumps up.

Really stuck on section 4.3. issue, added 2nd request to esp-idf issue [5.2]

### 3.3.2. Erratic Behaviors

For some reason, sometimes during connection w/ poc1 there's a lot of spikiness which power draw moving constantly between 14ma-60ma.

Is related to the combo of min, max, timeout and latency.  i.e. if you find a combo that doesn't spike, it will continue to not spike for subsequent runs.

### 3.3.3. IRAM placement

Activating sleep & wakeup IRAM seemed to make a tiny, tiny difference but hard to say it was so minor.

## 3.4. Code Duplication

Yanked in & adapted BLE code from production project for poc1.  Would be much better to embr-ize it.

Copy/pasted PGESP-37 code for poc2

## 3.5. Can't Reconnect - poc1

Unsurprising since it's currently so barebones, so not a troubleshooting task.
At the moment there's no ESP_GATTS_CONNECT_EVT, ESP_GATTS_DISCONNECT_EVT

It seems ADV is required for connect again.  Presumably a pair/bond would obviate this.

## 3.6. Slow service discovery

Higher intervals slows the speed at which services are discovered.  We'll want
to look into low intervals during discovery period then raise them way up.

I stumbled on `ble_gap_update_params` which may be the magic to adjust connection
intervals

## 3.6. NimBLE same as Bluedroid?

As per PGQT-50 Readme Section 3.5., avoid pursuing poc2 unless we are certain we can't just retarget poc1 to NimBLE.  At this point though it's looking like indeed they are two distinct APIs.  Therefore, poc2 is a go

## 3.7. RTC clock source (no 32k XTAL)

### 3.7.1. poc1

### 3.7.2. poc2

This does come up and somewhat inconsistently advertises.  Uses ~3ma less than regular clock source.  Impressive.  Connections fail:

```
I (50845) NimBLE: disconnect; reason=574
```

and

```
I (62105) NimBLE: disconnect; reason=520
```

## 3.8. Unexpected Warnings

### 3.8.1. esp_perip_clk_init()

```
W (269) clk: esp_perip_clk_init() has not been implemented yet
```

### 3.8.2. rx_evt not registered

```
D (893) BLE_INIT: ble_hci_unregistered_hook ble hci rx_evt is not registered.
```

# 4. Troubleshooting

## 4.1. Crash on advertise attempt

Issue registered with Espressif [5.2]

```
BLE assert: line 1444 in function r_ble_lll_sched_check_remaining_entries, param: 0x1, 0x6000
assert failed: osi_assert_wrapper bt.c:265 (0)
```

ESP-IDF has no hit for "ble_lll_sched_check_remaining_entries" so I can only conclude that is part of the closed firmware.

Noteworthy is that if CONFIG_PGESP_LP_MODE == 0 (no low power config), this crash
is not observed.

For BT_LE_LP_CLK_SRC_DEFAULT:
"Using any clock source other than external 32.768 kHz XTAL supports only legacy ADV and SCAN due to low clock accuracy."

Additionally, if one keeps light sleep off then activates it during a connection
only, it crashes the same way.

RESOLVED:  Enabling BT_LE_POWER_CONTROL_ENABLED fixes this

## 4.2. esp_pm_configure: Not supported

If tickless idle is not enabled, you may see this

## 4.3. WDT-ish reset

Registered issue with Espressif [5.3].  They provide a patch which appears to work.  According to them v5.1.4 will have a fix.

For poc2, can't seem to follow example [6] to sleep flash (`CONFIG_ESP_SLEEP_POWER_DOWN_FLASH`).  I saw it work one time, and now it always does a mini-hang and then restarts, printing this:

```
rst:0x8 (TG1_WDT_HPSYS),boot:0xc (SPI_FAST_FLASH_BOOT)
```

Google and github searching reveals almost nothing on this, except for ESP32C5 code which indicates this is "Timer Group1 Watchdog reset hp system" [6.1] and
a unity test [6.2] neither of which I'd think would have bearing.  I can only
hypothesize that this string is in a closed source bootrom.

Also, their example [6] does not exhibit this behavior.

If I disable all the watchdogs, then I see:

```
rst:0x10 (LP_WDT_SYS),boot:0xc (SPI_FAST_FLASH_BOOT)
```

# 5. Results

| Date    | Test       | Board             | Result     | Notes
| -       | -          | -                 | -          | -
| 31MAR24 | N/A        | FeatherS3         | N/A        |
| 01APR24 | 1.3.1.5 A1 | ESP32C6 DevKit-C1 | ~11.9ma    |
| 01APR24 | 1.3.1.5 B1 | ESP32C6 DevKit-C1 | ~2.6ma     |
| 10APR24 | 1.3.1.3 A1 | ESP32C6 DevKit-C1 | ~16mA      |
| 10APR24 | 1.3.1.4 A1 | ESP32C6 DevKit-C1 | ~14.75mA   |
| 10APR24 | 1.3.1.3 B1 | ESP32C6 DevKit-C1 | ~7mA       |
| 10APR24 | 1.3.1.4 B1 | ESP32C6 DevKit-C1 | ~5.5mA     |
| 10APR24 | 1.3.1.5 A1 | ESP32C6 DevKit-C1 | ~12ma      |
| 11APR24 | 1.3.1.3 A2 | ESP32C6 DevKit-C1 | ~16.25mA   |
| 24APR24 | 1.3.1.3 B2 | ESP32C6 DevKit-C1 | ~15.5mA    |
|  | N/A       | WaveShare ESP32C6 | N/A    |

# References

1. https://docs.silabs.com/bluetooth/2.13/general/system-and-performance/optimizing-current-consumption-in-bluetooth-low-energy-devices
2. https://novelbits.io/ble-power-consumption-optimization/
3. https://www.mistywest.com/posts/low-power-optimizations-for-a-bluetooth-connected-wearable/
4. https://developer.qualcomm.com/hardware/qca4020-qca4024/learning-resources/improving-battery-life-in-ble-communication
5. https://github.com/espressif/esp-idf/blob/v5.1.3/examples/bluetooth/bluedroid/ble/
    1. https://github.com/espressif/esp-idf/blob/v5.1.3/examples/bluetooth/bluedroid/ble/gatt_server_service_table/main/gatts_table_creat_demo.c 
    2. https://github.com/espressif/esp-idf/issues/13566
    3. https://github.com/espressif/esp-idf/issues/13619
6. https://github.com/espressif/esp-idf/tree/v5.1.3/examples/bluetooth/nimble/power_save
    1. https://github.com/espressif/esp-idf/blob/5a40bb8746633477c07ff9a3e90016c37fa0dc0c/components/esp_rom/include/esp32c5/beta3/esp32c5/rom/rtc.h#L83
    2. https://github.com/espressif/esp-idf/blob/5a40bb8746633477c07ff9a3e90016c37fa0dc0c/components/esp_system/test_apps/esp_system_unity_tests/main/test_reset_reason.c#L83 
