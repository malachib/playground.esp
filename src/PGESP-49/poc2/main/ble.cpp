#include <freertos/FreeRTOSConfig.h>

#include "nimble/nimble_port.h"
#include "nimble/nimble_port_freertos.h"
#include "host/ble_hs.h"
#include "host/util/util.h"
#include "console/console.h"
#include "services/gap/ble_svc_gap.h"
#include "services/cts/ble_svc_cts.h"

#include "ble.h"

static const char* TAG = "PGESP-49.2::ble";
static const char *device_name = "PGESP-49.2";

static int ble_cts_prph_gap_event(struct ble_gap_event *event, void *arg);

uint8_t ble_cts_prph_addr_type;

void print_addr(const uint8_t* u8p)   // DEBT: Consoildate this into EMBR
{
    MODLOG_DFLT(INFO, "%02x:%02x:%02x:%02x:%02x:%02x",
                u8p[5], u8p[4], u8p[3], u8p[2], u8p[1], u8p[0]);
}


inline static void ble_cts_prph_advertise()
{
#if CONFIG_PGESP_BLE_ADV_ENABLED
    // DEBT: Make this const once we iron out README 4.3.
    static ble_uuid16_t services[]
    {
        BLE_UUID16_INIT(BLE_SVC_CTS_UUID16)
    };

    ble_prph_advertise(device_name, ble_cts_prph_gap_event, services, 1);
#endif
}


static int ble_cts_prph_gap_event(struct ble_gap_event *event, void *arg)
{
    switch (event->type)
    {
        case BLE_GAP_EVENT_CONNECT:
            /* A new connection was established or a connection attempt failed */
            MODLOG_DFLT(INFO, "connection %s; status=%d\n",
                        event->connect.status == 0 ? "established" : "failed",
                        event->connect.status);

            // TODO: Do a ble_gap_update_params nearby

            if (event->connect.status != 0)
            {
                /* Connection failed; resume advertising */
                ble_cts_prph_advertise();
            }
            break;

        case BLE_GAP_EVENT_DISCONNECT:
            MODLOG_DFLT(INFO, "disconnect; reason=%d\n", event->disconnect.reason);

            /* Connection terminated; resume advertising */
            ble_cts_prph_advertise();

            break;

        case BLE_GAP_EVENT_ADV_COMPLETE:
            MODLOG_DFLT(INFO, "adv complete\n");
            ble_cts_prph_advertise();
            break;

        case BLE_GAP_EVENT_SUBSCRIBE:
            MODLOG_DFLT(INFO, "subscribe event; cur_notify=%d\n value handle; "
                        "val_handle=%d\n",
                        event->subscribe.cur_notify, event->subscribe.attr_handle);

            break;

        case BLE_GAP_EVENT_MTU:
            MODLOG_DFLT(INFO, "mtu update event; conn_handle=%d mtu=%d\n",
                        event->mtu.conn_handle,
                        event->mtu.value);
            break;

    }

    return 0;
}


static void ble_cts_prph_on_sync(void)
{
    int rc;

    rc = ble_hs_id_infer_auto(0, &ble_cts_prph_addr_type);
    assert(rc == 0);

    uint8_t addr_val[6] = {0};
    rc = ble_hs_id_copy_addr(ble_cts_prph_addr_type, addr_val, NULL);

    MODLOG_DFLT(INFO, "Device Address: ");
    print_addr(addr_val);
    MODLOG_DFLT(INFO, "\n");

    /* Begin advertising */
    ble_cts_prph_advertise();
}

static void ble_cts_prph_on_reset(int reason)
{
    MODLOG_DFLT(ERROR, "Resetting state; reason=%d\n", reason);
}


static void ble_prph_host_task(void *param)
{
    ESP_LOGI(TAG, "BLE Host Task Started");
    /* This function will return only when nimble_port_stop() is executed */
    nimble_port_run();

    nimble_port_freertos_deinit();
}



void init_ble()
{
    esp_err_t ret = nimble_port_init();
    if (ret != ESP_OK)
    {
        MODLOG_DFLT(ERROR, "Failed to init nimble %d \n", ret);
        return;
    }

    /* Initialize the NimBLE host configuration */
    ble_hs_cfg.sync_cb = ble_cts_prph_on_sync;
    ble_hs_cfg.reset_cb = ble_cts_prph_on_reset;

    /* Enable bonding */
    ble_hs_cfg.sm_bonding = 1;
    ble_hs_cfg.sm_our_key_dist |= BLE_SM_PAIR_KEY_DIST_ENC | BLE_SM_PAIR_KEY_DIST_ID;
    ble_hs_cfg.sm_their_key_dist |= BLE_SM_PAIR_KEY_DIST_ENC | BLE_SM_PAIR_KEY_DIST_ID;

    ble_hs_cfg.sm_sc = 1;
    ble_hs_cfg.sm_mitm = 1;
}


void start_ble()
{
    /* Set the default device name */
    int rc = ble_svc_gap_device_name_set(device_name);
    assert(rc == 0);

    /* Start the task */
    nimble_port_freertos_init(ble_prph_host_task);
}
