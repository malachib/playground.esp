#pragma once

// DEBT: Get NimBLE dependencies in here

void ble_prph_advertise(
    const char* device_name,
    ble_gap_event_fn,
    const ble_uuid16_t* service_uuids,
    int service_uuids_count);
