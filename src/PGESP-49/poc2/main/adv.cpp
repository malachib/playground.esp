#include "nimble/nimble_port.h"
#include "nimble/nimble_port_freertos.h"
#include "host/ble_hs.h"
#include "host/util/util.h"
#include "services/gap/ble_svc_gap.h"
#include "services/cts/ble_svc_cts.h"


static const char* TAG = "PGESP-49.2::adv";

// Guidance from https://github.com/espressif/esp-idf/blob/v5.1.3/examples/bluetooth/nimble/ble_cts/cts_prph/main/main.c

// Wrapper around https://mynewt.apache.org/latest/network/ble_hs/ble_gap.html#c.ble_gap_adv_start

// DEBT
extern uint8_t ble_cts_prph_addr_type;

// The non-CONFIG_EXAMPLE_EXTENDED_ADV variety
// DEBT: Still hard wired to CTS
void ble_prph_advertise(const char* device_name,
    ble_gap_event_fn cb,
    const ble_uuid16_t* service_uuids,
    int service_uuids_count)
{
    // https://mynewt.apache.org/latest/network/ble_hs/ble_gap.html#c.ble_gap_adv_params
    struct ble_gap_adv_params adv_params;
    struct ble_hs_adv_fields fields;
    int rc;

    /*
     *  Set the advertisement data included in our advertisements:
     *     o Flags (indicates advertisement type and other general info)
     *     o Advertising tx power
     *     o Device name
     */
    memset(&fields, 0, sizeof(fields));

    /*
     * Advertise two flags:
     *      o Discoverability in forthcoming advertisement (general)
     *      o BLE-only (BR/EDR unsupported)
     */
    fields.flags = BLE_HS_ADV_F_DISC_GEN |
                   BLE_HS_ADV_F_BREDR_UNSUP;

    /*
     * Indicate that the TX power level field should be included; have the
     * stack fill this value automatically.  This is done by assigning the
     * special value BLE_HS_ADV_TX_PWR_LVL_AUTO.
     */
    fields.tx_pwr_lvl_is_present = 1;
    fields.tx_pwr_lvl = BLE_HS_ADV_TX_PWR_LVL_AUTO;

    fields.name = (uint8_t *)device_name;
    fields.name_len = strlen(device_name);
    fields.name_is_complete = 1;

    // C++ complains about taking an address of a temporary, and so do I
    // DEBT: Do a little R&D to see if somehow this is actually safe in C
    //fields.uuids16 = (ble_uuid16_t[]) {
    //    BLE_UUID16_INIT(BLE_SVC_CTS_UUID16)
    //};

    fields.uuids16 = service_uuids;

    fields.num_uuids16 = service_uuids_count;
    fields.uuids16_is_complete = 1;

    rc = ble_gap_adv_set_fields(&fields);
    if (rc != 0) {
        MODLOG_DFLT(ERROR, "error setting advertisement data; rc=%d\n", rc);
        return;
    }

    /* Begin advertising */
    memset(&adv_params, 0, sizeof(adv_params));
    adv_params.conn_mode = BLE_GAP_CONN_MODE_UND;
    adv_params.disc_mode = BLE_GAP_DISC_MODE_GEN;
#if CONFIG_PGESP_BLE_ADV_MIN_INTERVAL
    adv_params.itvl_min = CONFIG_PGESP_BLE_ADV_MIN_INTERVAL / 1.25;
    adv_params.itvl_max = CONFIG_PGESP_BLE_ADV_MAX_INTERVAL / 1.25;
#elif CONFIG_PGESP_BLE_ENABLED
#error Looks like configuration is goofed up
#endif

    ESP_LOGD(TAG, "ble_prph_advertise: itvl_min=%d, itvl_max=%d",
        adv_params.itvl_min,
        adv_params.itvl_max);

    // This is only for conn
    //adv_params.latency = CONFIG_PGESP_BLE_ADV_TIMEOUT / 10;
    
    rc = ble_gap_adv_start(ble_cts_prph_addr_type, NULL, BLE_HS_FOREVER,
                           &adv_params, cb, NULL);
    if (rc != 0) {
        MODLOG_DFLT(ERROR, "error enabling advertisement; rc=%d\n", rc);
        return;
    }
}

