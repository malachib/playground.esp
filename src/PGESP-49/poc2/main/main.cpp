#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <esp_log.h>
#include <nvs_flash.h>


static const char* TAG = "PGESP-49.2::main";

void set_light_sleep(bool enabled);
void init_ble();
int gatt_svr_init();
void start_ble();

extern "C" void app_main(void)
{
    ESP_LOGI(TAG, "main: entry");

    /* Initialize NVS — it is used to store PHY calibration data */
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }

    set_light_sleep(true);

    ESP_ERROR_CHECK(ret);

#if CONFIG_PGESP_BLE_ENABLED
    init_ble();
#if CONFIG_PGESP_BLE_GATT_ENABLED
    gatt_svr_init();
#endif
    start_ble();
#endif

    for(;;)
    {
        ESP_LOGD(TAG, "app_main: blip");
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}