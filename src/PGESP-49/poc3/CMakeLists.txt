cmake_minimum_required(VERSION 3.19)

set(COMPONENTS main)

include($ENV{IDF_PATH}/tools/cmake/project.cmake)

project(PGESP-49.3)
