idf_component_register(
    SRCS
        adv.cpp ble.cpp
        "main.cpp" pm.cpp
    INCLUDE_DIRS "include"
    REQUIRES bt esp_pm nvs_flash)

target_compile_definitions(${COMPONENT_TARGET} PRIVATE "-DTAG_PREFIX=\"PGQT-49.3::\"")
