# Overview

Cut-down version of poc1 to coordinate with Espressif
https://github.com/espressif/esp-idf/issues/13566

# 1. RESERVED

# 2. Infrastructure

## 2.1. ESP32C6 DevKitC-1

# 3. Opinions & Observations

## 3.1. Log

### 10APR24

As per @zhaoweiliang2021's recommendation, enabling CONFIG_ESP_PHY_MAC_BB_PD

Noticing on my own BT_LE_POWER_CONTROL_ENABLED might be interesting

# References

See poc1 References
