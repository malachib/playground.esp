# Guidance

https://github.com/espressif/esp-iot-solution/blob/master/documents/hmi_solution/ugfx/ugfx_guide_en.md
https://github.com/espressif/esp-iot-solution/tree/master/examples/hmi/ugfx_example

Guidance picked up from PGESP-2.frab:

```
#define SSD1306_I2C_BUS          I2C_NUM_0
// Not sure if there is some arduino pin mapping going on
#define ESP32_SDA1          GPIO_NUM_5
#define ESP32_SCL1          GPIO_NUM_4
#define ESP32_FREQ_HZ       1000000
//#define ESP32_I2C_BUF_DISABLE_FLAG  0

// Pulling from Adafruit lib, but not sure our D-Duino clone *actually* uses I2C
// for the SSD
#define SSD1306_I2C_ADDRESS   0x3C  // 011110+SA0+RW - 0x3C or 0x3D
// Address for 128x32 is 0x3C
// Address for 128x64 is 0x3D (default) or 0x3C (if SA0 is grounded)
```

# Problems

Can't even get boilerplate up, hitting similar issues as:
http://bbs.esp32.com/viewtopic.php?f=22&t=8437 
with `error: implicit declaration of function board_lcd_set_orientation'`