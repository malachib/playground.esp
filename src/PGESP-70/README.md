# Overview

Document v1

# 1. Scope

Attempt to bring up SSD1680 Crowpanel 2.13"

# 2. Infrastructure

## POC1

Espressif's esp_lcd_ssd1681 flavor [2]
Used also as a comparison point against Elecrow's Arduino examples [1.3]

## POC2

vroland EPDiy flavor, if we care

## POC3

Our own raw SPI version, primarily in support of partial updates

## Hardware

### IO

| IO    | Category  | Desc
| -     | -         | -
| 9     | disp      | BUSY
| 10    | disp      | RES
| 13    | disp      | DC
| 14    | disp      | CS
| 12    | disp      | SCLK
| 11    | disp      | MOSI
| 19    |           | LED
| 1     |           | EXIT
| 2     |           | MENU
| 4     |           | DOWN
| 5     |           | CONF
| 6     |           | UP
| 7     | power     | LCD_3.3_CTL

### Peripherals

Onboard also is a W25Q64JVXGIQ flash chip, presumably to feed ESP32S3 itself

### SPI commands

Copy/pasted from [1.3.1]

For SPI register 0x22 Display Update Control 2:

```
  A7 Enable internal clock oscillator                  使能内部时钟振荡器
  A6 Turn-on DC-Dc boost                               启动DC-DC升压
  A5 Read the temperature sensor                       读取温度传感器
  A4 Search and load the LUT from OTP                  搜索并从OTP加载LUT
  A3 Perform 0=DISPLAY Mode 1 1=DISPLAY Mode 2         控制EPD的显示模式
  A2 Perform image display (content in RAM)sequence    执行RAM中的图像显示
  A1 Turn-off DC-DC boost                              关闭DC-DC升压
  A0 Disable the internal clock oscillator             失能内部时钟振荡器
```

# 3. Observations

## 3.1. Aggregated

Observations gleaned from across sources

### 20JAN25

SSD1681 likes 1MHz SPI clock, command and param bits = 8 [2.1]
Special `epaper_panel_set_bitmap_color` is called per red and black [2.1] -
perhaps not needed for gray scale or pure b/w

As clearly indicated, `epaper_panel_refresh_screen(panel_handle)` is required for a true refresh to occur.

### 25JAN25

When comparing datasheeting for 1680 [1.4] and 1681 [2.2], we see nearly identical datasheets:

* Both have register 0x01 for "Driver Output control"
* Both have register 0x12 for "SW RESET"
* Both have register 0x22 for "Display Update Control 2" aka DUC
* Both have register 0x24 for "Write RAM"
* Both have register 0x31 for "Load WS OTP"
* Both have register 0x32 for "Write LUT register" - SSD1680 has additional XON setting
* Both have register 0x3C for "Border Waveform Control"

Deviation in driver approaches:

* Arduino [1.3.1] uses DUC:0xC7, 0xF4, 0xF7
* Espressif [2] uses DUC:0xB1, 0xCF

Quick ref table has been moved to section 4.2.2. as of 09FEB25

In context to 4.2.2.6, unexpected that enable analog disable analog can appear at the same time.  Perhaps it's a sequence of events?

I appreciate that Elecrow doc summarized bit behavior here.

### 01FEB25

We may be doing more updating than necessary since the default applies the red channel.  Also, LUT4 occupies space in LUT table, but is otherwise unmentioned.

### 09FEB25

No response on Elecrow forums yet.  Pinged.
ChatGPT gets a LOT of things wrong.  I think I may be hurting myself using it, I corrected it over 5 times today on SSD1680 datasheet operations - and I am a newcomer to this.

### 15FEB25

On 10FEB25 they [1.1.1] responded [1.1] and pretty much parroted back my question in the form of an answer.  But, they did indicate a useful email: techsupport3@elecrow.com

Posting a new question [1.5] asking about the full screen flash

### 16FEB25

No update [1.5] per se, though interestingly title has been modified to include "DIE01021S"

## 3.2. Purely for reference

[3] and [3.1] may be illuminating

## 3.3. Elecrow Discussion

From [1.1] we glean that Elecrow's example Arduino code:

* copy/pasted their own EPD support, not dependent on any other GL library

## 3.4. esp-idf ssd1681 component

Internal framebuffer hardcoded to 200x200 internal buffer size, matching SSD1681 specs.  SSD1680 is 176x296 spec
If using non-copy mode this is not a consideration.

### 3.4.1. LUT

#### 3.4.1.1. Overview

Also, the example code for this component tells us "If you are not using waveshare 1.54 inch V2 e-paper panel, please use the waveform lut provided by your panel vendor instead of using the demo built-in ones, or just simply comment the `epaper_panel_set_custom_lut` call and use the panel built-in waveform lut."

In other words: if in doubt, leave it alone.

#### 3.4.1.2. Breakdown

Identical behavior in both SSD1680 [1.2] and SSD1681 [2.2] datasheets.  Section 6.5 tells us LUT2 and LUT3 apply only to red channel.

### 3.4.2. copy mode

`process_bitmap` in particular hardcodes to 200x200.  It handles image mirroring and axis swapping.
It is only called in "copy mode" (vs non_copy_mode).  Therefore, our tests here almost definitely need non_copy_mode.

### 3.4.3. Initialization

Since SSD1680 does manage a different buffer size, init is a tiny bit different.  Namely, a different
MUX resolution is specified during Driver Output Control phase.  Note that this is not fully sufficient, a remaining ~10 vertical pixels remain uninitialized.

Making a fork of SSD1681 [2.3] to add SSD1680 support to it mainly for this reason

## 3.5. SSD1680 Datasheet

Datasheet [1.4] findings here.

Command `0x11` (`SSD1681_CMD_DATA_ENTRY_MODE`) supports rotation and axis control.  Cool!

Also, command `0x0F` (gate scan position) may help with partial updates on a row basis.

"Display Mode" seems to refer to dual page buffer "Ping-Pong" as per p.30, though only Display Mode 2 is indicated as actually supporting it.

# 4. Conclusions

## 4.1. Results

| Date      | Result
| -         | -
| 25JAN25   | Close to working, tux image displays.  Initial clear broken, seems to only go to 200 height

## 4.2. Quick Reference

### 4.2.1. Command Summary

| Value | res   | Name                          | Description
| -     | -     | -                             |
| 0x11  |       | "Data Entry mode setting"     |
| 0x20  |       | "Master Activation"           | Takes no extra parameters
| 0x21  |       |                               |
| 0x22  |       | "Display Update Control 2"    |
| 0x24  |       | Write RAM (B/W)               | SSD1681_CMD_WRITE_BLACK_VRAM (no params)
| 0x32  |       | Load LUT                      |
| 0x4E  |       |                               | SSD1681_CMD_SET_INIT_X_ADDR_COUNTER
| 0x4F  |       |                               | SSD1681_CMD_SET_INIT_Y_ADDR_COUNTER

### 4.2.2. Command Breakdown

#### 4.2.2.1. RESERVED

#### 4.2.2.2. SSD1681_CMD_DATA_ENTRY_MODE (0x11)

Sets up mirroring and axis

#### 4.2.2.3. RESERVED

#### 4.2.2.4. RESERVED

#### 4.2.2.5. SSD1681_CMD_ACTIVE_DISP_UPDATE_SEQ (command 0x20)

#### 4.2.2.6. SSD1681_CMD_SET_DISP_UPDATE_CTRL (command 0x22)

| Value | Bits      | Context   | Breakdown
| -     | -         | -         |
| 0x0F  | 00001111  | ChatGPT   | Power-saving Update (if supported)
| 0xB1  | 10110001  | Espressif | Load temp value, load LUT w/ disp mode 1, no disp, disable clock
| 0xC7  | 11000111  | Arduino   | Enable analog, disp mode 1, disable analog, disable OSC
| 0xCF  | 11001111  | Espressif | Enable analog, disp mode 2, disable analog, disable OSC
| 0xF4  | 11110100  | Arduino   | Enable analog, load temp value, disp mode 1
| 0xF7  | 11110111  | Arduino   | Enable analog, load temp value, disp mode 1, disable analog, disable osc (ChatGPT refers to this as "partial update")
| 0xFF  | 11111111  | ChatGPT   | Full Update


# Terminology

| Term          | Context | Description
| -             | -       | -          
| Display Mode  |       | 1 for full update, 2 for partial update
| FR[n]         |       | Frame rate of group n
| POR           |       | Power on reset default value (probably)
| LUT           |       | WaveForm Lookup Table
| OTP         |         | One Time Programmable
| RP[n]         |       | Repeat counting number
| SR[nAB]     |         | State repeat counting number for Phase A & B
| SR[nCD]       |       | 
| SR[nXY]       |       | Seems to be aggregated of nAB and nCD
| TP[nX]        |       | "phase length set by the number of frame"
| VBD           |       | Border Driving Signal
| VS[nX-LUTm]   |       | Source and VCOM voltage level
| WS            |       | WaveForm Settings
| XON[nXY]      |       |

# References

1. https://www.elecrow.com/crowpanel-esp32-2-13-e-paper-hmi-display-with-122-250-resolution-black-white-color-driven-by-spi-interface.html
    1. https://forum.elecrow.com/discussion/1055/esp32s3-2-13-e-paper-reference/
        1. https://forum.elecrow.com/profile/Jennie
    2. https://www.elecrow.com/download/product/DIE01021S/CrowPanel_ESP32_Display-2.13(E)_Inch.pdf
    3. https://github.com/Elecrow-RD/ESP32_S3-Ink-Screen
        1. https://github.com/Elecrow-RD/ESP32_S3-Ink-Screen/tree/main/ESP32_S3%20Ink%20Screen%20Information/2.13%20Inch_ESP32_S3/Example%20Code/2.13_Global_refresh
    4. https://www.elecrow.com/download/product/DIE01021S/SSD1680_Datasheet.pdf
    5. https://forum.elecrow.com/discussion/1096/esp32s3-2-13-e-paper-full-screen-refresh
2. https://components.espressif.com/components/espressif/esp_lcd_ssd1681
    1. https://github.com/espressif/esp-bsp/blob/30d2504a679c1cb0d10833f772d97453098b4c08/components/lcd/esp_lcd_ssd1681/examples/epaper_demo/main/main.c
    2. https://cdn-learn.adafruit.com/assets/assets/000/099/573/original/SSD1681.pdf
    3. https://github.com/malachi-iot/esp-bsp
3. https://github.com/ZinggJM/GxEPD2
    1. https://github.com/mbv/ssd1680