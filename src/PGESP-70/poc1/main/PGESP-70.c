#include <string.h>

#include <esp_log.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_lcd_panel_io.h"
#include "esp_lcd_panel_vendor.h"
#include "esp_lcd_panel_ssd1681.h"
#include "esp_lcd_panel_ops.h"
#include "driver/spi_common.h"
#include "driver/gpio.h"

// Chunks lifted wholesale from [1.2.1]

#include "img_bitmap.h"

// Adapted and specific to CrowPanel 2.13"

// SPI Bus
#define EPD_PANEL_SPI_CLK           1000000
#define EPD_PANEL_SPI_CMD_BITS      8
#define EPD_PANEL_SPI_PARAM_BITS    8
#define EPD_PANEL_SPI_MODE          0
// e-Paper GPIO
#define EXAMPLE_PIN_NUM_EPD_DC      13
#define EXAMPLE_PIN_NUM_EPD_RST     10
#define EXAMPLE_PIN_NUM_EPD_CS      14
#define EXAMPLE_PIN_NUM_EPD_BUSY    9
// e-Paper SPI
#define EXAMPLE_PIN_NUM_MOSI        11
#define EXAMPLE_PIN_NUM_SCLK        12

// CrowPanel specific LDO enable (it seems)
#define EPD_PANEL_POWER             7
#define EPD_PANEL_WIDTH             122
#define EPD_PANEL_HEIGHT            250

static const char* TAG = "PGESP-70::poc1";

static bool give_semaphore_in_isr(const esp_lcd_panel_handle_t handle, const void *edata, void *user_data)
{
    SemaphoreHandle_t *epaper_panel_semaphore_ptr = user_data;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(*epaper_panel_semaphore_ptr, &xHigherPriorityTaskWoken);
    if (xHigherPriorityTaskWoken == pdTRUE) {
        portYIELD_FROM_ISR();
        return true;
    }
    return false;
}

void app_main(void)
{
    esp_err_t ret;
    // --- Init SPI Bus
    ESP_LOGI(TAG, "Initializing SPI Bus...");
    spi_bus_config_t buscfg =
    {
        .sclk_io_num = EXAMPLE_PIN_NUM_SCLK,
        .mosi_io_num = EXAMPLE_PIN_NUM_MOSI,
        .miso_io_num = -1,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = SOC_SPI_MAXIMUM_BUFFER_SIZE
    };
    ESP_ERROR_CHECK(spi_bus_initialize(SPI2_HOST, &buscfg, SPI_DMA_CH_AUTO));

    // --- Init ESP_LCD IO
    ESP_LOGI(TAG, "Initializing panel IO...");
    esp_lcd_panel_io_handle_t io_handle = NULL;
    esp_lcd_panel_io_spi_config_t io_config = {
        .dc_gpio_num = EXAMPLE_PIN_NUM_EPD_DC,
        .cs_gpio_num = EXAMPLE_PIN_NUM_EPD_CS,
        .pclk_hz = EPD_PANEL_SPI_CLK,
        .lcd_cmd_bits = EPD_PANEL_SPI_CMD_BITS,
        .lcd_param_bits = EPD_PANEL_SPI_PARAM_BITS,
        .spi_mode = EPD_PANEL_SPI_MODE,
        .trans_queue_depth = 10,
        .on_color_trans_done = NULL
    };
    ESP_ERROR_CHECK(esp_lcd_new_panel_io_spi((esp_lcd_spi_bus_handle_t) SPI2_HOST, &io_config, &io_handle));

    // --- Create esp_lcd panel
    ESP_LOGI(TAG, "Creating SSD1681 panel...");
    esp_lcd_ssd1681_config_t epaper_ssd1681_config = {
        .busy_gpio_num = EXAMPLE_PIN_NUM_EPD_BUSY,
        // NOTE: Enable this to reduce one buffer copy if you do not use swap-xy, mirror y or invert color
        // since those operations are not supported by ssd1681 and are implemented by software
        // Better use DMA-capable memory region, to avoid additional data copy
        .non_copy_mode = true,
    };
    esp_lcd_panel_dev_config_t panel_config = {
        .reset_gpio_num = EXAMPLE_PIN_NUM_EPD_RST,
        .flags.reset_active_high = false,
        .vendor_config = &epaper_ssd1681_config
    };
    esp_lcd_panel_handle_t panel_handle = NULL;

    // "NOTE: Please call gpio_install_isr_service() manually before esp_lcd_new_panel_ssd1681()
    // because gpio_isr_handler_add() is called in esp_lcd_new_panel_ssd1681()" [2.1]
    gpio_install_isr_service(0);

    // CrowPanel specific
    gpio_set_direction((gpio_num_t)EPD_PANEL_POWER, GPIO_MODE_OUTPUT);
    gpio_set_level((gpio_num_t)EPD_PANEL_POWER, 1);

    ret = esp_lcd_new_panel_ssd1681(io_handle, &panel_config, &panel_handle);
    ESP_ERROR_CHECK(ret);
    // --- Reset the display
    ESP_LOGI(TAG, "Resetting e-Paper display...");
    ESP_ERROR_CHECK(esp_lcd_panel_reset(panel_handle));
    vTaskDelay(100 / portTICK_PERIOD_MS);

    // --- Initialize LCD panel
    ESP_LOGI(TAG, "Initializing e-Paper display...");
    ESP_ERROR_CHECK(esp_lcd_panel_init(panel_handle));
    vTaskDelay(100 / portTICK_PERIOD_MS);
    // Turn on the screen
    ESP_LOGI(TAG, "Turning e-Paper display on...");
    ESP_ERROR_CHECK(esp_lcd_panel_disp_on_off(panel_handle, true));

    static SemaphoreHandle_t epaper_panel_semaphore;
    epaper_panel_semaphore = xSemaphoreCreateBinary();
    xSemaphoreGive(epaper_panel_semaphore);

    const unsigned bufsize = EPD_PANEL_WIDTH * EPD_PANEL_HEIGHT / 8;

    uint8_t* const framebuffer = heap_caps_malloc(bufsize, MALLOC_CAP_DMA);

    assert(framebuffer);

    memset(framebuffer, 0, bufsize);

    // Red channel not considered for this part, though the SSD1680Z does support it
    epaper_panel_set_bitmap_color(panel_handle, SSD1681_EPAPER_BITMAP_BLACK);
    esp_lcd_panel_draw_bitmap(panel_handle, 0, 0, EPD_PANEL_WIDTH, EPD_PANEL_HEIGHT, framebuffer);

    // --- Register the e-Paper refresh done callback
    // cbs does not have to be static for ssd1681 driver, for the callback ptr is copied, not pointed
    epaper_panel_callbacks_t cbs = {
        .on_epaper_refresh_done = give_semaphore_in_isr,
    };
    epaper_panel_register_event_callbacks(panel_handle, &cbs, &epaper_panel_semaphore);

    // --- Draw full-screen bitmap
    ESP_LOGI(TAG, "Drawing bitmap...");
    ESP_LOGI(TAG, "Show image full-screen");

    xSemaphoreTake(epaper_panel_semaphore, portMAX_DELAY);
    //ESP_ERROR_CHECK(esp_lcd_panel_mirror(panel_handle, false, false));
    ESP_ERROR_CHECK(esp_lcd_panel_invert_color(panel_handle, false));
    //ESP_ERROR_CHECK(esp_lcd_panel_swap_xy(panel_handle, false));
    ESP_LOGI(TAG, "Drawing bitmap...");

    // NOTE: Kinda works, but it seems like DMA isn't quite done - we get odd artifacting
    //memcpy(framebuffer, BITMAP_64_128, 64 * 128 / 8);
    //ESP_ERROR_CHECK(esp_lcd_panel_draw_bitmap(panel_handle, 0, 0, 64, 128, framebuffer));
    ESP_ERROR_CHECK(esp_lcd_panel_draw_bitmap(panel_handle, 0, 0, 64, 128, BITMAP_64_128));
    ESP_ERROR_CHECK(epaper_panel_refresh_screen(panel_handle));
}
