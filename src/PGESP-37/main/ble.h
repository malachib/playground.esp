#pragma once

// DEBT: Get NimBLE dependencies in here

void ble_prph_advertise(
    const char* device_name,
    ble_gap_event_fn,
    const ble_uuid16_t* service_uuids,
    int service_uuids_count);

namespace nimble {

// EXPERIMENTAL

struct Peripheral
{
    const char* name_;
};

struct Advertise
{
    ble_gap_adv_params params {};

    void set_fields();
    void start(ble_gap_event_fn);
};



struct Service
{
};

}