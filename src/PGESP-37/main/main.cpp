#include <esp_log.h>
#include <nvs_flash.h>


static const char* TAG = "PGESP-37::main";

void init_ble();
int gatt_svr_init();
void start_ble();

extern "C" void app_main(void)
{
    ESP_LOGI(TAG, "main: entry");

    /* Initialize NVS — it is used to store PHY calibration data */
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }

    ESP_ERROR_CHECK(ret);

    init_ble();
    gatt_svr_init();
    start_ble();
}