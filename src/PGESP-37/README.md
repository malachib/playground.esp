# Overview

# 1. Design Goals

Task is dedicated to NimBLE
Starting with time service (CTS), plan to add in DIS and BAS

# 2. RESERVED

# 3. Opinions & Observations

## 3.1. Old Goals

Plan was to simulate GPS and/or illunating LED light

## 3.2. Compared to Bluedroid

### 3.2.1. Initial Impressions

So far, this seems a LOT more straightforward than Bluedroid.

### 3.2.2. Performance: RAM

There is conflicting information whether this uses more or less RAM [2.1], [2.2].
Espressif themselves claim "ESP-NimBLE requires less heap and flash size." [2]
During PGESP-49, I noticed a TON of dynamic allocation/free  with Bluedroid.  I conjecture there's less of that in NimBLE

### 3.2.3. Performance: ROM

It seems pretty universal that NimBLE uses a lot less ROM [2], [2.1], [2.2]

### 3.2.4. Perfomrance: Power draw

Unfair to make comparisons since this PGESP does nothing to optimize power usage.
See PGESP-49 poc2 for that.

Seeing 64ma rock steady draw from ESP32C6 hanging off CP2102

## 3.3. Extended Advertising

A BLE 5 feature [1].  Bears some investigation

## 3.4. Incidentals

Not directly related to our NimBLE stuff, but cool things I stumbled on during this PGESP.  Generally these are all mynewt things [3]

### 3.4.1. mynewt: os_mbuf

Looks to be similar to LwIP's netbuf/pbuf arrangement.  Very cool.

### 3.4.2. mynewt: LoRa PHY and LoRaWAN

Would be great to revisit this

### 3.4.3. mynewt: CoAP

Most CoAP implementations I'm underwhelmed with.  I'm a little more optimistic about this one

### 3.4.4. mynewt: BLE-MESH

One wonders if this is the implemention Espressif uses [4]

# References

1. https://github.com/espressif/esp-idf/tree/v5.1.3/examples/bluetooth/nimble/ble_cts/cts_prph
2. https://docs.espressif.com/projects/esp-idf/en/v5.2.1/esp32/api-guides/bluetooth.html 
    1. https://esp32.com/viewtopic.php?t=33783
    2. https://www.esp32.com/viewtopic.php?t=16094
3. https://mynewt.apache.org/
4. https://docs.espressif.com/projects/esp-idf/en/v5.2.1/esp32/api-guides/esp-ble-mesh/ble-mesh-index.html