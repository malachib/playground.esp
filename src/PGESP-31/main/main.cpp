#include <estd/thread.h>
#include <estd/iostream.h>

#include "esp_system.h"
#include "esp_log.h"

void initialize();

extern "C" void app_main(void)
{
    static const char* TAG = "PGESP-31: app_main";

    ESP_LOGI(TAG, "Bringup");

    initialize();

    for(;;)
    {
        static int counter = 0;

        estd::this_thread::sleep_for(estd::chrono::seconds(1));

        ESP_LOGD(TAG, "counter=%d", ++counter);
    }
}