#include <estd/locale.h>
#include <estd/port/freertos/timer.h>

#include <embr/platform/esp-idf/timer.h>
#include <embr/platform/esp-idf/log.h>

using namespace estd::chrono_literals;


static void timer_callback(TimerHandle_t t)
{
    static const char* TAG = "timer_callback";

    estd::freertos::internal::timer timer(t);

    ESP_LOGI(TAG, "entry");
}

static embr::esp_idf::Timer timer(TIMER_GROUP_0, TIMER_1);
static estd::freertos::timer<> f_timer("test timer", 3s, false, nullptr, timer_callback);
// overengineering this part a little
static estd::chrono::milliseconds elapsed;

static void isr_utility_function(BaseType_t* higherPriorityTaskWoken)
{
    //estd::internal::basic_istream<layer1::stringbuf<32>> fmt;
    constexpr int sz = 1024;
    char stack_load[sz];
    static const char* TAG = "isr_utility_function";

    // DEBT: wanted to use num_put but I never got that far with estd.  to_chars is more than up
    // to the task, however
    //num_get<char, const char*> n;
    //n.get(in, in + strlen(in), fmt, state, v);

    estd::to_chars(stack_load, stack_load + sz, elapsed.count());

    stack_load[sz - 1] = 0; // just to aggrevate any stack problems

    BaseType_t retval = f_timer.reset_from_isr(higherPriorityTaskWoken);

    ESP_DRAM_LOGI(TAG, "elapsed: %s / %d", stack_load, retval);
}

// Reach here once per ms
static bool IRAM_ATTR isr_handler (void* arg)
{
    //static const char* TAG = "isr_handler";

    elapsed += 1ms;

    // DEBT: Put this suite of operators into estd
    //++elapsed;

    BaseType_t higherPriorityTaskWoken = false;

    /*
     * FIX: We get an operator *= compile error
    if(elapsed > 10s) */
    // NOTE: This doesn't appear yet
    if(elapsed == 5s)
    {
        isr_utility_function(&higherPriorityTaskWoken);
        elapsed = 0ms;
    }

    return higherPriorityTaskWoken == pdTRUE;
}

void initialize()
{
    static const char* TAG = "timer_scheduler_init";
    void* arg = nullptr;

    ESP_LOGI(TAG, "group=%d, idx=%d, arg=%p", timer.group, timer.idx, arg);

    timer_config_t config;

    // Typically clock runs at 80MHz, meaning this divider results in 1MHz timer ticks
    config.divider = 80;

    config.counter_dir = TIMER_COUNT_UP;
    config.alarm_en = TIMER_ALARM_EN;
    config.intr_type = TIMER_INTR_LEVEL;
    //config.auto_reload = TIMER_AUTORELOAD_DIS;
    config.auto_reload = TIMER_AUTORELOAD_EN;   // Reset timer to 0 when end condition is triggered
    config.counter_en = TIMER_PAUSE;            // Start paused so that we can add isr callback
#if SOC_TIMER_GROUP_SUPPORT_XTAL
    config.clk_src = TIMER_SRC_CLK_APB;
#endif
    timer.init(&config);

    timer.set_counter_value(0);
    timer.set_alarm_value(1000);            // 1000us = 1ms
    // isr_callback_add cannot precede init
    timer.isr_callback_add(isr_handler, arg, ESP_INTR_FLAG_LEVEL1 | ESP_INTR_FLAG_IRAM);
    //timer.enable_intr();  // Looks like intr_type *probably* sets this already
    timer.start();

    f_timer.start(100ms);
}
