#include <estd/thread.h>

#include <mdns.h>

#include "esp_log.h"

#if ESTD_IDF_VER < ESTD_IDF_VER_3_0_0
#error mDNS not available in ESP8266 idf-version < 3.0.0
#endif

static void setup_mdns()
{
    static const char* TAG = "setup_mdns";

    //structure with TXT records
    mdns_txt_item_t serviceTxtData[3] = {
        {"board","esp32"},
        {"u","user"},
        {"p","password"}
    };

    // guidance here https://github.com/espressif/ESP8266_RTOS_SDK/blob/master/examples/protocols/mdns/main/mdns_example_main.c
    // not yet adding services
    ESP_ERROR_CHECK( mdns_init() );
    ESP_LOGI(TAG, "Set mDNS hostname=%s", CONFIG_MDNS_HOSTNAME);
    ESP_ERROR_CHECK( mdns_hostname_set(CONFIG_MDNS_HOSTNAME) );
    ESP_ERROR_CHECK( mdns_instance_name_set(CONFIG_MDNS_INSTANCE) );
    // lie and say we have a web server running, just so we can register ourselves
    ESP_ERROR_CHECK( mdns_service_add("ESP32-WebServer", "_http", "_tcp", 80, serviceTxtData, 3) );
    //add another TXT item
    ESP_ERROR_CHECK( mdns_service_txt_item_set("_http", "_tcp", "path", "/foobar") );
    //change TXT item value
    ESP_ERROR_CHECK( mdns_service_txt_item_set("_http", "_tcp", "u", "admin") );
}

// NOTE:
// mdns_handle_system_event looks like it wants to hang off wifi's event_handler
// so keep an eye on things being that we aren't calling that
// NOTE:
// example https://github.com/espressif/ESP8266_RTOS_SDK/blob/master/examples/protocols/mdns/main/mdns_example_main.c
// doesn't play these games with got_ip_event it just fires off mdns init right away, so that suggests that
// we do need to make that mdns_handle_system_event call
extern "C" void got_ip_event() 
{
    setup_mdns();
}

extern "C" void test_task(void*)
{
    static const char* TAG = "test_task";

    for(;;)
    {
        estd::this_thread::sleep_for(estd::chrono::seconds(10));

        static int counter = 0;

        ESP_LOGI(TAG, "Waiting...%d", counter++);
    }
}