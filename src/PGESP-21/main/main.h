#pragma once

#include <driver/gpio.h>

// remember, for ext1:
// "Only GPIOs which are have RTC functionality 
//  can be used in this bit map: 0,2,4,12-15,25-27,32-39"
constexpr gpio_num_t wake_pin1 = (gpio_num_t) CONFIG_WAKEUP_PIN_1;
constexpr gpio_num_t wake_pin2 = (gpio_num_t) CONFIG_WAKEUP_PIN_2;

void setup_gpio();