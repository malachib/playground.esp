#include "esp_log.h"
#include "nvs_flash.h"

extern "C" void app_main(void)
{
    static const char *TAG = "app_main";

    esp_err_t ret = nvs_flash_init();

    ESP_LOGI(TAG, "started");
}