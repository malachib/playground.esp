#include "main.h"

// ISR-driven GPIO good example
// https://github.com/espressif/esp-idf/blob/master/examples/peripherals/gpio/main/gpio_example_main.c
//static xQueueHandle gpio_evt_queue = NULL;

static void IRAM_ATTR gpio_isr_handler(void* arg)
{
    //uint32_t gpio_num = (uint32_t) arg;
    //xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}


void setup_gpio()
{
    //hook isr handler for specific gpio pin
    // NOTE: Posted request on esp32.com forums regarding this specifically
    gpio_isr_handler_add(wake_pin1, gpio_isr_handler, (void*) wake_pin1);
}