#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <estd/chrono.h>
#include <estd/thread.h>

#include <esp_attr.h>
#include <esp_sleep.h>

#include <driver/rtc_io.h> // for rtc_gpio_deinit

#include "main.h"

// example code does something similar to our own:
// https://github.com/espressif/esp-idf/blob/master/examples/system/deep_sleep/main/deep_sleep_example_main.c
// NOTE: VERY interesting to use touch peripheral to wakeup, but not what we're going to
// do here in PGESP-21

// as per https://docs.espressif.com/projects/esp-idf/en/latest/api-guides/deep-sleep-stub.html
void RTC_IRAM_ATTR esp_wake_deep_sleep(void) {
    esp_default_wake_deep_sleep();
    // Add additional functionality here

    // TODO: assert that we're woken up by EXT1 , shouldn't be woken up by
    // anything else
    /*
    switch(esp_sleep_get_wakeup_cause())
    {
        case ESP_SLEEP_WAKEUP_EXT1:
            break;
    } */

    // will tell us which pins woke us from ext1 deep sleep
    uint64_t gpios = esp_sleep_get_ext1_wakeup_status();

    // as described in https://github.com/espressif/esp-idf/blob/master/examples/system/deep_sleep/main/deep_sleep_example_main.c
    if(gpios & 1ULL << wake_pin1)
    {

    }
    if(gpios & 1ULL << wake_pin2)
    {

    }


    rtc_gpio_deinit(wake_pin1);
    rtc_gpio_deinit(wake_pin2);

    // FIX: Need to be 100% sure that we can actually do this here.  80% sure right now
    // some minor exploration of that here https://www.esp32.com/viewtopic.php?t=1674
    setup_gpio();
}

extern "C" void got_ip_event()
{
    
}

extern "C" void test_task(void*)
{
    estd::this_thread::sleep_for(estd::chrono::seconds(2));

    // no need for infinite loop, because esp_deep_sleep_start effectively
    // terminates everything
    //for(;;) vTaskDelay(2);

    // bit mask of GPIOs we want to be triggered by for wakeup
    constexpr uint64_t gpios_to_listen_to = 
        (1ULL << wake_pin1) |
        (1ULL << wake_pin2);

    esp_sleep_enable_ext1_wakeup(gpios_to_listen_to, ESP_EXT1_WAKEUP_ANY_HIGH);
    esp_deep_sleep_start();
}


