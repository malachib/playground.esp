# Overview

# 1. Scope

When attempting to consume a primary component who in turn hosts sub-components, things get dicey.  In particular, a path not found issue occurs (4.1.1)

# 2. Infrastructure

## 2.1. Libraries

Remember, lib1 and lib2 consume shared_lib, not the other way around

### 2.1.1. lib1

### 2.1.2. lib2

### 2.1.3. shared_lib

## 2.2. Apps

### 2.2.1. app_local

Consumes component directly via path
Does NOT exhibit 4.1.1. issue

### 2.2.2. app_remote

Consumes component via URL (point git back to playground repo).
Exhibits 4.1.1. issue

# 3. Opinions & Observations

## 3.1. Journal

### 10FEB25

I don't know why 4.1.1. shows - the `idf_component.yml` to which it refers specifies no path.
Asked for assistance from esp component folks [1]

### 15FEB25

No update

### 16FEB25

No update

# 4. Data Points & Conclusions

## 4.1. Problem Domain

### 4.1.1. path not found

```
ERROR: The "path" field in the manifest file
  "/tmp/tmpco5pk05l/src/PGESP-74/managed/components/lib1/idf_component.yml"
  does not point to a directory.  You can safely remove this field from the
  manifest if this project is an example copied from a component repository.

  The dependency will be downloaded from the ESP component registry.
  Documentation:
  https://docs.espressif.com/projects/idf-component-manager/en/latest/reference/manifest_file.html#override-path
```

# References

1. https://github.com/espressif/idf-component-manager/issues/82