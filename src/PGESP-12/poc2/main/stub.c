#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "driver/gpio.h"
#include "blink.h"

void udp_echo_init();
void blinkenTask(void *pvParameters);

void stub()
{
#if !CONFIG_FREERTOS_USE_TICKLESS_IDLE
#error Tickless idle must be enabled
#endif

    gpio_pad_select_gpio(GPIO_LED);

    udp_echo_init();
    xTaskCreate(blinkenTask, "blinkenTask", 512, NULL, 2, NULL);
    printf("Initialized blink and echo: blink on GPIO %d\n", CONFIG_BLINK_GPIO);
}

int main()
{
    return 0;
}