#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#if ESTD_IDF_VER >= ESTD_IDF_VER_2_0_0_644
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#else
#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"
#endif

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"
#include "airkiss.h"

#include <estd/port/identify_platform.h>

#include <string.h>


extern "C" void udp_echo_init();

#define	FPM_SLEEP_MAX_TIME      0xFFFFFFF

#define EXP2

void wakeup_cb(void)
{
    wifi_fpm_close();									//	disable	force	sleep	function

#ifdef EXP2
    wifi_set_opmode(STATION_MODE);						//	set	station	mode

    /* This initiates a lengthy DHCP acquisition phase.  Destroys power
       savings
    struct station_config sta_config;
    memset(&sta_config, 0, sizeof(struct station_config));
    memcpy(sta_config.ssid, CONFIG_WIFI_SSID, strlen(CONFIG_WIFI_SSID));
    memcpy(sta_config.password, CONFIG_WIFI_PASSWORD, strlen(CONFIG_WIFI_PASSWORD));
    wifi_station_set_config(&sta_config);    
    */

    wifi_station_connect();		
#endif																						//	connect	to	AP
}

// from https://bbs.espressif.com/viewtopic.php?t=133
// also https://bbs.espressif.com/viewtopic.php?f=7&t=1134&start=10
void ICACHE_FLASH_ATTR
save_power() 
{
    // 90% unsure if DTIM detection works after wifi_station_disconnect
    // getting some indications it does not.  This guy is asking the
    // same question https://bbs.espressif.com/viewtopic.php?t=2276 
#ifdef EXP2
    wifi_station_disconnect();
    wifi_set_opmode(NULL_MODE); // set WiFi mode to null mode
    //wifi_set_opmode_current(NULL_MODE);
    os_delay_us(1000);
#endif
    wifi_fpm_set_sleep_type(LIGHT_SLEEP_T); // set modem sleep
    wifi_fpm_open(); // enable force sleep
    wifi_fpm_set_wakeup_cb(wakeup_cb);
    wifi_fpm_do_sleep(1 * 1000 * 1000);
}


// was going to experiment with
// https://github.com/espressif/esp8266-nonos-sample-code/blob/master/01System/Light_Sleep_Demo/user/user_main.c
// But it appears these are no longer available
//static struct espconn udp_client;
//static esp_udp udp;

void espconn_init()
{

}