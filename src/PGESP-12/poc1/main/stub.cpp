#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"


extern "C" void udp_echo_init();

void save_power();
void espconn_init();


extern "C" void testTask(void*) 
{
    // with some guidance from https://community.blynk.cc/t/esp8266-light-sleep/13584/9
    wifi_set_sleep_type(LIGHT_SLEEP_T);

    udp_echo_init();

    espconn_init();

    int counter = 0;
        
    for(;;)
    {
        //save_power();
        vTaskDelay(20000 / portTICK_PERIOD_MS);
        printf("Resting...%d\n", counter++);
    }
}