We're getting some missing bytes.  I don't think it's just an artifact
of how tcpdump reports things, since the udpecho.py also never receives
anything back

tcpdump output:

```
00:09:21.434673 IP (tos 0x0, ttl 64, id 11414, offset 0, flags [none], proto UDP (17), length 32)
    192.168.2.2.49738 > 192.168.2.179.echo: [udp sum ok] UDP, length 4
	0x0000:  5ccf 7f3a 08ba 58b0 3561 d381 0800 4500  \..:..X.5a....E.
	0x0010:  0020 2c96 0000 4011 c831 c0a8 0202 c0a8  ..,...@..1......
	0x0020:  02b3 c24a 0007 000c 5342 3132 330a       ...J....SB123.
00:09:21.636659 IP truncated-ip - 4 bytes missing! (tos 0x0, ttl 255, id 5, offset 0, flags [none], proto UDP (17), length 32)
    192.168.2.179.echo > 192.168.2.2.49738: UDP, length 4
	0x0000:  58b0 3561 d381 5ccf 7f3a 08ba 0800 4500  X.5a..\..:....E.
	0x0010:  0020 0005 0000 ff11 35c2 c0a8 02b3 c0a8  ........5.......
	0x0020:  0202 0007 c24a 000c 5342  
```

EXP1 mode fixes that

# Troubles

Not working like I'd like it to.  Even though documentation says DTIM polling is enabled during light-sleep, I don't receive UDP packets following the example sleep code given. 

That makes sense since the example code all actually shuts off the WiFi
completely.  In that mode, I do get <1ma usage.  But no WiFi or DTIM!

If I don't do that, then power usage fluxuates (some
kind of power savings is attempting to happen) but it bounces between
25 and 70 ma.  

Perhaps we can use beacon_interval? https://github.com/espressif/ESP8266_RTOS_SDK/blob/1e294d110262625d6e8df0b4c0ceb5fe9f2f6b58/components/esp8266/include/esp_wifi_types.h
Don't know, default should be good
already

Expected performance: https://blog.creations.de/?p=149