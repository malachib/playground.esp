#include <estd/port/identify_platform.h>

#if ESTD_IDF_VER >= ESTD_IDF_VER_2_0_0_644
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#else
#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"
#endif

#include <estd/string.h>

#if ESTD_IDF_VER > ESTD_IDF_VER_2_0_0_444
void wifi_setup(void)
{
    tcpip_adapter_init();
    //ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));
    
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    /*
    wifi_config_t wifi_config = {
	.sta = {
	    .ssid = CONFIG_WIFI_SSID,
	    .password = CONFIG_WIFI_PASSWORD,
	    .listen_interval = CONFIG_WIFI_LISTEN_INTERVAL,
	},
    }; */
    wifi_config_t wifi_config;

    // just fiddling around.  assignment oddly gives a warning even though
    // underlying code (seems to) be expecting a const char*
    estd::layer2::string<> ssid((char*)wifi_config.sta.ssid);
    //ssid = CONFIG_WIFI_SSID;

    strcpy((char*)wifi_config.sta.ssid, CONFIG_WIFI_SSID);
    strcpy((char*)wifi_config.sta.password, CONFIG_WIFI_PASSWORD);

#ifdef CONFIG_WIFI_LISTEN_INTERVAL
    wifi_config.sta.listen_interval = CONFIG_WIFI_LISTEN_INTERVAL;
#endif

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
}
#else
// NOTE: Copy/pasted from elsewhere but not tested in this context
void wifi_setup()
{
    struct ip_info ip_config;
    struct station_config sta_config;
    memset(&sta_config, 0, sizeof(struct station_config));
    wifi_set_opmode(STATION_MODE);
    memcpy(sta_config.ssid, CONFIG_WIFI_SSID, strlen(CONFIG_WIFI_SSID));
    memcpy(sta_config.password, CONFIG_WIFI_PASSWORD, strlen(CONFIG_WIFI_PASSWORD));
    wifi_station_set_config(&sta_config);

    wifi_station_disconnect();
    wifi_station_connect();
}
#endif