#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include <estd/port/identify_platform.h>

#if ESTD_IDF_VER >= ESTD_IDF_VER_2_0_0_644
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#else
#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"
#endif

extern "C" void udp_echo_init();

void save_power();
void espconn_init();
void wifi_setup();

extern "C" __attribute__((weak)) void got_ip_event() {}

static const char* TAG = "stub";

extern "C" void test_task(void*) 
{
    ESP_LOGI(TAG, "Starting up test_task");

#ifdef CONFIG_SLEEP_ENABLE
#if ESTD_IDF_VER <= ESTD_IDF_VER_2_0_0_444
    // with some guidance from https://community.blynk.cc/t/esp8266-light-sleep/13584/9
    wifi_set_sleep_type(LIGHT_SLEEP_T);
#else
    // esp-idf style
    wifi_setup();

    esp_wifi_set_ps(WIFI_PS_MIN_MODEM);
#endif
#endif

    udp_echo_init();

    //espconn_init();

    int counter = 0;
        
    for(;;)
    {
        //save_power();
        vTaskDelay(20000 / portTICK_PERIOD_MS);
        printf("Resting...%d\n", counter++);
    }
}