#include <FreeRTOS.h>
#include "task.h"

void udp_echo_init();

void testTask(void* p)
{
    udp_echo_init();

    for(;;)
    {
        taskYIELD();
    }
}