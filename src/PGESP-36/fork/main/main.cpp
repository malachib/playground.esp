#include <epd_driver.h>
#include <epd_highlevel.h>

#include <embr/platform/esp-idf/gpio.h>


/**
 * Upper most button on side of device. Used to setup as wakeup source to start from deepsleep.
 * Pinout here https://ae01.alicdn.com/kf/H133488d889bd4dd4942fbc1415e0deb1j.jpg
 */
gpio_num_t FIRST_BTN_PIN = GPIO_NUM_39;

void init_display()
{
    epd_init(EPD_OPTIONS_DEFAULT);
    EpdiyHighlevelState hl = epd_hl_init(EPD_BUILTIN_WAVEFORM);
}

extern "C" void app_main(void)
{
    init_display();
}