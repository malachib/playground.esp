# Overview

# Detail

Goal is to draw *ANY* graphics with Lilyo T5 E-Ink v2.3[^9]
ESP-IDF approach is desired.  platformio is used extensively by authors,
which is acceptable to start, but ESP-IDF SHOULD be used to finish.

## 1. Apps & Libraries

These libraries are considered, not chosen.  Too much chaos so far
to arrive at a conclusion

### 1.1. lib: CalEPD

For CalEPD, "support is already merged on master"[^3]

### 1.2. lib: epdiy

"supports a driver for the ESP-IDF"[^2]

### 1.3. app: main (poc1)

### 1.4. app: fork

### 1.5. app: poc2

### 1.6. app: poc3

### 1.3. lvgl_epaper_drivers

### 1.4. lv_port_esp32-epaper

## 2. Notes

Did not seem there was serious LVGL support, but `lvgl_epaper_drivers` may change that.

Just for experimentation, including LVGL stock esp32 drivers[^5] though we aren't directly interested since they don't
seem to include e-paper

## 3. Observations & Troubleshooting

### 3.1. epdiy: ESP32S3 support

`epdiy` library in its current state uses low level/not published i2s API that is incompatible with ESP32S3.[^6].
Martin F[^8] has a fork which has some progress there[^7] but doesn't seem to be ready yet

### 3.2. Lilygo canonical

#### 3.2.1. Warnings

"Due to the problem of the characteristics of the ink screen, it cannot be partially refreshed for a long time, otherwise, it will leave residual images and lead to irreversible damage."[^9]

### 3.2.2. Recommendations

Lilygo's own site uses vroland's library[^10]

### 3.3. Martin Berlin, resident hero

Although I am not experiencing a ton of personal success, this dude is
a fountain of e paper support code. 

His top level LVGL fork [^11] reaches into submodule [^4] for epaper drivers 

## 4. Sub/Side Projects

### 4.1. fork

Since nobody has working esp32s3 support.  I have to rabbit hole and fork Martin F's repo and try to fix it.

### 4.2. Try vroland's epdiy directly

Just to bring up part[^2] try:

* examples/demo - ESP-IDF native
* examples/lilygo-t5-47-epd-platformio

Not sure either will work with the S3 variant

# References

[^1]: https://epdiy.readthedocs.io/en/latest/getting_started.html
[^2]: https://github.com/vroland/epdiy
[^3]: https://github.com/martinberlin/CalEPD
[^4]: https://github.com/martinberlin/lvgl_epaper_drivers
[^5]: https://github.com/lvgl/lvgl_esp32_drivers
[^6]: https://github.com/vroland/epdiy/issues/203
[^7]: https://github.com/martinberlin/epdiy-rotation/tree/lilygos3
[^8]: https://github.com/martinberlin
[^9]: https://www.lilygo.cc/products/t5-4-7-inch-e-paper-v2-3
[^10]: https://github.com/Xinyuan-LilyGO/LilyGo-EPD47/tree/esp32s3
[^11]: https://github.com/martinberlin/lv_port_esp32-epaper