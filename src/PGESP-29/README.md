# Background

Guidance from:

* https://github.com/espressif/esp-idf/blob/v4.4.0/examples/system/console/basic/main/console_example_main.c 
* https://github.com/espressif/esp-idf/blob/v4.4.1/examples/system/console/basic/main/console_example_main.c 

Couldn't find in any of the examples the necessary reference to `fatfs` component.  Fortunately I figured it out from elsewhere.

## Results

| Date    | esp-idf | Result
|-------- | ------- | ------
| 23MAY22 | v4.4.0  | Compiles