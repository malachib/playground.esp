// NOTE: This stuff is so useful we might consider putting it into embr

template <typename TFirst, typename TSecond, unsigned N>
TSecond get_second(const estd::pair<TFirst, TSecond> (&mappings)[N], TFirst key)
{
    for(const auto pair : mappings)
        if(pair.first == key) return pair.second;

    return TSecond();
}



template <typename TMapping, unsigned N>
const char* get_name(const estd::pair<TMapping, const char*> (&mappings)[N], TMapping key)
{
    return get_second(mappings, key);
}


template <typename TMapping, unsigned N>
const char* get_name(const estd::pair<TMapping, const char*> (&mappings)[N], TMapping key, const char* default_name)
{
    const char* name = get_second(mappings, key);

    return name == nullptr ? default_name : name;
}

// TODO: Come up with a case insensitive char_traits
// https://vorbrodt.blog/2021/04/10/case-insensitive-string-etc/ this guys on the right track
bool iequals(const char* a, const char* a_end, const char* b)
{
    unsigned int sz = strlen(b);
    if (a_end - a != sz)
        return false;
    for (; a != a_end; ++a, ++b)
        if (tolower(*a) != tolower(*b))
            return false;
    return true;
}

template <typename TMapping, unsigned N>
estd::optional<TMapping> get_key(const estd::pair<TMapping, const char*> (&mappings)[N], const char* name)
{
    //estd::ctype<char, estd::internal::classic_locale_type>::is
    const char* name_end = name + strlen(name);
    for(const auto pair : mappings)
    {
        if(iequals(name, name_end, pair.second)) return pair.first;
    }

    return estd::nullopt;
}

template <unsigned N>
const char* match(const char* const (&strings)[N], const char* match_to)
{
    const char* match_to_end = match_to + strlen(match_to);

    for(const char* name : strings)
        if(iequals(match_to, match_to_end, name)) return name;

    return nullptr;
}

template <typename TIt, typename TPredicate>
inline void for_each_more(TIt first, TIt last, TPredicate p)
{
    // Have to forcefully abort otherwise we'd always call
    // the trailing p()
    if(first == last)   return;

    TIt i = first;
    --last;
    for(; i < last; ++i)
        p(*i, true);

    p(*i, false);
}

template <typename TStringImpl, unsigned N>
void concat(const char* const (&strings)[N], const char* delimiter,
    // DEBT: This would be better, but += / append not playing nice at the moment
    //estd::internal::dynamic_array<TStringImpl>& out)
    TStringImpl& out)
{
    for_each_more(strings, &strings[N], [&](const char* v, bool more)
    {
        out += v;

        if(more) out += delimiter;
    });
}


template <typename TMapped, typename TStringImpl, unsigned N>
void concat_names(const estd::pair<TMapped, const char*> (&mapped)[N], const char* delimiter,
    // DEBT: This would be better, but += / append not playing nice at the moment
    //estd::internal::dynamic_array<TStringImpl>& out)
    TStringImpl& out)
{
    for_each_more(mapped, &mapped[N], [&](const estd::pair<TMapped, const char*> v, bool more)
    {
        out += v.second;

        if(more) out += delimiter;
    });
}

