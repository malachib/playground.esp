#pragma once

#define MOUNT_PATH "/data"
// NOTE: Nifty, existing 'history.txt' stays intact in partition from esp-idf example code.
// NOTE: It appears subdirectories and long filenames don't work here
#define HISTORY_PATH MOUNT_PATH "/PGESP_29.txt"

