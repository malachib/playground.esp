#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void initialize();
void initialize_console();
void initialize_spi();

#ifdef __cplusplus
}
#endif