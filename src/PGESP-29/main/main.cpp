#include "esp_system.h"
#include "esp_log.h"
#include "init.h"

extern "C" void app_main(void)
{
    static const char* TAG = "app_main";

    ESP_LOGI(TAG, "PGESP-29 Bringup");

    initialize();
}