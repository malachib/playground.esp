#include "esp_system.h"
#include "esp_log.h"

// Console specific stuff
#include "esp_console.h"
#include "linenoise/linenoise.h"
#include "argtable3/argtable3.h"

#include <embr/platform/esp-idf/spi.h>

#include <string>

#include "utility.hpp"

static const char TAG[] = "PGESP-29::spi";

using namespace embr::esp_idf;

static spi::bus bus(SPI2_HOST);

static struct {
    struct arg_str *command;
    struct arg_str *host;
    struct arg_end *end;
} spi_bus_args;

static struct {
    struct arg_str *command;
    struct arg_end *end;
} spi_device_args;

static struct {
    struct arg_str *command;
    struct arg_str *target;
    struct arg_str *value;
    struct arg_end *end;
} spi_config_args;

static int spi_device(int argc, char **argv)
{
    ESP_LOGI(TAG, "Fake device action");
    return 0;
}

namespace commands {

constexpr const char
    set[] = "set",
    get[] = "get",
    select[] = "select",
    reset[] = "reset";

constexpr const char
    bus[] = "bus",
    device[] = "device";

constexpr const char
    init[] = "init",
    free[] = "free",
    configure[] = "configure";

namespace spi_config {

constexpr const char* list[] = { set, get, reset, select };

constexpr const char* select_targets[] = { bus, device };

// TODO: Pull in nvs example code for its type knowledge, we need to do something similar.
// That said, estd typeid shenanigans may be more interesting
constexpr const char* bus_targets[] =
{
    "hz"
};

}

namespace spi_bus {

constexpr const char* list[] = { init, free, select };

}

namespace spi_device {

constexpr const char* list[] = { configure, select };

}

}


static constexpr const estd::pair<spi_host_device_t, const char*> spi_host_device_names[] = {
    { SPI1_HOST, "SPI1" },
    { SPI2_HOST, "SPI2" },
    { SPI3_HOST, "SPI3" },
};

template <class TArgs>
static bool _arg_parse(int argc, char **argv, TArgs* args)
{
    int nerrors = arg_parse(argc, argv, (void **) &args);
    if (nerrors != 0)
    {
        arg_print_errors(stderr, spi_bus_args.end, argv[0]);
        return false;
    }
    return true;
}


static int spi_bus(int argc, char **argv)
{
    if(!_arg_parse(argc, argv, &spi_bus_args)) return 1;

    const char* command =
        match(commands::spi_bus::list, spi_bus_args.command->sval[0]);
    bool host_arg_present = spi_bus_args.host->count == 1;

    auto host_id = host_arg_present ?
                get_key(spi_host_device_names, spi_bus_args.host->sval[0]) :
                estd::nullopt;

    ESP_LOGD(TAG, "Bus action: command=%s, host_id matched=%d, host_id value=%d",
             command ? command : "no match",
             host_id.has_value(),
             host_id.value()
             );

    if(!host_id.has_value() && host_arg_present)
    {
        static estd::layer1::string<32> host_glossary;

        concat_names(spi_host_device_names, ", ", host_glossary);

        printf("Host ID must be one of: %s\n", host_glossary.data());
        return 1;
    }

    if(command == commands::init)
    {
        spi_bus_config_t c {

        };
        bus.initialize(c, SPI_DMA_CH_AUTO);
    }
    else if(command == commands::free)
    {
        bus.free();
    }
    else if(command == commands::select)
    {
        const char* current_host_name = get_name(spi_host_device_names, bus.host_id, "(null)");

        if(host_id.has_value())
        {
            const char* selected_host_name = get_name(spi_host_device_names, *host_id);

            printf("Switching selected bus from %s to %s\n", current_host_name, selected_host_name);

            new(&bus) spi::bus(*host_id);
        }
        else
        {
            printf("Current bus is %s\n", current_host_name);
        }
    }

    return 0;
}


static int spi_config(int argc, char **argv)
{
    if(!_arg_parse(argc, argv, &spi_config_args)) return 1;

    const char* command =
        match(commands::spi_config::list, spi_config_args.command->sval[0]);

    ESP_LOGI(TAG, "spi_config: command=%s", command ? command : "(null)");

    if(command == commands::select)
    {
        const char* target = match(commands::spi_config::select_targets, spi_config_args.target->sval[0]);
    }
    else if(command == commands::set)
    {

    }
    else if(command == commands::get)
    {

    }

    return 0;
}

namespace registrar { namespace spi {



static void device_init(void)
{
    static estd::layer1::string<64> glossary = "SPI device command: ";

    concat(commands::spi_device::list, ", ", glossary);

    spi_device_args.command = arg_str1(NULL, NULL, "<command>", glossary.data());
    spi_device_args.end = arg_end(1);

    const esp_console_cmd_t cmd = {
        .command = "spi-device",
        .help = "SPI device control",
        .hint = NULL,
        .func = &spi_device,
        .argtable = &spi_device_args,
    };
    ESP_ERROR_CHECK( esp_console_cmd_register(&cmd) );
}

static void bus_init(void)
{
    static estd::layer1::string<64> command_glossary = "SPI bus command: ";
    static estd::layer1::string<64> host_glossary = "Which SPI host: ";

    concat(commands::spi_bus::list, ", ", command_glossary);
    concat_names(spi_host_device_names, ", ", host_glossary);

    spi_bus_args.command = arg_str1(NULL, NULL, "<command>", command_glossary.data());
    spi_bus_args.host = arg_strn(NULL, NULL, "<spi_host_device_t>", 0, 1,host_glossary.data());
    spi_bus_args.end = arg_end(2);

    const esp_console_cmd_t cmd = {
        .command = "spi-bus",
        .help = "SPI host bus control",
        .hint = NULL,
        .func = &spi_bus,
        .argtable = &spi_bus_args,
    };
    ESP_ERROR_CHECK( esp_console_cmd_register(&cmd) );
}

static void config_init(void)
{
    static estd::layer1::string<64> command_glossary = "SPI config command: ";
    static estd::layer1::string<64> target_glossary = "Which SPI target (select only): ";

    concat(commands::spi_config::list, ", ", command_glossary);
    concat(commands::spi_config::select_targets, ", ", target_glossary);

    spi_config_args.command = arg_str1(NULL, NULL, "<command>", command_glossary.data());
    spi_config_args.target = arg_str1(NULL, NULL, "<target>", target_glossary.data());
    spi_config_args.value = arg_strn(NULL, NULL, "<value>", 0, 1, "Value to write");
    spi_config_args.end = arg_end(3);

    const esp_console_cmd_t cmd = {
        .command = "spi-config",
        .help = "SPI host or device configuration",
        .hint = NULL,
        .func = &spi_config,
        .argtable = &spi_config_args,
    };
    ESP_ERROR_CHECK( esp_console_cmd_register(&cmd) );
}


}}

extern "C" void initialize_spi()
{
    const char* name = get_name(spi_host_device_names, SPI2_HOST);
    ESP_LOGD(TAG, "Test: %s", name);

    registrar::spi::device_init();
    registrar::spi::bus_init();
    registrar::spi::config_init();
}