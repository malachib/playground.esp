tcpdump reports that 1472 is the maximum MTU size, though I think that's reported 
by linux itself

however, the results are in.  we're able to shuttle 1467 bytes (xlarge file) back 
and forth with impunity.  

ESP8266 MTU seems to be at 1500 exactly.
At one time I brute forced the MTU down to 1000 on
PGESP-4.poc1a and it still seemed to balk.