#! /usr/bin/env python

# Client and server for udp (datagram) echo.
#
# Usage: udpecho -s [port]            (to start a server)
# or:    udpecho -c host [port] <file (client)

import sys
from socket import *

BUFSIZE = 2048

def main():
    if len(sys.argv) < 1:
        usage()
    else:
        client()

def usage():
    sys.stdout = sys.stderr
    print 'or:    udpecho host [repeat] <file (client)'
    sys.exit(2)

def client():
    if len(sys.argv) < 2:
        usage()
    host = sys.argv[1]
    if len(sys.argv) > 2:
        repeat = eval(sys.argv[2])
    else:
        repeat = 1
    # fixed port, this is what echo code uses
    port = 7
    addr = host, port
    s = socket(AF_INET, SOCK_DGRAM)
    s.bind(('', 0))
    print 'udp echo client ready, reading stdin'
    # sends entire chunk, not line by line
    all_line = ''
    for x in range(repeat):
        while 1:
            line = sys.stdin.readline()
            all_line += line
            if not line:
                s.sendto(all_line, addr)
                data, fromaddr = s.recvfrom(BUFSIZE)
                match = data == all_line
                #print 'client received %r from %r' % (data, fromaddr)
                print '%02d client received %d from %r.  Match=%d' % (
                    x, len(data), fromaddr, match)
                if not match:
                    f = open('workfile', 'w')
                    f.write(data)
                    exit
                break

main()