#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

extern "C" void udp_echo_init();

extern "C" void testTask(void* p)
{
    // echo may be enough for our test, but plan on an automatic sender as well
    udp_echo_init();

    for(;;)
    {
        taskYIELD();
    }
}