# Overview

# Infrastructure

`testable` component proves little that `component1` and `component2` didn't, just using to reflect what is done in `estd`

# References

1. RESERVED
    1. https://docs.espressif.com/projects/esp-idf/en/v5.2.1/esp32/api-guides/unit-tests.html
    2. https://github.com/espressif/esp-idf/blob/v5.2.1/examples/system/unit_test/test/main/example_unit_test_test.c
