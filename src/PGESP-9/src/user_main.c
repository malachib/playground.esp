#include "esp_common.h"
#include "config.h"
//#include "task.h"

void conn_ap_init(void)
{
    wifi_set_opmode(STATIONAP_MODE);
    struct station_config config;
    memset(&config, 0, sizeof(config));  //set value of config from address of &config to width of size to be value '0'
    sprintf(config.ssid, DEMO_AP_SSID);
    sprintf(config.password, DEMO_AP_PASSWORD);
    wifi_station_set_config(&config);
    wifi_station_connect();
}

void my_loop(void* param)
{
    conn_ap_init();
    int counter = 0;
    const int xDelay = 5000 / portTICK_RATE_MS;

    for(;;)
    {
        printf("Hello World: %d\r\n", counter++);
        vTaskDelay(xDelay);
    }
}

/******************************************************************************
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABCCC
 *                A : rf cal
 *                B : rf init data
 *                C : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
 *******************************************************************************/
uint32 user_rf_cal_sector_set(void)
{
    flash_size_map size_map = system_get_flash_size_map();
    uint32 rf_cal_sec = 0;

    switch (size_map) {
        case FLASH_SIZE_4M_MAP_256_256:
            rf_cal_sec = 128 - 5;
            break;

        case FLASH_SIZE_8M_MAP_512_512:
            rf_cal_sec = 256 - 5;
            break;

        case FLASH_SIZE_16M_MAP_512_512:
        case FLASH_SIZE_16M_MAP_1024_1024:
            rf_cal_sec = 512 - 5;
            break;

        case FLASH_SIZE_32M_MAP_512_512:
        case FLASH_SIZE_32M_MAP_1024_1024:
            rf_cal_sec = 1024 - 5;
            break;

        default:
            rf_cal_sec = 0;
            break;
    }

    return rf_cal_sec;
}


void user_init(void)
{
    printf("SDK version:%s\n", system_get_sdk_version());

    //wifi_set_event_handler_cb(wifi_event_handler_cb);
    xTaskCreate(my_loop, "my_loop", 512, NULL, 4, NULL);
}
