/**
 *  10AUG24 MB Adapted from esp32_c3_lyra
 */

#include "esp_log.h"
#include "driver/gpio.h"
#include <string.h>
#include "board.h"
#include "audio_error.h"
#include "audio_mem.h"
#include "soc/soc_caps.h"

#include <arduino-mapping.h>

static const char *TAG = "AdafruitBFF";

esp_err_t get_i2c_pins(i2c_port_t port, i2c_config_t *i2c_config)
{
    /*
    AUDIO_NULL_CHECK(TAG, i2c_config, return ESP_FAIL);
    if (port == 0) {
        i2c_config->sda_io_num = GPIO_NUM_9;
        i2c_config->scl_io_num = GPIO_NUM_8;
    } else {
        i2c_config->sda_io_num = -1;
        i2c_config->scl_io_num = -1;
        ESP_LOGE(TAG, "i2c port %d is not supported", port);
        return ESP_FAIL;
    }   */
    return ESP_OK;
}

// General Qt/Xiao assignments

#define MOSI D10
#define MISO D9
#define SCK D8
#define CS A0


// https://learn.adafruit.com/i2s-amplifier-bff/overview
// A0 = DIN
// A1 = LRC (wordclock select)
// A2 = BIT (bitclock)

/*
#define BOARD_ADAFRUIT_BFF_AUDIO    1
#define BOARD_ADAFRUIT_BFF_I2SAMP   2
#define BOARD_ADAFRUIT_BFF          CONFIG_BSP_ADF  */

esp_err_t get_i2s_pins(int port, board_i2s_pin_t *i2s_config)
{
    AUDIO_NULL_CHECK(TAG, i2s_config, return ESP_FAIL);

//#if BOARD_ADAFRUIT_BFF == BOARD_ADAFRUIT_BFF_I2SAMP
#if CONFIG_BSP_ADF_ADAFRUIT_BFF_I2SAMP
    i2s_config->bck_io_num = A2;
    i2s_config->ws_io_num = A1;
    i2s_config->data_out_num = A0;
#elif CONFIG_BSP_ADF_ADAFRUIT_BFF_AUDIO
//#elif BOARD_ADAFRUIT_BFF == BOARD_ADAFRUIT_BFF_AUDIO
    i2s_config->bck_io_num = A3;
    i2s_config->ws_io_num = A2;
    i2s_config->data_out_num = A1;
#else
#error
#endif
    i2s_config->data_in_num = -1;

    return ESP_OK;
}

esp_err_t get_spi_pins(spi_bus_config_t *spi_config, spi_device_interface_config_t *spi_device_interface_config)
{
    AUDIO_NULL_CHECK(TAG, spi_config, return ESP_FAIL);
    AUDIO_NULL_CHECK(TAG, spi_device_interface_config, return ESP_FAIL);

    // UNTESTED
#if CONFIG_BSP_ADF_ADAFRUIT_BFF_AUDIO
    spi_config->mosi_io_num = MOSI;
    spi_config->miso_io_num = MISO;
    spi_config->sclk_io_num = SCK;

    spi_device_interface_config->spics_io_num = CS;
#else
    spi_config->mosi_io_num = -1;
    spi_config->miso_io_num = -1;
    spi_config->sclk_io_num = -1;
    spi_config->quadwp_io_num = -1;
    spi_config->quadhd_io_num = -1;

    spi_device_interface_config->spics_io_num = -1;
#endif

    ESP_LOGW(TAG, "SPI interface is not supported");
    return ESP_OK;
}

// sdcard
int8_t get_sdcard_intr_gpio(void)
{
    return SDCARD_INTR_GPIO;
}

int8_t get_sdcard_open_file_num_max(void)
{
    return SDCARD_OPEN_FILE_NUM_MAX;
}

#define SDCARD_PWR_CTRL -1

int8_t get_sdcard_power_ctrl_gpio(void)
{
    return SDCARD_PWR_CTRL;
}

// input-output pins
int8_t get_pa_enable_gpio(void)
{
    return PA_ENABLE_GPIO;
}

// adc button id
int8_t get_input_rec_id(void)
{
    return -1;
}

int8_t get_input_mode_id(void)
{
    return BUTTON_MODE_ID;
}

int8_t get_input_set_id(void)
{
    return BUTTON_SET_ID;
}

int8_t get_input_play_id(void)
{
    return BUTTON_PLAY_ID;
}

int8_t get_input_volup_id(void)
{
    return BUTTON_VOLUP_ID;
}

int8_t get_input_voldown_id(void)
{
    return BUTTON_VOLDOWN_ID;
}

// led pins
/*
int8_t get_ws2812_led_gpio(void)
{
    return WS2812_LED_GPIO_PIN;
}
*/

int8_t get_es8311_mclk_src(void)
{
    return -1;
}

