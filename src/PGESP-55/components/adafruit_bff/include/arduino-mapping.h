#pragma once

#include "sdkconfig.h"

// DEBT: use embr board traits.  For now, slum it with inline defs
#ifdef CONFIG_BOARD_ESP32S3_LILYGO_T_QT_PRO
#define A0 34
#define A1 33
#define A2 39
#define A3 38
#define D7 48
#define D8 18
#define D9 17
#define D10 16
#elif defined(CONFIG_BOARD_ESP32S3_SEEED_XIAO)
#define A0 1
#define A1 2
#define A2 3
#elif defined(CONFIG_BOARD_ESP32C3_SEEED_XIAO)
// NOTE: ESP-ADF does not seem to support c3
#define A0 2
#define A1 3        // FIX: doublecheck this guy
#define A2 4
#else
#error Board not supported
#endif

