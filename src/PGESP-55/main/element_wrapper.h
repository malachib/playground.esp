/**
 *  c++ audio element wrapper helper
 *  v0.1
 */
#pragma once

#include <esp_err.h>
#include <audio_element.h>

template <class Impl>
class AudioElementWrapper
{
    using impl_type = Impl;
    using pointer = impl_type*;

public:
    static esp_err_t close(audio_element_handle_t self)
    {
        auto this_ = (pointer)audio_element_getdata(self);
        return this_->close(self);
    }

    static esp_err_t open(audio_element_handle_t self)
    {
        auto this_ = (pointer)audio_element_getdata(self);
        return this_->open(self);
    }

    static esp_err_t destroy(audio_element_handle_t self)
    {
        auto this_ = (pointer)audio_element_getdata(self);
        return this_->destroy();
    }

    static audio_element_err_t process(audio_element_handle_t self, char *in_buffer, int in_len)
    {
        auto this_ = (pointer)audio_element_getdata(self);
        return this_->process(self, in_buffer, in_len);
    }
};


template <class Impl>
void set_actions(audio_element_cfg_t* cfg)
{
    using type = AudioElementWrapper<Impl>;

    cfg->destroy = type::destroy;
    cfg->process = type::process;
    cfg->open = type::open;
    cfg->close = type::close;
}


class AudioElementImplBase
{
public:
    using handle_type = audio_element_handle_t;

    esp_err_t open(audio_element_handle_t) { return ESP_OK; }
    esp_err_t close(handle_type) { return ESP_OK; }
    esp_err_t destroy() { return ESP_OK; }
    audio_element_err_t process(handle_type, char *in_buffer, int in_len) { return audio_element_err_t{}; }
};

