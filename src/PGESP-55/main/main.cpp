#include <esp_log.h>

#include <cstring>

#include <estd/port/version.h>          // For ESTD_BUILD_SEMVER

#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_mem.h"
#include "audio_common.h"

#include "audio_hal.h"

#include "i2s_stream.h"
#include "mp3_decoder.h"

#include "board.h"
#include "element.h"

static const char* TAG = "PGESP-55::main";

extern "C" void audio_init(audio_element_handle_t* i2s_stream_writer);

// medium rate mp3 audio
extern const uint8_t mr_mp3_start[] asm("_binary_music_16b_2c_22050hz_mp3_start");
extern const uint8_t mr_mp3_end[]   asm("_binary_music_16b_2c_22050hz_mp3_end");

static struct marker
{
    int pos;
    const uint8_t *start = mr_mp3_start;
    const uint8_t *end = mr_mp3_end;
} file_marker;

// 05MAR25 DEBT: ADF has concerningly weak versioning, so cobble this together.
// 0% gauruntee we're looking at 2.7, and 2.7.1 I just came up with myself to indicate
// their master branch is ahead of 2.7
#ifndef ESP_ADF_VERSION
#define ESP_ADF_VERSION ESTD_BUILD_SEMVER(2, 7, 1)
#endif

#if ESP_ADF_VERSION > ESTD_BUILD_SEMVER(2, 7, 0)
int
#else
audio_element_err_t
#endif
mp3_music_read_cb(audio_element_handle_t el, char *buf, int len, TickType_t wait_time, void *ctx)
{
    int read_size = file_marker.end - file_marker.start - file_marker.pos;
    if (read_size == 0) {
        return AEL_IO_DONE;
    } else if (len < read_size) {
        read_size = len;
    }
    memcpy(buf, file_marker.start + file_marker.pos, read_size);
    file_marker.pos += read_size;
    return (audio_element_err_t)read_size;
}



// FIX: Experimncing Read IO failure:
/*

E (436) AUDIO_ELEMENT: [mp3] Read IO type ringbuf but ringbuf not set
E (446) MP3_DECODER: Failed to read audio data (line 129)
E (446) AUDIO_ELEMENT: [mp3] AEL_STATUS_ERROR_OPEN,-1

*/

extern "C" void app_main(void)
{
    ESP_LOGI(TAG, "entry");

    audio_pipeline_handle_t pipeline;
    audio_element_handle_t i2s_stream_writer, mp3_decoder;

    // Pseudo custom BSP based code (see my_board [2.5])
    audio_board_handle_t board_handle = audio_board_init();

    //audio_hal_handle_t h{};
    audio_hal_handle_t h = board_handle->audio_hal;
    audio_hal_ctrl_codec(h, AUDIO_HAL_CODEC_MODE_DECODE, AUDIO_HAL_CTRL_START);
    int player_volume;
    audio_hal_get_volume(h, &player_volume);

    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline = audio_pipeline_init(&pipeline_cfg);

    mp3_decoder_cfg_t mp3_cfg = DEFAULT_MP3_DECODER_CONFIG();
    mp3_decoder = mp3_decoder_init(&mp3_cfg);
    audio_element_set_read_cb(mp3_decoder, mp3_music_read_cb, NULL);

    audio_element_handle_t custom_element = custom_element_init();

    // DEBT: breaking up init like this is undesirable
    audio_init(&i2s_stream_writer);

    audio_pipeline_register(pipeline, mp3_decoder, "mp3");
    audio_pipeline_register(pipeline, custom_element, "custom");
    audio_pipeline_register(pipeline, i2s_stream_writer, "i2s");

#if ENABLE_CUSTOM_ELEMENT
    const char *link_tag[] = {"mp3", "custom", "i2s"};
#else
    const char *link_tag[] = {"mp3", "i2s"};
#endif
    audio_pipeline_link(pipeline, link_tag, sizeof link_tag / sizeof link_tag[0]);

    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    audio_event_iface_handle_t evt = audio_event_iface_init(&evt_cfg);

    ESP_ERROR_CHECK(audio_pipeline_set_listener(pipeline, evt));

    ESP_ERROR_CHECK(audio_pipeline_run(pipeline));

    for(;;)
    {
        audio_event_iface_msg_t msg;
        esp_err_t ret = audio_event_iface_listen(evt, &msg, portMAX_DELAY);

        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT
            && msg.source == mp3_decoder
            && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO)
        {
            // TODO: MP3 decoder reports this
            // at beginning, that's where example sets i2c sample & bitrate
            audio_element_info_t music_info {};
            audio_element_getinfo(mp3_decoder, &music_info);
            ESP_LOGI(TAG, "[ * ] Receive music info from mp3 decoder, sample_rates=%d, bits=%d, ch=%d",
                     music_info.sample_rates, music_info.bits, music_info.channels);
            i2s_stream_set_clk(i2s_stream_writer, music_info.sample_rates, music_info.bits, music_info.channels);
            continue;
        }

#ifdef CONFIG_ENABLE_TASKLESS_ELEMENT
#endif
    }
}
