// Guidance from [2.7] & [4.1]

#include <new>

#include <esp_log.h>

#include <audio_mem.h>

#include "element.h"


static constexpr int BUF_SIZE = 512;


class Custom : public AudioElementImplBase
{
    static constexpr const char* TAG = "PGESP-55::Custom";

    //bool eof_ = false;
#ifdef CONFIG_ENABLE_CUSTOM_BUFFER
    char* buf_ = nullptr;
    int buf_size_ = BUF_SIZE;
#endif

public:
    esp_err_t open(audio_element_handle_t);
    audio_element_err_t process(handle_type, char* in_buffer, int in_len);

    Custom();
};

//         r_size = audio_element_input(self, (char *)equalizer->buf, BUF_SIZE);

Custom::Custom()
{

}

esp_err_t Custom::open(audio_element_handle_t self)
{
    audio_element_info_t info {};
    audio_element_getinfo(self, &info);

    ESP_LOGI(TAG, "open: sample_rates=%d, channels=%d",
        info.sample_rates,
        info.channels);

#ifdef CONFIG_ENABLE_CUSTOM_BUFFER
    buf_ = (char *)audio_calloc(1, buf_size_);
    
    if (buf_ == NULL)
    {
        ESP_LOGE(TAG, "calloc buffer failed. (line %d)", __LINE__);
        return ESP_ERR_NO_MEM;
    }
#endif

    return ESP_OK;
}

// TODO: Still need to get to the bottom of the difference between in_buffer
// and audio_element_input -- it sure would be nice to DIRECTLY touch the ringbuf!
audio_element_err_t Custom::process(handle_type self, char* in_buffer, int in_len)
{
    // Some inspiration from [4.5], though not sure it's 100% correct

    int ret = 0;
    int r_size = 0;

#ifdef CONFIG_ENABLE_CUSTOM_BUFFER
    int len = buf_size_;

    // NOTE: Presumes 16-bit feed
    auto buf = (int16_t*)buf_;
    int buf_size = buf_size_ >> 1;

    // Many examples do this eof flag.  Perhaps it's an optimization,
    // or perhaps that code was written before AEL_IO_DONE was a thing
    //if(!eof_)
    {
        // TODO: Revise this based on README section 3.9, though we'll
        // need to be careful we are int16_t aligned
        r_size = audio_element_input(self, buf_, buf_size_);
    }
#else
    // NOTE: Presumes 16-bit feed
    // NOTE: Experimenting with writing back to in_buffer
    auto buf = (int16_t*)in_buffer;
    int buf_size = in_len >> 1;
    int& len = in_len;
    r_size = in_len;

    // It seems maybe this ISN'T zerocopy.  [4.5] still does a read, and
    // pipeline hangs if we don't do this input
    r_size = audio_element_input(self, in_buffer, in_len);
#endif

    if(r_size < 0)
    {
        switch(r_size)
        {
            case AEL_IO_DONE:
                ESP_LOGI(TAG, "process: done");
                break;

            default:
                ESP_LOGI(TAG, "process: error: %d", r_size);
                break;
        }

        return audio_element_err_t(r_size);
    }

    if ( //len != r_size ||
        (r_size % 2) != 0)
    {
        ESP_LOGW(TAG, "unexpected rsize: %d, len: %d", r_size, len);
    }

    if(r_size > 0)
    {
        int16_t gain = 80;

        // Considering an easy gain stage here
        while(buf_size--)
        {
            // NOTE: R&D indicates that a float multiply isn't too costly, but
            // some form of fixed point could be better
            int32_t v = *buf;

            // 0-64 = 0-100%
            // 65-128 = ~101%-200%
            v *= gain;
            // ESP32 divides are VERY expensive ... bit shift it is!
            v >>= 6;

            *buf = int16_t(v);
            buf++;
        }

#ifdef CONFIG_ENABLE_CUSTOM_BUFFER
        ret = audio_element_output(self, buf_, r_size);
#else
        ret = audio_element_output(self, in_buffer, r_size);
#endif
    }
    return audio_element_err_t(ret);
}

template <class Impl>
void set_actions(audio_element_cfg_t* cfg, Impl* impl)
{
    set_actions<Impl>(cfg);
    cfg->data = impl;
}

audio_element_handle_t custom_element_init()
{
    audio_element_cfg_t cfg {};

    //auto custom = (Custom*)audio_calloc(1, sizeof(Custom));
    // NOTE: Just a formality, calling currently-empty constructor
    //new (custom) Custom();

    auto custom = new Custom();

    // DEBT: Copy/pasted direct from ASP-ADF audio-element.h since C++ initializes
    // structs a bit different than C
    cfg.buffer_len         = DEFAULT_ELEMENT_BUFFER_LENGTH;
    // Bummer, default isn't big enough.  Kind of a surprise
    //cfg.task_stack         = DEFAULT_ELEMENT_STACK_SIZE;
#ifdef CONFIG_ENABLE_TASKLESS_ELEMENT
    // NOTE: Not ready yet
    cfg.task_stack         = 0;
#endif
    cfg.task_stack         = 4096;
    cfg.task_prio          = DEFAULT_ELEMENT_TASK_PRIO;
    cfg.task_core          = DEFAULT_ELEMENT_TASK_CORE;

    set_actions(&cfg, custom);

    // Most examples allocate their own buffer.  Seems overkill, but we'll follow suit
#ifdef CONFIG_ENABLE_CUSTOM_BUFFER
    cfg.buffer_len = 0;
#else
    // NOTE: Not ready yet, hangs pipeline
    cfg.buffer_len = BUF_SIZE;
#endif
    cfg.tag = "PGESP-55";

    audio_element_handle_t el = audio_element_init(&cfg);

    // Loaded via cfg->data
    //audio_element_setdata(el, custom);

    // audio_element_init_t in fact sets this to some defaults, but it's 44.1Khz.
    // still unknown what difference that really makes though.  Documentation has
    // 100% useless "Set audio element information." description
    audio_element_info_t info {};
    audio_element_setinfo(el, &info);

    return el;
}