#pragma once

#include <esp_err.h>
#include <audio_element.h>

#include "element_wrapper.h"

#if CONFIG_ENABLE_CUSTOM_ELEMENT
#define ENABLE_CUSTOM_ELEMENT 1
#endif

audio_element_handle_t custom_element_init();

