#include <esp_log.h>

#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_mem.h"
#include "audio_common.h"

#include "i2s_stream.h"


// DEBT: Only part of audio init, since the macros aren't C++ friendly
void audio_init(audio_element_handle_t* i2s_stream_writer)
{
    //i2s_stream_cfg_t i2s_cfg = I2S_STREAM_PDM_TX_CFG_DEFAULT();
    // C++ is upset about order of init here
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = AUDIO_STREAM_WRITER;
    *i2s_stream_writer = i2s_stream_init(&i2s_cfg);
}