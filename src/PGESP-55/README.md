# Overview

# 1. Detail & Scope

Either:

* Playback a static audio file
* Record to a static audio file

# 2. Infrastructure, Concepts and Tasks

## 2.1. Devices

### 2.1.1. Espressif

ESP32S3 is preferred to match client requirements
ESP32C3 appears to not link (See section 3.6)

### 2.1.2. Adafruit

Two devices are considered:

* "Audio BFF" [5.1]
* "Amplifier BFF" [5]

"Audio" seems to be an upgrade of "Amplifier", adding an SD card.
We focus on "Amplifier" but eventually would like to make the component configurable

"Audio" uses a MAX98357
"Amplifier" uses a MAX93785 (I think it's a typo, probably is MAX98357)

No I2C gain control etc. is apparent

### 2.1.3. Lilygo QT Pro (ESP32S3)

### 2.1.4. Seeed Xiao ESP32S3

## 2.2. ESP-ADF & ESP-IDF

ESP-ADF are pretty neglectful about their version tagging.  So, we are hanging
off `99112c8c20af3f348127fdd8ddbb27f67531d757` commit off their repo.

Also, we are using patched ESP-IDF submmodule (v4.4.4) inside aforementioned repo.

## 2.3. TODO

### 2.3.1. Compile against ESP-IDF 5.1 - DONE

### 2.3.2. Use 'Console Peripheral'

Examples have a complete robust console peripheral demonstration.

### 2.3.3. Try 'WiFi' peripheral

Optional, outside scope of this project.
"SmartConfig, BluFi, and AirKiss are supported for network configuration."

### 2.3.4. Find documentation for BSP-ish

Does not seem to exist.  We have to reverse engineer [2.5] "my_board"

# 3. Opinions & Observations

## 3.1. ESP-IDF version

ESP-ADF can use either its submodule ESP-IDF or an already-installed ESP-IDF.
Not every version of ESP-IDF and Python is supported [2] [2.2]
Canonical quick start tells us to use submodule [2.2] which at present is an older ESP-IDF 4.4.4

## 3.2. API surface

API surface seems sensible on first glance, but massive.  

### 3.2.1. Examples

Leaning more heavily on example code, specifically mp3 player [2.4] because API is massive.
There's also a play-from-SD example which may be more fitting [2.6]

## 3.3. ESP-ADF stability

Versioning is not as locked down as I would like.  Latest release is Q3 2023, reducing the surprise of 
ESP-IDF 4.4.4 pseudo dependency [Section 3.1]

Code base itself is active.

Overall, smells of "second stringer" status for Espressif.

## 3.4. This ADF project itself

Looks like if one specifies 'custom board' then one needs to also include a board component as seen
in `components/my_board` [2.4]

## 3.5. Custom Board

So far no canonical instructions seem to be present for a custom board [4.1] [4.2]
[4.3]
Hand assembling/reverse engineering Adafruit I2S BFF "BSP" myself [5]
Comes along well, passes tests.  Have not finished or tried "Audio BFF" yet

## 3.6. ESP32C3

While compiling against ESP-IDF 4.4.4, this target has serious linker issues, presumably due to a combination of:

- Lack of C3 ADF precompiled blobs
- Lack of aforementioned specifically at the ESP-SR level
- Precompiled blobs being too new for GCC 8.4.0

These assertions are unverified.  However one thing's for sure, ESP32C3 won't link

Noteworthy is we hit this issue even when selecting LYRA-C3

```
/home/malachi/.espressif/tools/riscv32-esp-elf/esp-2021r2-patch5-8.4.0/riscv32-esp-elf/bin/../lib/gcc/riscv32-esp-elf/8.4.0/../../../../riscv32-esp-elf/bin/ld: -march=: ISA string must begin with rv32 or rv64
/home/malachi/.espressif/tools/riscv32-esp-elf/esp-2021r2-patch5-8.4.0/riscv32-esp-elf/bin/../lib/gcc/riscv32-esp-elf/8.4.0/../../../../riscv32-esp-elf/bin/ld: failed to merge target specific data of file /home/malachi/.espressif/tools/riscv32-esp-elf/esp-2021r2-patch5-8.4.0/riscv32-esp-elf/bin/../lib/gcc/riscv32-esp-elf/8.4.0/rv32imc/ilp32/no-rtti/libgcc.a(trunctfdf2.o)
```

## 3.7. I2S low level format details

### 3.7.1. Standard Philips Format

Gleaned from wiki [6]:

* signed integer
* big endian

### 3.7.2. ESP32S3's treatment

In context of `i2s_hal_slot_config_t`:

`big_endian` flag defauls to `false` for I2S config.  According to our buddy chatgpt,
this means ESP32S3 internally reorders bytes.  `big_endian=true` means bytes are left
untouched.

## 3.8. Task-less audio element

If one does the work of `audio_element_task`, namely consuming `audio_event_iface_waiting_cmd_msg` and `audio_element_process_running` then
theoretically a task-less element is possible.  However, `audio_element_process_running` is hidden as a static

It seems we can manage task-less by manually calling audio_element_input [7]:

```
// Main loop to process audio manually
while ((bytes_read = fread(buffer, 1, sizeof(buffer), file)) > 0) {
    audio_element_input(mp3_decoder, buffer, bytes_read);
    
    // Manually process the MP3 decoder
    audio_element_process(mp3_decoder);

    // Manually process the I2S stream
    audio_element_process(i2s_stream_writer);
}
```

## 3.9. Audio Element process' in_buffer

This may be exactly what it appears, and the prediliction for doing an
`audio_element_input` may be an artifact of showing the more versatile,
but not necessarily better, use case.

in_buffer seems very much better for zero-copy style treatment.  However,
that is misleading. in fact it all still copies out of ringbuffer.

# 4. Conclusions

## 4.1. ESP-IDF v5.2.2

### 29JUL24

c131-debian
ESP-ADF commit: `99112c8c20af3f348127fdd8ddbb27f67531d757`

By defaut Doesn't compile:

```
HINT: You are maybe using pre FreeRTOS V8.0.0 data types. The backward compatibility of such data types is no longer enabled by default. Please turn on CONFIG_FREERTOS_ENABLE_BACKWARD_COMPATIBILITY explicitly to use such data types.
```

Aforementioned recommendation does result in successful compile.

### 10AUG24

rover (MBA debian)
ESP-ADF commit: `fced87a34079b939f2963ffc48742e01ff6fbc41`

First compile works.  Subsequent compiles do not:


```
CMake Error at /home/malachi/Projects/ext/esp-idf/tools/cmake/build.cmake:544 (message):
  ERROR: Some components (espressif/esp-dsp) in the "managed_components"
  directory were modified on the disk since the last run of the CMake.
  Content of this directory is managed automatically.

  If you want to keep the changes, you can move the directory with the
  component to the "components"directory of your project.

*................. lots of other stuff ................*

Call Stack (most recent call first):
  /home/malachi/Projects/ext/esp-idf/tools/cmake/project.cmake:605 (idf_build_process)
  CMakeLists.txt:16 (project)
```

c131-debian with commit `99112c8c20af3f348127fdd8ddbb27f67531d757` continues
to compile just fine.

### 12AUG24

rover now uses
ESP-ADF commit: `99112c8c20af3f348127fdd8ddbb27f67531d757`

However still same crash as 10AUG24.  Semi-workaround is to delete
`managed_components/espressif__esp-dsp`.

## 4.2. I2S format

App runs happily with I2S_STREAM_CFG_DEFAULT, who in turn uses I2S_COMM_FORMAT_STAND_I2S.
That is documented as "I2S communication I2S Philips standard, data launch at second BCK"

# 5. Journal

## 05MAR25

Revisiting things.  ESP-ADF last official update was v2.7 (14SEP24)
I really wish they had made this an esp component.  Continuing the disturbing trend,
versioned docs also seem to be out [2.1.1]

Compiling using ESP-IDF v5.4
debian-hp
ESP-ADF commit: `8301b97d70d523051897546f639273b7706ddb4d`

Mildly breaking change for stream_func signature.  Not hard to fix on its own, but the chaos
of versioning makes it a little more involved.

# Test Results

Tests done on Lilygo QT-Pro ESP32S3 with adafruit Amplifier BFF [5]

| Date    | Board | ESP-IDF | ESP-ADF |  Custom Element | Result
| ----    | ----  | ----    | ----    | ----           | ---
| 10AUG24 | Amp   | v5.2.2  | 99112c8c20af3f348127fdd8ddbb27f67531d757 | No             | Pass
| 05MAR25 | Amp   | v5.4.0  | 8301b97d70d523051897546f639273b7706ddb4d | No             | Pass

# Attribution

Music samples in this folder are taken from: "Galway"
Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0
http://creativecommons.org/licenses/by/3.0/

# References

1. RESERVED
2. https://github.com/espressif/esp-adf
    1. https://docs.espressif.com/projects/esp-adf/en/v2.0/get-started/index.html
        1. https://docs.espressif.com/projects/esp-adf/en/latest/get-started/index.html
    2. https://espressif-docs.readthedocs-hosted.com/projects/esp-adf/en/latest/get-started/index.html
    3. https://espressif-docs.readthedocs-hosted.com/projects/esp-adf/en/latest/api-reference/index.html
    4. https://espressif-docs.readthedocs-hosted.com/projects/esp-adf/en/latest/api-reference/codecs/mp3_decoder.html
    5. https://github.com/espressif/esp-adf/tree/v2.6/examples/get-started/play_mp3_control
    6. https://github.com/espressif/esp-adf/tree/v2.6/examples/player/pipeline_play_sdcard_music
    7. https://github.com/espressif/esp-adf-libs/blob/831afe75657e0e86564a57e405eb71d8baea4d44/esp_codec/equalizer.c
3. https://github.com/espressif/esp-sr
4. RESERVED
    1. https://www.esp32.com/viewtopic.php?t=20395
    2. https://esp32.com/viewtopic.php?t=13698
    3. https://github.com/espressif/esp-adf/issues/997
    4. https://esp32.com/viewtopic.php?t=14905
    5. https://esp32.com/viewtopic.php?t=20171
5. https://learn.adafruit.com/i2s-amplifier-bff/overview
    1. https://learn.adafruit.com/adafruit-audio-bff
6. https://en.wikipedia.org/wiki/I%C2%B2S
7. ChatGPT