#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include <estd/port/identify_platform.h>

#if ESTD_IDF_VER >= ESTD_IDF_VER_2_0_0_644
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#else
#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"
#endif

#include "mbedtls/ssl.h"
#include "mbedtls/esp_debug.h"

int dtls_main();

#if defined(MBEDTLS_DEBUG_C)
void setup_debug_log(mbedtls_ssl_config* conf)
{
    mbedtls_esp_enable_debug_log(conf, CONFIG_MBEDTLS_DEBUG_LEVEL);
}
#endif

static void announce()
{
    printf("DTLS task running");
    fflush( stdout );
}

void test_task(void* data)
{
    announce();

    for(;;)
    {
        // FIX: Just the mere presence of this call crashes
        // things
        dtls_main();

        vTaskDelay(1000);
    }
}