extern "C" {

#ifdef OLD_STYLE
#include "esp_common.h"
#else
#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#endif

#include "lwip/sockets.h"
#include <time.h>
#include <string.h>
#include <stdio.h>

}

int dtls_main();

extern "C" void testTask(void* data)
{
    for(;;)
    {
        printf("testTask\n");
        fflush( stdout );

        dtls_main();
    }
}