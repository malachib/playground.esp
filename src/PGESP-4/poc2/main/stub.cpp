extern "C" {
#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#include "mbedtls/ssl.h"
#include "mbedtls/debug.h"
#include "mbedtls/esp_debug.h"

void dtls_main();
void setup_debug_log(mbedtls_ssl_config* conf);

}

// conspiculously missing from ESP32 setup, though present in ESP8266
#define CONFIG_MBEDTLS_DEBUG_LEVEL 2

#if defined(MBEDTLS_DEBUG_C)
void setup_debug_log(mbedtls_ssl_config* conf)
{
    mbedtls_esp_enable_debug_log(conf, CONFIG_MBEDTLS_DEBUG_LEVEL);
}
#endif

extern "C" int main()
{
    dtls_main();
    return 0;
}