#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"

#include "mbedtls/ssl.h"
#include "mbedtls/esp_debug.h"

int dtls_main();

#if defined(MBEDTLS_DEBUG_C)
void setup_debug_log(mbedtls_ssl_config* conf)
{
    mbedtls_esp_enable_debug_log(conf, CONFIG_MBEDTLS_DEBUG_LEVEL);
}
#endif

static void announce()
{
    printf("DTLS task running");
    fflush( stdout );
}

void testTask(void* data)
{
    announce();

    for(;;)
    {
        // FIX: Just the mere presence of this call crashes
        // things
        dtls_main();

        vTaskDelay(1000);
    }
}