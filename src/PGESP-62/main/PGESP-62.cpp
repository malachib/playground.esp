#include <esp_log.h>

#include <estd/string.h>

static const char* TAG = "PGESP-62";

extern "C" void app_main(void)
{
    estd::layer1::string<32> s{"hi2u"};

    ESP_LOGI(TAG, "app_main: entry=%s", s.c_str());
}
