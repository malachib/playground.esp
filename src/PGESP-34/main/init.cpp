#include <embr/platform/esp-idf/pm.h>
#include <embr/platform/esp-idf/gptimer.h>

#include <esp-helper.h>

#include "esp_wifi.h"

using namespace embr::esp_idf;

v5::Timer timer;

void initialize()
{
    init_flash();

    // See PGESP-33 for more comments on timer_config
    gptimer_config_t timer_config =
    {
        .clk_src = GPTIMER_CLK_SRC_APB,
        .direction = GPTIMER_COUNT_UP,
        .resolution_hz = 1 * 1000 * 1000, // 1MHz, 1 tick = 1us
        .flags = 0
    };

    ESP_ERROR_CHECK(timer.init(&timer_config));

    wifi_init_sta();

    pm_traits<>::config_type pm_config =
    {
        .max_freq_mhz = 80,
        .min_freq_mhz = 10,
        .light_sleep_enable = true
    };

    // Activates dynamic frequency scaling
    ESP_ERROR_CHECK(esp_pm_configure(&pm_config));

    esp_wifi_set_ps(WIFI_PS_MAX_MODEM);
}