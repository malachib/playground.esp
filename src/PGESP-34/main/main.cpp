#include <estd/thread.h>
#include <estd/iostream.h>

#include <esp_system.h>
#include <esp_log.h>

void initialize();

using namespace estd::chrono_literals;

extern "C" void app_main(void)
{
    static const char* TAG = "PGESP-34: app_main";

    ESP_LOGI(TAG, "Bringup");

    initialize();

    for(;;)
    {
        static int counter = 0;

        ESP_LOGI(TAG, "counter=%d", ++counter);

        estd::this_thread::sleep_for(10s);
    }
}
