# PGESP-34: Power modes w/ radio

WiFi on always.  Bluetooth only activates on command, and is really
possibly not relevant to the test at all (do I really care about brief
power usage when provisioning button is pressed?)

Measured modes are: "Modem sleep" mode with DFS 10mhz-80mhz
Not measuring full power, because we can get that result from something like
embr's udp-echo

gptimer runs for 3s in response to a GPIO activity.  Not debounced, to
keep test simpler

## Requirements

esp-idf v5.0

# References

1. https://github.com/espressif/esp-idf/tree/v5.0/examples/wifi/power_save