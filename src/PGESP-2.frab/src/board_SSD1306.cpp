extern "C" {

#include <gfx.h>

#include "esp_system.h"
//#include "soc/gpio_struct.h"
//#include "driver/gpio.h"
#include <string.h>


}

#include "board_SSD1306.h"
#include <frab/i2c.h>
#include <frab/frab.h>

#include "esp_err.h"
#include "esp_log.h"

// lifted from https://github.com/yanbe/ssd1306-esp-idf-i2c
// Control byte
#define OLED_CONTROL_BYTE_CMD_SINGLE    0x80
#define OLED_CONTROL_BYTE_CMD_STREAM    0x00
#define OLED_CONTROL_BYTE_DATA_STREAM   0x40

#define SSD1306_I2C_BUS          I2C_NUM_0
// Not sure if there is some arduino pin mapping going on
#define ESP32_SDA1          GPIO_NUM_5
#define ESP32_SCL1          GPIO_NUM_4
#define ESP32_FREQ_HZ       1000000
//#define ESP32_I2C_BUF_DISABLE_FLAG  0

// Pulling from Adafruit lib, but not sure our D-Duino clone *actually* uses I2C
// for the SSD
#define SSD1306_I2C_ADDRESS   0x3C  // 011110+SA0+RW - 0x3C or 0x3D
// Address for 128x32 is 0x3C
// Address for 128x64 is 0x3D (default) or 0x3C (if SA0 is grounded)


namespace frab = framework_abstraction;

typedef frab::layer1::i2c<SSD1306_I2C_BUS, ESP32_SDA1, ESP32_SCL1> i2c_t;
// for this to work, we must expose stop() call since auto stop isn't happening
typedef i2c_t::tx<false, true> tx_t;

i2c_t i2c;

#if defined(ESP32_I2C_BATCH) || defined(ESP32_REPEAT_START)
#ifdef ESP32_REPEAT_START_EXP
frab::experimental::placement_helper<tx_t> _tx;
#else
frab::experimental::placement_helper<frab::layer1::i2c_tx_master> _tx;
#endif
#endif

extern "C" void init_board(GDisplay *g)
{
    i2c.config(ESP32_FREQ_HZ);
}

#ifdef ESP32_I2C_BATCH_EXP
static volatile bool command_mode;    
#endif

#ifdef ESP32_REPEAT_START
// flag denoting that we have queued some data
bool first_command_sent = false;
#endif

#if defined(ESP32_I2C_BATCH) || defined(ESP32_I2C_BATCH_EXP) || defined(ESP32_REPEAT_START)
extern "C" void acquire_bus(GDisplay *g) 
{
    ESP_LOGD(SSD_1306_TAG, "Acquiring bus");
#ifdef ESP32_REPEAT_START
    ESP_LOGD(SSD_1306_TAG, "Acquiring bus: repeat start");
    first_command_sent = false;
    _tx.construct();
#elif defined(ESP32_I2C_BATCH)
    _tx.construct();
    _tx.get().addr(SSD1306_I2C_ADDRESS);
#endif
#ifdef ESP32_I2C_BATCH_EXP
    command_mode = false;    
#endif
    ESP_LOGD(SSD_1306_TAG, "Acquired bus");
}

#ifdef SSD1306_FASTMODE_EXP
void ssd1306_i2c_post_init_board(GDisplay* g)
{

}
#endif

extern "C" void release_bus(GDisplay *g) 
{
    ESP_LOGD(SSD_1306_TAG, "Releasing bus");
#if defined(ESP32_I2C_BATCH) || defined(ESP32_REPEAT_START)
#ifdef ESP32_REPEAT_START_EXP
    _tx.get().stop();
#else
    _tx.get().commit(SSD1306_I2C_BUS);
#endif
    _tx.destroy();
#endif
}
#endif

// Batching almost works, and seems way faster
#ifdef ESP32_REPEAT_START
void write_repeat_start_helper()
{
    ESP_LOGV(SSD_1306_TAG, "rpt_start 1");

    auto& tx = _tx.get();
    if(first_command_sent) 
    {
        ESP_LOGV(SSD_1306_TAG, "rpt_start 1.1");
#ifndef ESP32_REPEAT_START_EXP
        tx.send(SSD1306_I2C_BUS);
#endif
        _tx.recycle(); // repeat START signal sent here (in constructor)
        ESP_LOGV(SSD_1306_TAG, "rpt_start 1.2");

    }
    else
    {
        first_command_sent = true;
    }
    tx.addr(SSD1306_I2C_ADDRESS);
    ESP_LOGV(SSD_1306_TAG, "rpt_start 2");
    //tx.repeat_start_experimental();
}
#endif


extern "C" void write_cmd(GDisplay *g, uint8_t cmd) 
{
#if defined(ESP32_I2C_BATCH) || defined(ESP32_REPEAT_START)
    auto& tx = _tx.get();
#ifdef ESP32_REPEAT_START
    write_repeat_start_helper();
#endif
#else
    auto tx = i2c.get_tx(SSD1306_I2C_ADDRESS);
#endif

    ESP_LOGV(SSD_1306_TAG, "write_cmd 1");

#ifdef ESP32_I2C_BATCH_EXP
    if(!command_mode)
    {
        tx.write(OLED_CONTROL_BYTE_CMD_STREAM);
        command_mode = true;
    }
#else
    tx.write(OLED_CONTROL_BYTE_CMD_SINGLE);
#endif

    tx.write(cmd);

    ESP_LOGV(SSD_1306_TAG, "write_cmd 2");
}

extern "C" void write_data(GDisplay *g, uint8_t* data, uint16_t length)
{
#if defined(ESP32_I2C_BATCH) || defined(ESP32_REPEAT_START)
    auto& tx = _tx.get();
#ifdef ESP32_REPEAT_START
    write_repeat_start_helper();
#endif
#else
    auto tx = i2c.get_tx(SSD1306_I2C_ADDRESS);
#endif

    ESP_LOGV(SSD_1306_TAG, "write_data 1");

    tx.write(OLED_CONTROL_BYTE_DATA_STREAM);
    tx.write(data, length); //, false); // disabled, SOME example somewhere had this true, but not yanbe's

    ESP_LOGV(SSD_1306_TAG, "write_data 1");
}