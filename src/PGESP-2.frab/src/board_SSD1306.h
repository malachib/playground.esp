/*
 * This file is subject to the terms of the GFX License. If a copy of
 * the license was not distributed with this file, you can obtain one at:
 *
 *              http://ugfx.org/license.html
 */

#ifndef _GDISP_LLD_BOARD_H
#define _GDISP_LLD_BOARD_H

#ifdef __cplusplus
extern "C" {
#endif

#define USE_I2C

#define SSD_1306_TAG "SSD1306"

#ifdef USE_I2C

// Almost works, and seems faster, but glitchy
// Seems that one could detect and use OLED_CONTROL_BYTE_CMD_STREAM mode but 
// some fancy queing and observing of bus release would have to happen
//#define ESP32_I2C_BATCH

// this makes things crash
//#define ESP32_I2C_BATCH_EXP

// simplistic repeat-start to hold on to bus.  does NOT attempt to batch
// commands together on its own
// TODO: clean it up and make it a permenant part of things
#define ESP32_REPEAT_START

// Enable fancier/more portable tx stop-bypass
// Does not work yet.  Code is there, compiles, doesnt crash but just blank screen
//#define ESP32_REPEAT_START_EXP

// Enable speed boost over initial i2c bus speed during init_board
// experimental and not only stubbed out at this time
#define SSD1306_FASTMODE_EXP


#endif


// TODO: try to associate this with GDisplay if convenient
// NOTE: Looks like special allocations need to happen for DMA, as described 
// here https://esp-idf.readthedocs.io/en/v2.0/api/peripherals/spi_master.html#transaction-data
void init_board(GDisplay *g);

void ssd1306_i2c_post_init_board(GDisplay* g);

static GFXINLINE void post_init_board(GDisplay *g) {
#ifdef SSD1306_FASTMODE_EXP
	ssd1306_i2c_post_init_board(g);
#endif
	(void) g;
}

static GFXINLINE void setpin_reset(GDisplay *g, bool_t state) {
	(void) g;
	(void) state;
}

#if defined(ESP32_I2C_BATCH) || defined(ESP32_I2C_BATCH_EXP) || defined(ESP32_REPEAT_START)
void acquire_bus(GDisplay *g);
void release_bus(GDisplay *g);
#else
static GFXINLINE void acquire_bus(GDisplay *g) {
	(void) g;
}

static GFXINLINE void release_bus(GDisplay *g) {
	(void) g;
}
#endif

void write_cmd(GDisplay *g, uint8_t cmd);

void write_data(GDisplay *g, uint8_t* data, uint16_t length);

#ifdef __cplusplus
// close extern "C"
}
#endif

#endif /* _GDISP_LLD_BOARD_H */
