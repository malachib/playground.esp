# Overview

# Detail

# Parts Tested

## MCU

## NeoPixel Ring - 16x5050

As per [3]

# Troubleshooting

Looks like with FeatherWing attached, PWR (D10) is always low.  Also looks like D11 stays low too.

# Results

| -----   | -------     |
| Date    | Board       |
| -----   | -------     |
| 05JAN24 | FeatherS3   |

# References

1. https://learn.adafruit.com/adafruit-prop-maker-featherwing
    1. https://learn.adafruit.com/adafruit-prop-maker-featherwing/arduino-code
2. https://components.espressif.com/components/zorxx/neopixel
    1. https://github.com/zorxx/neopixel/blob/main/example/main/main.c
3. https://www.adafruit.com/product/1463 
4. https://esp32s3.com/feathers3.html
    1. https://www.adafruit.com/product/5399
5. https://github.com/codewitch-honey-crisis/htcw_rmt_led_strip
6. https://components.espressif.com/components/espressif/led_strip 
    1. https://github.com/espressif/idf-extra-components/blob/master/led_strip/examples/led_strip_rmt_ws2812/main/led_strip_rmt_ws2812_main.c