#include <algorithm>

#include <esp_log.h>

#include <embr/platform/esp-idf/gpio.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

extern "C" {
#include "neopixel.h"
}

#include "led_strip.h"

#if CONFIG_BOARD_ESP32S3_UM_FEATHERS3
// [4]
#define NEOPIXEL_PIN 33     // (D5 [1])
//#define NEOPIXEL_PIN 40     // BUILTIN
#define POW_PIN 3           // (D10 [1])
#define RED_LED_PIN 7
#define GREEN_LED_PIN 10
#define BLUE_LED_PIN 11
#define STATUS_LED 13
#else
#error Only FeatherS3 supported at this time
#endif

static constexpr const char* TAG = "PGESP-39";
static constexpr embr::esp_idf::gpio
    power_pin((gpio_num_t)POW_PIN),
    red_led((gpio_num_t)RED_LED_PIN),
    green_led((gpio_num_t)GREEN_LED_PIN),
    blue_led((gpio_num_t)BLUE_LED_PIN);

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

static constexpr unsigned pixel_count = CONFIG_PIXEL_COUNT;

static void test1(void)
{
   tNeopixelContext neopixel = neopixel_Init(pixel_count, NEOPIXEL_PIN);
   tNeopixel pixel[] =
   {
       { 0, NP_RGB(50, 0,  0) }, /* red */
       { 0, NP_RGB(0,  50, 0) }, /* green */
       { 0, NP_RGB(0,  0,  0) }, /* off */
   };

   ESP_LOGI(TAG, "[%s] Starting", __func__);
   for(int i = 0; i < ARRAY_SIZE(pixel); ++i)
   {
      neopixel_SetPixel(neopixel, &pixel[i], 1);
      vTaskDelay(pdMS_TO_TICKS(1000));
   }
   ESP_LOGI(TAG, "[%s] Finished", __func__);
   neopixel_Deinit(neopixel);
}


void test2(unsigned iterations)
{
    //static constexpr const char* TAG = "test2";
    tNeopixelContext neopixel = neopixel_Init(pixel_count, NEOPIXEL_PIN);

    uint32_t refreshRate = neopixel_GetRefreshRate(neopixel);
    uint32_t taskDelay = std::max(1U, (unsigned)pdMS_TO_TICKS(1000UL / refreshRate));

    ESP_LOGI(TAG, "[%s] Starting", __func__);
    for(int i = 0; i < iterations * pixel_count; ++i)
    {
        tNeopixel pixel[] =
        {
            { (i)   % pixel_count, NP_RGB(0, 0,  0) },
            { (i+5) % pixel_count, NP_RGB(0, 50, 0) }, /* green */
        };

        neopixel_SetPixel(neopixel, pixel, ARRAY_SIZE(pixel));
        vTaskDelay(taskDelay);
    }

    ESP_LOGI(TAG, "[%s] Finished", __func__);
    neopixel_Deinit(neopixel);
}

// From https://github.com/espressif/idf-extra-components/blob/master/led_strip/examples/led_strip_rmt_ws2812/main/led_strip_rmt_ws2812_main.c
// (no tagged version)

led_strip_handle_t test3()
{
    led_strip_handle_t led_strip;

    /* LED strip initialization with the GPIO and pixels number*/
    led_strip_config_t strip_config = {
        .strip_gpio_num = NEOPIXEL_PIN, // The GPIO that connected to the LED strip's data line
        .max_leds = pixel_count, // The number of LEDs in the strip,
        .led_pixel_format = LED_PIXEL_FORMAT_GRB, // Pixel format of your LED strip
        .led_model = LED_MODEL_WS2812, // LED strip model
        //.flags {
        //    invert_out = false, // whether to invert the output signal (useful when your hardware has a level inverter)
        //}
    };

    strip_config.flags.invert_out = false;

    led_strip_rmt_config_t rmt_config = {
    #if ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5, 0, 0)
        .rmt_channel = 0,
    #else
        .clk_src = RMT_CLK_SRC_DEFAULT, // different clock source can lead to different power consumption
        .resolution_hz = 10 * 1000 * 1000, // 10MHz

        // 800Khz (arduino code seems to use this [1]) NEO_KHZ800, but don't be fooled
        // that was + NEO_GRB which takes it higher
        //.resolution_hz = 800 * 1000,
        //.flags {
        //    with_dma = false // whether to enable the DMA feature
        //}
    #endif
    };

    rmt_config.mem_block_symbols = 0;
    rmt_config.flags.with_dma = false;

    ESP_ERROR_CHECK(led_strip_new_rmt_device(&strip_config, &rmt_config, &led_strip));
    return led_strip;
}


#define PIN_BIT(x) (1ULL<<x)

void init_gpio()
{
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;

    io_conf.pin_bit_mask =
        PIN_BIT(POW_PIN) |
        PIN_BIT(RED_LED_PIN) |
        PIN_BIT(GREEN_LED_PIN) |
        PIN_BIT(BLUE_LED_PIN);
    gpio_config(&io_conf);
}

extern "C" void app_main()
{
    ESP_LOGI(TAG, "app_main: entry");

    init_gpio();

    //power_pin.set_direction(GPIO_MODE_OUTPUT);
    ESP_ERROR_CHECK(power_pin.level(0));
    //ESP_ERROR_CHECK(gpio_set_level((gpio_num_t)POW_PIN, 1));

    led_strip_handle_t led_strip = test3();

    //gpio_dump_io_configuration();

    bool led_on_off = false;
    int rotator = 0;

    for(;;)
    {
        //test1();
        //test2(100);
        //vTaskDelay(10);
        //ESP_ERROR_CHECK(power_pin.level(led_on_off));
        ESP_ERROR_CHECK(gpio_set_level((gpio_num_t)POW_PIN, 1));
        ESP_ERROR_CHECK(red_led.level(led_on_off));
        ESP_ERROR_CHECK(green_led.level(led_on_off));
        ESP_ERROR_CHECK(blue_led.level(led_on_off));

        // Exampled from [6.1]
        if (led_on_off) {
            if(++rotator % pixel_count == 0)    rotator = 0;

            for (int i = 0; i < pixel_count; i++) {

                if(rotator == i)
                    ESP_ERROR_CHECK(led_strip_set_pixel(led_strip, i, 255, 0, 0));
                else
                    // Neopixelx16 ring has a brigher blue bias
                    ESP_ERROR_CHECK(led_strip_set_pixel(led_strip, i, 10, 10, 5));

            }
            /* Refresh the strip to send data */
            ESP_ERROR_CHECK(led_strip_refresh(led_strip));
        } else {
            /* Set all LED off to clear all pixels */
            ESP_ERROR_CHECK(led_strip_clear(led_strip));
        }

        ESP_LOGD(TAG, "LED state=%u", led_on_off);

        led_on_off = !led_on_off;
        vTaskDelay(pdMS_TO_TICKS(500));
    }
}