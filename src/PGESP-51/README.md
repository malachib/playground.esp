# Overview

Document v0.1

# 1. Goals

Research of a mesh-able iBeacon, if such a thing is practical

# 2. RESERVED

# 3. Observations

## 3.1. Recollections

I recall the iBeacon spec downloaded from Apple does mention mesh behaviors

## 3.2. Search Results (unfavorable)

Right from the get go, searching from "iBeacon mesh" doesn't produce favorable results.  Articles entitled
"move over iBeacon for BLE Mesh Beacon"

Closest hit I got was [1] and this is very much not a mesh as one would expect it.

# References

1. https://docs.lumenrad.io/miraos/2.7.1/api/bluetooth/ble-beacons.html
