Document v0.1

# 1. Goals

# 2. Infrastructure

Tested against ESP-IDF v5.2.2

# 3. Opinions & Observations

## 3.1. Overall Compatibility

### 3.1.1. Compilation

Feels almost compileable, but enough barriers are present to qualify this as a port effort (Section 4.2, 4.3).  Unknown how well it actually functions once those compilation errors are resolved

### 3.1.2. Performance

Naturally until 3.1.2. issues are resolved, we are not able to easily ascertain
performance metrics

## 3.2. Exceptions

zmq depends on exceptions

# 4. Troubleshooting and Diagnostics

## 4.1. install(EXPORT) issues

As was seen with estd during its static linking, we get:

```
CMake Error: install(EXPORT "ZeroMQ-targets" ...) includes target "libzmq-static" which requires target "__idf_cxx" that is not in any export set.
CMake Error: install(EXPORT "ZeroMQ-targets" ...) includes target "libzmq-static" which requires target "__idf_newlib" that is not in any export set.
CMake Error: install(EXPORT "ZeroMQ-targets" ...) includes target "libzmq-static" which requires target "__idf_freertos" that is not in any export set.
CMake Error: install(EXPORT "ZeroMQ-targets" ...) includes target "libzmq-static" which requires target "__idf_esp_hw_support" that is not in any export set.
CMake Error: install(EXPORT "ZeroMQ-targets" ...) includes target "libzmq-static" which requires target "__idf_heap" that is not in any export set.
CMake Error: install(EXPORT "ZeroMQ-targets" ...) includes target "libzmq-static" which requires target "__idf_log" that is not in any export set.
CMake Error: install(EXPORT "ZeroMQ-targets" ...) includes target "libzmq-static" which requires target "__idf_soc" that is not in any export set.
CMake Error: install(EXPORT "ZeroMQ-targets" ...) includes target "libzmq-static" which requires target "__idf_hal" that is not in any export set.
CMake Error: install(EXPORT "ZeroMQ-targets" ...) includes target "libzmq-static" which requires target "__idf_esp_rom" that is not in any export set.
CMake Error: install(EXPORT "ZeroMQ-targets" ...) includes target "libzmq-static" which requires target "__idf_esp_common" that is not in any export set.
CMake Error: install(EXPORT "ZeroMQ-targets" ...) includes target "libzmq-static" which requires target "__idf_esp_system" that is not in any export set.
CMake Error: install(EXPORT "ZeroMQ-targets" ...) includes target "libzmq-static" which requires target "__idf_xtensa" that is not in any export set.
```

Setting both BUILD_STATIC and BUILD_SHARED to OFF helps, though I have not yet reached
a full compile and link phase to be sure it fully solves things.

## 4.2. ESP-IDF strictness

A stray `%u` needs to be `%lu`, hand editing this for now

## 4.3. ESP-IDF posix

### 4.3.1. sockets

`ip.cpp` is doing some socket operations that ESP-IDF is not happy with:

- AF_UNIX
- socketpair [1]

According to `ip.cpp` ZMQ_HAVE_OPENVMS mode doesn't rely on socketpair, so
we may try activating that

# References

1. https://github.com/espressif/esp-idf/issues/13772