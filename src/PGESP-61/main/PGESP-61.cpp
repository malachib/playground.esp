#include <esp_heap_caps.h>
#include <esp_log.h>
#include <esp_system.h>

static const char* TAG = "PGESP-61";

extern "C" void app_main(void)
{
    //heap_caps_dump(MALLOC_CAP_INTERNAL);
    heap_caps_print_heap_info(MALLOC_CAP_INTERNAL);

    ESP_LOGI(TAG, "app_main: phase 1");

    //heap_caps_dump(MALLOC_CAP_INTERNAL);
    heap_caps_print_heap_info(MALLOC_CAP_INTERNAL);
}
