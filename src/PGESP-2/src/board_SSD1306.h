/*
 * This file is subject to the terms of the GFX License. If a copy of
 * the license was not distributed with this file, you can obtain one at:
 *
 *              http://ugfx.org/license.html
 */

#ifndef _GDISP_LLD_BOARD_H
#define _GDISP_LLD_BOARD_H

#include "esp_system.h"
#include "soc/gpio_struct.h"
#include "driver/gpio.h"
#include <string.h>

#include "esp_err.h"
#include "esp_log.h"

#define USE_I2C

#ifdef USE_I2C
#include "driver/i2c.h"
#else
#include "driver/spi_master.h"
#endif

#define SSD_1306_TAG "SSD1306"

// Raw stubs, adapt from:
// SPI call ref: https://github.com/espressif/esp-idf/blob/master/examples/peripherals/spi_master/main/spi_master_example_main.c
// SSD1306 ref: https://github.com/goeck/SSD1306/blob/master/boards/addons/gdisp/board_SSD1306_spi.h
//#define SSD1306_PAGE_PREFIX		0x40

// FIX: just remembered, D-Duino uses I2C not SPI...

// FIX: all these are for a different board
// For a multiple display configuration we would put all this in a structure and then
//      set g->board to that structure.
/*
#define SSD1306_RESET_PORT              GPIOB
#define SSD1306_RESET_PIN               5
#define SSD1306_CS_PORT                 GPIOA
#define SSD1306_CS_PIN                  4
#define SSD1306_SCK_PORT                GPIOA
#define SSD1306_SCK_PIN                 5
#define SSD1306_MISO_PORT               GPIOA
#define SSD1306_MISO_PIN                6
#define SSD1306_MOSI_PORT               GPIOA
#define SSD1306_MOSI_PIN                7 */

#ifdef USE_I2C

// this somehow breaks things (nothing draws)
//#define ESP32_I2C_BATCH

// this makes things crash
//#define ESP32_I2C_BATCH_EXP

// this doesn't help
//#define EXP_OLED_INIT

// lifted from https://github.com/yanbe/ssd1306-esp-idf-i2c
// Control byte
#define OLED_CONTROL_BYTE_CMD_SINGLE    0x80
#define OLED_CONTROL_BYTE_CMD_STREAM    0x00
#define OLED_CONTROL_BYTE_DATA_STREAM   0x40

#define SSD1306_I2C_BUS          I2C_NUM_0
//#define ESP32_SDA1          25
//#define ESP32_SCL1          19
// Not sure if there is some arduino pin mapping going on
#define ESP32_SDA1          GPIO_NUM_5
#define ESP32_SCL1          GPIO_NUM_4
// Works at 1000000 but keeping it at 100000 as we experiment
#define ESP32_FREQ_HZ       100000
#define ESP32_I2C_BUF_DISABLE_FLAG  0

// Pulling from Adafruit lib, but not sure our D-Duino clone *actually* uses I2C
// for the SSD
#define SSD1306_I2C_ADDRESS   0x3C  // 011110+SA0+RW - 0x3C or 0x3D
// Address for 128x32 is 0x3C
// Address for 128x64 is 0x3D (default) or 0x3C (if SA0 is grounded)

//#define SSD1306_I2C_ADDR    0   // FIX: need to assign this

#define WRITE_BIT  I2C_MASTER_WRITE /*!< I2C master write */
#define READ_BIT   I2C_MASTER_READ  /*!< I2C master read */

#define ACK_CHECK_EN   0x1     /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS  0x0     /*!< I2C master will not check ack from slave */

#else

static spi_device_handle_t spi;


#ifdef USE_VSPI
// according to https://learn.sparkfun.com/tutorials/esp32-thing-hookup-guide
// the pin mappings are:
// MOSI = 23
// MISO = 19
// SCLK = 18
// SS = 5

// Unverified, but should work - lifted and adapted from spi_master_example (guessing its VSPI)
#define SSD1306_RESET_PIN   18  // Be sure to remember to wire this one up
#define SSD1306_CS_PIN      22
#define SSD1306_SCK_PIN     19
#define SSD1306_MISO_PIN    25
#define SSD1306_MOSI_PIN    23
#else
// Thinking these are HSPI, from https://esp32.com/viewtopic.php?f=13&t=1683
#define SSD1306_RESET_PIN   25  // Be sure to remember to wire this one up.  Odd that this is MISO pin elsewhere...
#define SSD1306_CS_PIN      15
#define SSD1306_SCK_PIN     14
#define SSD1306_MISO_PIN    12
#define SSD1306_MOSI_PIN    13
#endif
#endif

// Fundamental commands (pg.28)
#define OLED_CMD_SET_CONTRAST           0x81    // follow with 0x7F
#define OLED_CMD_DISPLAY_RAM            0xA4
#define OLED_CMD_DISPLAY_ALLON          0xA5
#define OLED_CMD_DISPLAY_NORMAL         0xA6
#define OLED_CMD_DISPLAY_INVERTED       0xA7
#define OLED_CMD_DISPLAY_OFF            0xAE
#define OLED_CMD_DISPLAY_ON             0xAF

// Addressing Command Table (pg.30)
#define OLED_CMD_SET_MEMORY_ADDR_MODE   0x20    // follow with 0x00 = HORZ mode = Behave like a KS108 graphic LCD
#define OLED_CMD_SET_COLUMN_RANGE       0x21    // can be used only in HORZ/VERT mode - follow with 0x00 and 0x7F = COL127
#define OLED_CMD_SET_PAGE_RANGE         0x22    // can be used only in HORZ/VERT mode - follow with 0x00 and 0x07 = PAGE7

// Hardware Config (pg.31)
#define OLED_CMD_SET_DISPLAY_START_LINE 0x40
#define OLED_CMD_SET_SEGMENT_REMAP      0xA1    
#define OLED_CMD_SET_MUX_RATIO          0xA8    // follow with 0x3F = 64 MUX
#define OLED_CMD_SET_COM_SCAN_MODE      0xC8    
#define OLED_CMD_SET_DISPLAY_OFFSET     0xD3    // follow with 0x00
#define OLED_CMD_SET_COM_PIN_MAP        0xDA    // follow with 0x12
#define OLED_CMD_NOP                    0xE3    // NOP

// Timing and Driving Scheme (pg.32)
#define OLED_CMD_SET_DISPLAY_CLK_DIV    0xD5    // follow with 0x80
#define OLED_CMD_SET_PRECHARGE          0xD9    // follow with 0xF1
#define OLED_CMD_SET_VCOMH_DESELCT      0xDB    // follow with 0x30

// Charge Pump (pg.62)
#define OLED_CMD_SET_CHARGE_PUMP        0x8D    // follow with 0x14

// TODO: try to associate this with GDisplay if convenient
// NOTE: Looks like special allocations need to happen for DMA, as described 
// here https://esp-idf.readthedocs.io/en/v2.0/api/peripherals/spi_master.html#transaction-data
static GFXINLINE void init_board(GDisplay *g) 
{
#ifdef USE_I2C
    // I2C master init, adapted from https://github.com/espressif/esp-idf/blob/master/examples/peripherals/i2c/main/i2c_example_main.c
    int i2c_master_port = SSD1306_I2C_BUS;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = ESP32_SDA1;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = ESP32_SCL1;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = ESP32_FREQ_HZ;
    ESP_ERROR_CHECK(i2c_param_config(i2c_master_port, &conf));
    ESP_ERROR_CHECK(i2c_driver_install(i2c_master_port, conf.mode,
                       ESP32_I2C_BUF_DISABLE_FLAG,
                       ESP32_I2C_BUF_DISABLE_FLAG, 0));

#ifdef EXP_OLED_INIT
    // ++++++++++++++++++++
    // uGFX should be doing all this, but lifting out of ssd1306-esp-idf-i2c
    // just in hopes it helps
    esp_err_t espRc;

	i2c_cmd_handle_t cmd = i2c_cmd_link_create();

	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (SSD1306_I2C_ADDRESS << 1) | I2C_MASTER_WRITE, true);
	i2c_master_write_byte(cmd, OLED_CONTROL_BYTE_CMD_STREAM, true);

	i2c_master_write_byte(cmd, OLED_CMD_SET_CHARGE_PUMP, true);
	i2c_master_write_byte(cmd, 0x14, true);

	i2c_master_write_byte(cmd, OLED_CMD_SET_SEGMENT_REMAP, true); // reverse left-right mapping
	i2c_master_write_byte(cmd, OLED_CMD_SET_COM_SCAN_MODE, true); // reverse up-bottom mapping

	i2c_master_write_byte(cmd, OLED_CMD_DISPLAY_ON, true);
	i2c_master_stop(cmd);

	espRc = i2c_master_cmd_begin(SSD1306_I2C_BUS, cmd, 1000/portTICK_PERIOD_MS);
	if (espRc == ESP_OK) {
		ESP_LOGI(SSD_1306_TAG, "OLED configured successfully");
	} else {
		ESP_LOGE(SSD_1306_TAG, "OLED configuration failed. code: 0x%.2X", espRc);
	}
	i2c_cmd_link_delete(cmd);
    // ---------------------
#endif

#else
        unsigned        i;

        // As we are not using multiple displays we set g->board to NULL as we don't use it.
        g->board = 0;

		spi_bus_config_t buscfg={
			.miso_io_num=SSD1306_MISO_PIN,
			.mosi_io_num=SSD1306_MOSI_PIN,
			.sclk_io_num=SSD1306_SCK_PIN,
			.quadwp_io_num=-1,
			.quadhd_io_num=-1
		};

		spi_device_interface_config_t devcfg={
			.clock_speed_hz=10000000,               //Clock out at 10 MHz
			.mode=0,                                //SPI mode 0
			.spics_io_num=SSD1306_CS_PIN,           //CS pin
			.queue_size=7,                          //We want to be able to queue 7 transactions at a time
			//.pre_cb=ili_spi_pre_transfer_callback,  //Specify pre-transfer callback to handle D/C line
    	};
			
		//Initialize the SPI bus
		esp_err_t ret=spi_bus_initialize(HSPI_HOST, &buscfg, 1);
		assert(ret==ESP_OK);
		//Attach the LCD to the SPI bus
		ret=spi_bus_add_device(HSPI_HOST, &devcfg, &spi);
		assert(ret==ESP_OK);

/*
        switch(g->controllerdisplay) {
        case 0:                                                                                 // Set up for Display 0
                // RESET pin.
                palSetPadMode(SSD1306_RESET_PORT, SSD1306_RESET_PIN, PAL_MODE_OUTPUT_PUSHPULL);

                palSetPadMode(SSD1306_MISO_PORT, SSD1306_MISO_PIN,      PAL_MODE_OUTPUT_PUSHPULL|
                               PAL_STM32_OSPEED_HIGHEST);
                palSetPadMode(SSD1306_MOSI_PORT, SSD1306_MOSI_PIN,      PAL_MODE_ALTERNATE(0)|
                                                                                                                        PAL_STM32_OSPEED_HIGHEST);
                palSetPadMode(SSD1306_SCK_PORT,  SSD1306_SCK_PIN,       PAL_MODE_ALTERNATE(0)|
                                                                                                                        PAL_STM32_OSPEED_HIGHEST);
                palSetPad(SSD1306_CS_PORT, SSD1306_CS_PIN);
                palSetPadMode(SSD1306_CS_PORT,   SSD1306_CS_PIN,        PAL_MODE_OUTPUT_PUSHPULL|
                                                                                                                        PAL_STM32_OSPEED_HIGHEST);
                break;
        } */
#endif
}

static GFXINLINE void post_init_board(GDisplay *g) {
	(void) g;
}

static GFXINLINE void setpin_reset(GDisplay *g, bool_t state) {
	(void) g;
	(void) state;
}

#ifdef ESP32_I2C_BATCH
volatile i2c_cmd_handle_t ssd1306_i2c_cmd;
#endif

static GFXINLINE void acquire_bus(GDisplay *g) {
#ifdef ESP32_I2C_BATCH
    ESP_LOGD(SSD_1306_TAG, "Acquiring bus");
    ssd1306_i2c_cmd = i2c_cmd_link_create();
#ifndef ESP32_I2C_BATCH_EXP
    ESP_ERROR_CHECK(i2c_master_start(ssd1306_i2c_cmd));
#endif
#endif
	(void) g;
}

static GFXINLINE void release_bus(GDisplay *g) {
#ifdef ESP32_I2C_BATCH
#ifndef ESP32_I2C_BATCH_EXP
    ESP_ERROR_CHECK(i2c_master_stop(ssd1306_i2c_cmd));
#endif
    ESP_LOGD(SSD_1306_TAG, "Releasing bus");
    esp_err_t ret = i2c_master_cmd_begin(SSD1306_I2C_BUS, ssd1306_i2c_cmd, 1000 / portTICK_RATE_MS);
    ESP_ERROR_CHECK(ret);
    i2c_cmd_link_delete(ssd1306_i2c_cmd);
#endif
	(void) g;
}

static GFXINLINE void write_cmd(GDisplay *g, uint8_t cmd) {
#ifdef USE_I2C
#ifndef ESP32_I2C_BATCH
    // pieced together from ESP32 example code.  Not sure if this is gonna work out
    // clues from https://github.com/nkolban/esp32-snippets/blob/master/hardware/accelerometers/mpu6050.c
    i2c_cmd_handle_t c = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(c));
#else
    //ESP_LOGI(SSD_1306_TAG, "BATCH write cmd");
    i2c_cmd_handle_t c = ssd1306_i2c_cmd;
#ifdef ESP32_I2C_BATCH_EXP
    ESP_ERROR_CHECK(i2c_master_start(c));
#endif
#endif
    ESP_ERROR_CHECK(i2c_master_write_byte(c, ( SSD1306_I2C_ADDRESS << 1 ) | WRITE_BIT, ACK_CHECK_EN));
    ESP_ERROR_CHECK(i2c_master_write_byte(c, OLED_CONTROL_BYTE_CMD_SINGLE, ACK_CHECK_EN));
    ESP_ERROR_CHECK(i2c_master_write_byte(c, cmd, ACK_CHECK_EN));
#ifndef ESP32_I2C_BATCH
    ESP_ERROR_CHECK(i2c_master_stop(c));
    esp_err_t ret = i2c_master_cmd_begin(SSD1306_I2C_BUS, c, 1000 / portTICK_RATE_MS);
    ESP_ERROR_CHECK(ret);
    i2c_cmd_link_delete(c);
#else
#ifdef ESP32_I2C_BATCH_EXP
    ESP_ERROR_CHECK(i2c_master_stop(c));
#endif
#endif

#else
	// lifted straight from ESP32 example code
    esp_err_t ret;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=8;                     //Command is 8 bits
    t.tx_buffer=&cmd;               //The data is the cmd itself
    //t.user=(void*)0;                //D/C needs to be set to 0
    ret=spi_device_transmit(spi, &t);  //Transmit!
    assert(ret==ESP_OK);
#endif
}

static GFXINLINE void write_data(GDisplay *g, uint8_t* data, uint16_t length) {
#ifdef USE_I2C
#ifndef ESP32_I2C_BATCH
	// lifted straight from ESP32 example code
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
#else
    //ESP_LOGI(SSD_1306_TAG, "BATCH write data");
    i2c_cmd_handle_t cmd = ssd1306_i2c_cmd;
#ifdef ESP32_I2C_BATCH_EXP
    ESP_ERROR_CHECK(i2c_master_start(cmd));
#endif
#endif
    i2c_master_write_byte(cmd, ( SSD1306_I2C_ADDRESS << 1 ) | WRITE_BIT, ACK_CHECK_EN);
    ESP_ERROR_CHECK(i2c_master_write_byte(cmd, OLED_CONTROL_BYTE_DATA_STREAM, ACK_CHECK_EN));
    i2c_master_write(cmd, data, length, ACK_CHECK_EN);
#ifndef ESP32_I2C_BATCH
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    esp_err_t ret = i2c_master_cmd_begin(SSD1306_I2C_BUS, cmd, 1000 / portTICK_RATE_MS);
    ESP_ERROR_CHECK(ret);
    i2c_cmd_link_delete(cmd);
    //return ret;
#else
#ifdef ESP32_I2C_BATCH_EXP
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
#endif
#endif
#else
	// lifted straight from ESP32 example code
	esp_err_t ret;
    spi_transaction_t t;
    if (length==0) return;             //no need to send anything
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=length*8;                 //Len is in bytes, transaction length is in bits.
    t.tx_buffer=data;               //Data
    //t.user=(void*)1;                //D/C needs to be set to 1
    ret=spi_device_transmit(spi, &t);  //Transmit!
    assert(ret==ESP_OK);            //Should have had no issues.
#endif
}

#endif /* _GDISP_LLD_BOARD_H */
