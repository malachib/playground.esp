#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

extern "C" {
#include <gfx.h>
//#include <src/gdisp/gdisp.h>

#include "esp_err.h"
#include "esp_log.h"

portMUX_TYPE _hack_mux = portMUX_INITIALIZER_UNLOCKED;

}

void displayTask(void *pvParameters)
{
    static int counter = 0;
    
    for(;;)
    {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

const char tag[] = "displayTask2";

namespace ugfx {

struct display
{
    GDisplay* d;

public:
    display(GDisplay* d = GDISP) : d(d) {}

    inline coord_t width() { return gdispGGetWidth(d); }
    inline coord_t height() { return gdispGGetHeight(d); }

    inline void clear(color_t clear_to_color = Black) 
    { gdispGClear(d, clear_to_color); }
};

}

// Yanking this in direct from "basic" demo for gdisp
void displayTask2(void *pvParameters)
{
	coord_t		width, height;
	coord_t		i, j;

    ESP_LOGD(tag, "Display init start");

    // Initialize and clear the display
    gfxInit();

    ugfx::display d;

    ESP_LOGD(tag, "Display init done");

    // Get the screen size
    width = d.width();
    height = d.height();

    // Code Here
	gdispDrawBox(10, 10, width/2, height/2, White);
    gdispFillArea(width/2, height/2, width/2-10, height/2-10, White);
    gdispDrawLine(5, 30, width-50, height-40, White);

    while(TRUE) {
        for(i = 5, j = 0; i < width && j < height; i += 7, j += i/20)
            gdispDrawPixel(i, j, White);

        //gdisp_lld_display();

    	gfxSleepMilliseconds(5000);
        d.clear();

        ESP_LOGI(tag, "Display redraw");
    }       
}