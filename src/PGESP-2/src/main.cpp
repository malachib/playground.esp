#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "esp_err.h"
#include "esp_log.h"

void blinkenTask(void *pvParameters)
{
    static int counter = 0;

    //gpio_enable(gpio, GPIO_OUTPUT);
    while(1) {
        printf("Hello World %d\r\n", counter++);
        //gpio_write(gpio, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        //gpio_write(gpio, 0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

void displayTask(void *);
void displayTask2(void *);

extern "C" void app_main()
{
    // NOTE: seems to be having no effect
    esp_log_level_set("*", ESP_LOG_VERBOSE);
    
    // FIX: 90% sure vTaskStartScheduler already started by the time we get here
    // but doublecheck

    xTaskCreate(blinkenTask, "blinkenTask", 2048, NULL, 2, NULL);
    xTaskCreate(displayTask2, "displayTask", 2048, NULL, 2, NULL);
}