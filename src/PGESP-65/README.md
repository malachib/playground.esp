
# 1. Scope

BLE ESP32 Central to Linux Peripheral (bluez) diagnostic.
Specifically to address discovery issue as per [4]

# 2. Infrastructure

Specifically we have a synthetic peripheral via `periphal` script using bluez python bindings [1]

## 2.1. Espressif devices

### 2.1.1. Seeed ESP32C6

### 2.1.2. Unexpected Maker ESP32S3

## 2.2. Pixel 7 + BLE Scanner

Google Pixel 7 - Android 14
BLE Scanner [5] v3.31

## 2.3. BTMON

Used to capture many of the logs in `logs`

## 2.4. Bluez

## 2.5. Rover

Debian laptop running bluez.  Laptop used for 'peripheral' script testing
unless otherwise stated

## 2.6. peripheral script v0.2

One of two modes:

* Completely custom "BEEF" service or
* Anemic ANS service

# 3. Opinions & Observations

## 3.1. BLE startup

Looks like right away we stop advertising.  This correlates to `ble_svc_dis_init` call.  Spoke too soon, maybe not,
needs more testing

## 3.2. bluez

### 3.2.1. connect/disconnect events

Qt is on the record telling us we don't get connect/disconnect events until a characteristic is interacted with.  Similarly, I see no obvious connect/disconnect events [1] and some folks are inquiring about it [1.1]

It all is very DBus dependent and perhaps that can clue us in?

### 3.2.2. discoverability

Section 4 28SEP24 proves that discoverability of bluez peripheral is possible.

## 3.3. Similar Reports

iOS guy reports a similar 30s timeout [1.2]

## 3.4. bleak BLE library

Interesting, but appears to operate only as a client and we need a server

# 4. Findings

## 28SEP24 - Pixel 7 & ESP32C6

`periphal` script v0.1
Seeed ESP32C6 exhibits problem
Pixel 7 via BLE Scanner can happily connect

## 05OCT24 - synthetic & ESP32C6

Discovery timeout still takes 30s, so it's different than the
10000ms timeout specified on:

```c++
rc = ble_gap_connect(
        session.addr_type, addr, 10000,
        &conn_params,
        ble_gap_event, &session);
```

## 06OCT24 - synthetic & ESP32C6

Adding a 500ms delay before discovery results in success.
Hanging off BLE_GAP_EVENT_MTU also works (see today's notes)

## 08OCT24 - BLE_GAP_EVENT_LINK_ESTAB

This was added just a couple months ago as an Espressif-specific NimBLE event.
Their examples use this to initiate discovery.

## 11OCT24 - BLE_GAP_EVENT_LINK_ESTAB

Tested this today, same results as calling from BLE_GAP_EVENT_CONNECT - namely, group TX request goes out before connection request.

## 13OCT24 - S3 blecent -> Rover

`blecent` examples fails first connect to us in ANS, then kicks into general discovery (scan I think), connects, get MTU event, and succeeds in service discovery.  That MTU event is completely incidental though.

Test performed with 2.1.2.

Same strange connection behavior with different part to same linux destination corroborates the faulty bluez stack story

## 5. Journal

## 23SEP24

"Been struggling with this phase for a while" - missme project

## 05OCT24

* Setting `BT_NIMBLE_LOG_LEVEL` to `BT_NIMBLE_LOG_LEVEL_DEBUG`
* Taking logs of `btmon` and more verbose firmware

`ATT: Read By Group Type Response` transmits from synthetic during 05OCT24-2 session (marker 44.174228), but not 05OCT24-1.

Peculiar is `ATT: Read By Group Type Request` from Espressif
occurs just a moment before connection is established (marker 15.378139).  This may be an issue

Looks like my delay wasn't high enough with xTimerPendFunctionCall, need more.

## 06OCT24

Yep, delaying discovery fixes things.  That's kludgey, so instead hanging off MTU event.  Since it's not gaurunteed to get an MTU event (although it's typical to get one), we force it via
`ble_gattc_exchange_mtu`.

Even this is kludgey, since it forces extra wire traffic when occasionally none is required.  At least it doesn't seem to
initiate *2* MTU events

Stumbled on interesting C++ wrapper [9]

## 08OCT24

BLE_GAP_EVENT_LINK_ESTAB: Probably a good thing, but odd to the point of mildly concerning that a vendor specific feature is needed here at all.

## 09OCT24

Noticing that suggested solution [4] depends on brand new BLE_GAP_EVENT_LINK_ESTAB which isn't available in v5.3.1

## 11OCT24

At this point, observing BLE_GAP_EVENT_LINK_ESTAB ill effects starts me thinking some part of ESP-IDF/NimBLE BT stack has a race condition.

## 12OCT24

Inexplicably, ESTAB approach worked one and only one time.

### Python ANS

Attepting to run `blecent` example [10.1] however it needs an ANS peripheral.  I'd prefer not to have to fire up my entire Qt situation, so looking into doing it with Python.

Referencing ANS spec [12] to hopefully do this without too much incident.

Ah yes IAS was the simple one, ANS takes more effort.

### Stumbling on

bluez_peripheral source [1.3] - that could be useful.

Seperately, some interesting dbus Python examples of using BlueZ/BT [11] tells us about lower level support.

ESP-IDF also has high level support for ANS [10.2] - not directly relevant to PGESP-65 though.

## 6. Artifacts

### 6.1. Logs

#### 05OCT24-1

ESP32C6 -> synthetic

#### 05OCT24-2

BLE scanner -> synthetic

#### 05OCT24-3

ESP32C6 -> synthetic
With (very) minimal delay

#### 11OCT24-1

ESP32C6 -> synthetic
BLE_GAP_EVENT_LINK_ESTAB approach

# Terminology

* ACL = Asynchronous Connection-oriented Logical transport [6]
* HCI = Host Controller Interface [7]

# References

1. https://bluez-peripheral.readthedocs.io/en/stable/
    1. https://www.spinics.net/lists/linux-bluetooth/msg72485.html
    2. https://github.com/bluez/bluez/issues/153
    3. https://github.com/spacecheese/bluez_peripheral
2. https://github.com/hbldh/bleak
3. https://github.com/espressif/esp-idf/issues/10109
4. https://github.com/espressif/esp-idf/issues/14644
5. https://play.google.com/store/apps/details?id=com.macdom.ble.blescanner&hl=en_US
6. https://en.wikipedia.org/wiki/Asynchronous_connection-oriented_logical_transport
7. https://www.feasycom.com/bluetooth-host-controller-interface-hci.html
8. https://stackoverflow.com/questions/76813604/esp32-c3-nimble-client-server-implementation-unable-to-discover-services
9. https://github.com/h2zero/esp-nimble-cpp
10. ESP-IDF canonical reserved
    1. https://github.com/espressif/esp-idf/tree/v5.2.3/examples/bluetooth/nimble/blecent 
    2. https://docs.espressif.com/projects/esp-iot-solution/en/latest/bluetooth/ble_ans.html
11. https://git.kernel.org/pub/scm/bluetooth/bluez.git/tree/test/example-gatt-server
12. ANS_SPEC_V10.pdf