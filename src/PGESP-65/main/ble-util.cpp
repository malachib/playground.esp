#include <charconv>

#include <nimble/ble.h>
#include <host/ble_gap.h>
#include <host/ble_hs_adv.h>

static const char* TAG = "PGESP-65::ble-util";

// DEBT: Put this guy into embr, if we don't ultimately repair
// istream (https://github.com/malachi-iot/estdlib/issues/47)
bool parse_addr(const char* in, ble_addr_t* out)
{
    const char* start = in;

    for(int i = 5; i >= 0; i--)
    {
        int octet = 0;
        const char* const end = start + 2;
        
        // NOTE: We prefer estd flavor, but for PGESP keeping
        // dependencies light is ideal
        //estd::from_chars_result r = 
        std::from_chars(
            start, end, octet, 16);
        out->val[i] = octet;

        if(*end != ':')
        {
            // If happens before i == 0, error
            return i == 0;
        }

        start += 3;
    }

    return true;
}


void log_addr(const uint8_t addr_val[])
{
    ESP_LOGI(TAG, "Address: %02x:%02x:%02x:%02x:%02x:%02x",
        addr_val[5], addr_val[4], addr_val[3], addr_val[2], addr_val[1], addr_val[0]);
}