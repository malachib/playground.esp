#include <esp_log.h>

#include <freertos/FreeRTOS.h>
#include <freertos/timers.h>

#include <nimble/nimble_port.h>
#include <nimble/nimble_port_freertos.h>

#include <nimble/ble.h>
#include <host/ble_gap.h>
#include <host/ble_hs_adv.h>

extern "C" {
#include <services/gap/ble_svc_gap.h>
#include <services/gatt/ble_svc_gatt.h>
#include <services/dis/ble_svc_dis.h>
}

#include "app/ble.h"
#include "app/session.h"

static const char *device_name = "PGESP-65";
static const char* TAG = "PGESP-65";


// BLE_HS reasons quick ref
// 1  = BLE_HS_EAGAIN
// 7  = BLE_HS_ENOTCONN (saw one time), similar to [3]
// 13 = BLE_HS_ETIMEOUT
// 14 = BLE_HS_EDONE
// 19 = BLE_HS_ETIMEOUT_HCI

Session session;

#define BLE_HS_OK 0
#define ENABLE_MTU_DISC 0
#define ENABLE_DELAYED_DISC 0
#define ENABLE_ESTAB 1

static int ble_gatt_dsc_cb(
    uint16_t conn_handle,
    const ble_gatt_error* error,
    const ble_gatt_svc* service,
    void *arg)
{
    ESP_LOGI(TAG, "ble_gatt_dsc_cb: status=%d", error->status);
    return 0;
}

#if ENABLE_DELAYED_DISC
static TimerHandle_t timer_handle;
#endif

void start_discovery(TimerHandle_t)
{
    ESP_LOGI(TAG, "start_discovery: entry");

    // TODO: Reduce 30s timeout
    int ret = ble_gattc_disc_all_svcs(
        session.conn_handle, ble_gatt_dsc_cb, nullptr);
    //assert(ret == BLE_HS_OK);
    ESP_LOGI(TAG, "start_discovery: ret=%d", ret);
}



static int ble_gap_event(ble_gap_event* event, void* arg)
{
    // 0  = BLE_GAP_EVENT_CONNECT
    // 14 = BLE_GAP_EVENT_SUBSCRIBE
    // 15 = BLE_GAP_EVENT_MTU
    // 33 = BLE_GAP_EVENT_LINK_ESTAB
    ESP_LOGI(TAG, "ble_gap_event: type=%d", event->type);

    switch (event->type)
    {
        case BLE_GAP_EVENT_CONNECT:
        {
            const auto& c = event->connect;
            ESP_LOGI(TAG, "connection %s; status=%d",
                c.status == 0 ? "established" : "failed",
                c.status);

            if(c.status != 0) return 0;

            session.conn_handle = c.conn_handle;

#if ENABLE_DELAYED_DISC
            // Looks like we have to delay here, otherwise
            // disc request goes out before connection establishes
            // Some people [8] even put a vTaskDelay here!
            xTimerStart(timer_handle, portMAX_DELAY);
#elif ENABLE_MTU_DISC
            int ret = ble_gattc_exchange_mtu(c.conn_handle, nullptr, nullptr);
            assert(ret == 0);
#else
            // We know this fails, so disabling him
            //start_discovery(arg, 0);
#endif

            break;
        }

#if ENABLE_MTU_DISC
        case BLE_GAP_EVENT_MTU:
            assert(session.conn_handle == event->mtu.conn_handle);
            start_discovery({});
            break;
#elif ENABLE_ESTAB
        case BLE_GAP_EVENT_LINK_ESTAB:
            if(event->link_estab.status == BLE_HS_OK)
            {
                assert(session.conn_handle == event->connect.conn_handle);
                start_discovery({});
            }
            break;
#endif

        default:    break;
    }

    return 0;
}


// Initiate BLE connection as a central/master role
bool connect(const ble_addr_t* addr)
{
    int rc;

    // DEBT: Document the conn parameters - I have forgotten
    // the details and the relation between them all
    ble_gap_conn_params conn_params {};
    
    conn_params.scan_itvl = 0x0010,
    conn_params.scan_window = 0x0010,
    //conn_params.min_ce_len = 0x0010,
    //conn_params.max_ce_len = 0x0300,
    
    conn_params.itvl_min = BLE_GAP_INITIAL_CONN_ITVL_MIN;
    conn_params.itvl_max = BLE_GAP_INITIAL_CONN_ITVL_MAX;
    //conn_params.supervision_timeout = BLE_GAP_SUPERVISION_TIMEOUT_MS(1000);
    //conn_params.latency = 0;  // 0 for no latency
    conn_params.supervision_timeout = BLE_GAP_INITIAL_SUPERVISION_TIMEOUT;
    conn_params.latency = BLE_GAP_INITIAL_CONN_LATENCY;

    rc = ble_gap_connect(
        session.addr_type, addr, 10000,
        &conn_params,
        ble_gap_event, &session);

    assert(rc == BLE_HS_OK);
        
    return true;
}


static void ble_host_task(void *param)
{
    ESP_LOGI(TAG, "BLE Host Task Started");

    /* This function will return only when nimble_port_stop() is executed */
    nimble_port_run();

    nimble_port_freertos_deinit();
}


// "This callback is executed when the host and controller become synced.
//  This happens at startup and after a reset."
static void ble_on_sync(void)
{
    //static const char* TAG = "ble_htp_prph_on_sync";
    ESP_LOGI(TAG, "ble_on_sync: entry");
    
    int rc;

    // 1st param is privacy mode
    rc = ble_hs_id_infer_auto(0, &session.addr_type);
    assert(rc == BLE_HS_OK);

    ble_addr_t addr {};    // Sets type to BLE_ADDR_PUBLIC

    // Acquire our own address just as a sanity check
    rc = ble_hs_id_copy_addr(session.addr_type, addr.val, NULL);
    assert(rc == BLE_HS_OK);

    parse_addr(CONFIG_BUDDY_MAC, &addr);

    log_addr(addr.val);

    connect(&addr);    
}

static void ble_on_reset(int reason)
{
    ESP_LOGE(TAG, "Resetting state; reason=%d", reason);
}



void init_ble()
{
    ESP_ERROR_CHECK(nimble_port_init());

#if ENABLE_DELAYED_DISC
    ESP_LOGI(TAG, "approach: DELAY");
    timer_handle = xTimerCreate("deferred discovery",
        pdMS_TO_TICKS(500),
        false,
        &session,
        start_discovery);
#elif ENABLE_MTU_DISC
    ESP_LOGI(TAG, "approach: MTU");
#elif ENABLE_ESTAB
    ESP_LOGI(TAG, "approach: ESTAB");
#else
#error
#endif

    /* Initialize the NimBLE host configuration */
    ble_hs_cfg.sync_cb = ble_on_sync;
    ble_hs_cfg.reset_cb = ble_on_reset;

    //init_ble_security();

    ble_svc_gap_init();
    ble_svc_gatt_init();
    //ble_svc_dis_init();

    int rc = ble_svc_gap_device_name_set(device_name);
    assert(rc == BLE_HS_OK);

    /* Start the task */
    nimble_port_freertos_init(ble_host_task);
}