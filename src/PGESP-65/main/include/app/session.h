#pragma once

#include <cstdint>

struct Session
{
    uint8_t addr_type;
    uint16_t conn_handle;
};