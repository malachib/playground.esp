#pragma once

#include <nimble/ble.h>

bool parse_addr(const char* in, ble_addr_t* out);
void log_addr(const uint8_t addr_val[]);
void init_ble();
