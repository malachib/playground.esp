# Overview

# 1. Detail

# 2. Infrastructure

# 3. Observations

## 3.1. ASM output

[1] recommendations work as advertised

## 3.2. General Optimization Setting

`CONFIG_COMPILER_OPTIMIZATION` is interesting

## 3.3. Cycle performance

Exact cycle counts are elusive in datasheets [4] [4.1]

## 3.3.1. ChatGPT

ChatGPT indicates hardware multiply is 1-3 cycles (fast!) while integer divides are 30-100 cycles
FMUL (double precision) is still quick at 3-5 cycles
FDIV.S is 10-~20 cycles

## 3.3.2. LX6

## 3.3.3. LX7

# 4. Conclusions

## 4.1. Quick Reference Speed Chart

ESP32 is presumed LX6 w/ FPU
ESP32S3 is presumed LX7 w/ FPU

| MPU       | Operation | Cycles    | Notes
| -         | -         | -         | -
| ESP32     | MUL       | 1-3       | ChatGPT sourced
| ESP32     | DIV       | 30-100    | ChatGPT sourced
| ESP32     | FMUL.S    | 3-5       | ChatGPT sourced
| ESP32     | FMUL.D    | 5-7       | ChatGPT sourced
| ESP32     | FDIV.S    | 10-20     | ChatGPT sourced
| ESP32     | FDIV.D    | 15-25     | ChatGPT sourced
| ESP32S3   | MUL       | 1         | ChatGPT sourced
| ESP32S3   | DIV       | 30-100    | ChatGPT sourced
| ESP32S3   | FMUL.S    | 1-3       | ChatGPT sourced
| ESP32S3   | FMUL.D    | 3-5       | ChatGPT sourced
| ESP32S3   | FDIV.S    | 10-20     | ChatGPT sourced
| ESP32S3   | FDIV.D    | 15-25     | ChatGPT sourced

# References

1. https://github.com/espressif/esp-idf/issues/13226
2. https://esp32.com/viewtopic.php?t=40615
3. https://esp32.com/viewtopic.php?t=800
4. https://www.cadence.com/content/dam/cadence-www/global/en_US/documents/tools/silicon-solutions/compute-ip/isa-summary.pdf
    1. https://www.cadence.com/en_US/home/resources/product-briefs/xtensa-lx7-processor-pb.html