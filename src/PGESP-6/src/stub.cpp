extern "C" {
#ifdef ESP_PLATFORM
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#else
#include <FreeRTOS.h>
#include <task.h>
#include <esp/uart.h>
#endif
}


extern "C" void got_ip_event()
{
    
}

extern "C" void testTask(void*)
{
    for(;;);
}

extern "C" void test_task(void*)
{
    for(;;) 
    {
        taskYIELD();
    }
}