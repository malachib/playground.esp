# 1. Scope

Guidance from [1]

# 2. Infrastructure

## 2.1. Test Spreadsheet

ID `13MturGnswXGznKRbVqGerMxETRv8pLg_V-sJ_yY870M` as gleaned by [5]
Spreadsheet ID guidance also given in canonical docs [6]

## 2.2. OAuth2

From what AI tells me & I can tell, we start with the GCP provided Service Account JSON (sensitive data)
and need to extract `private_key` and `client_email`

### 2.2.1. JWT

Great guidance on an old forum topic [7] indicating mbedTLS calls and example code [7.1]

### 2.2.2. GCP Flow

Very good guidance also [3.2]

# 3. Observations

## 3.1. GCP / OAuth2

Seems there may be something other than a Service Account, a path called "Limited Input Device" [3.1.1]
For the time being though sticking to Service Account [3.2]

It appears we're doing a 2LO

## 3.2. Failed logins

Not obvious how to configure GCP to view/log auth attempts [10] [10.1] [10.2]

## 3.3. esp_http_client

Some helpful cert bundling for HTTPS [9.2]

## 3.4. QEMU Eth

Use `esp_eth` component [8.1]
Configure via OpenCores Ethernet [8.3] [8.4]


# 4. Troubleshooting

## 4.1. Seeed ESP326 Xiao

Confusingly, we are unable to bring things up.  First, we see:

```
D (1204) temperature_sensor: range changed, change to index 2
W (1274) wifi:ACK_TAB0   :0x   90a0b, QAM16:0x9 (24Mbps), QPSK:0xa (12Mbps), BPSK:0xb (6Mbps)
W (1274) wifi:CTS_TAB0   :0x   90a0b, QAM16:0x9 (24Mbps), QPSK:0xa (12Mbps), BPSK:0xb (6Mbps)
W (1274) wifi:(agc)0x600a7128:0xd20079f8, min.avgNF:0xce->0xd2(dB), RCalCount:0x7, min.RRssi:0x9f8(-96.50)
W (1284) wifi:(TB)WDEV_PWR_TB_MCS0:19
W (1284) wifi:(TB)WDEV_PWR_TB_MCS1:19
W (1294) wifi:(TB)WDEV_PWR_TB_MCS2:19
W (1294) wifi:(TB)WDEV_PWR_TB_MCS3:19
W (1294) wifi:(TB)WDEV_PWR_TB_MCS4:19
W (1304) wifi:(TB)WDEV_PWR_TB_MCS5:19
W (1304) wifi:(TB)WDEV_PWR_TB_MCS6:18
W (1304) wifi:(TB)WDEV_PWR_TB_MCS7:18
W (1314) wifi:(TB)WDEV_PWR_TB_MCS8:17
W (1314) wifi:(TB)WDEV_PWR_TB_MCS9:15
W (1314) wifi:(TB)WDEV_PWR_TB_MCS10:15
W (1324) wifi:(TB)WDEV_PWR_TB_MCS11:15
I (1324) wifi:11ax coex: WDEVAX_PTI0(0x000a00a0), WDEVAX_PTI1(0x000052bf).
```

Which the internet so far seems content to ignore.  Then, we see it struggle to authorize to `b0:be:76:a6:05:b3` which I am 95% sure is my SSID

It almost feels like Antenna is off, so I might try brute forcing IO3 and IO14 low as per
https://wiki.seeedstudio.com/xiao_esp32c6_getting_started/ even though that has never been needed before

## 4.2. QEMU

### 4.2.1. WiFi crash

Apparently WiFi is not supported in Espressif-provided QEMU [8].  See PGESP-77

### 4.2.2. OpenETH crash

```
assert failed: tcpip_inpkt /IDF/components/lwip/lwip/src/api/tcpip.c:257 (Invalid mbox)
```

Incredibly, a call to netif init is needed [8.3.1]

### 4.2.3. OpenETH DNS

Looks like DNS lookups are now failing

## 4.3. unsupported_grant_type

Pretty mysterious.  Some clues perhaps at [4.1] [4.2]
Turns out to be formatting the google request with its url-style-inside-body thingy

# Terminology

| Term          | Context   | Description
| -             | -         | -
| 2LO           | [3.3]     | two-legged OAuth
| 3LO           | [3.3]     | three-legged OAuth
| JWT claim set |           | JWT payload JSON
| JWS           | [11]      | JSON Web Signature

# References

1. https://github.com/espressif/esp-idf/tree/v5.4/examples/protocols/esp_http_client
2. https://stackoverflow.com/questions/48918069/how-to-use-google-sheets-rest-api-directly-to-create-a-sheet
    1. https://developers.google.com/oauthplayground/
3. https://console.cloud.google.com/welcome?project=malachi-bs-site
    1. https://developers.google.com/identity/protocols/oauth2
        1. https://developers.google.com/identity/protocols/oauth2/limited-input-device#creatingcred
    2. https://developers.google.com/identity/protocols/oauth2/service-account
    3. https://developers.google.com/sheets/api/scopes
4. https://stackoverflow.com/questions/67382392/update-a-google-sheet-using-rest-calls-with-google-sheets-api
    1. https://www.outsystems.com/forums/discussion/43710/google-services-oauth2-error-unsupported-grant-type-and-invalid-grant-type/
    2. https://stackoverflow.com/questions/44480882/unsupported-grant-type-in-google-oauth
5. https://stackoverflow.com/questions/36061433/how-do-i-locate-a-google-spreadsheet-id
6. https://developers.google.com/sheets/api/guides/concepts
7. https://esp32.com/viewtopic.php?t=6828
    1. https://github.com/nkolban/esp32-snippets/blob/master/cloud/GCP/JWT
8. https://github.com/espressif/esp-idf/issues/15087
    1. https://github.com/espressif/esp-idf/commit/31dac92e5f0daac98190fd603df213a0a25a3807
    2. https://docs.espressif.com/projects/esp-idf/en/v5.4/esp32/api-guides/tools/qemu.html
    3. https://docs.espressif.com/projects/esp-idf/en/v4.3/esp32/api-reference/kconfig.html#config-eth-use-openeth
        1. https://esp32.com/viewtopic.php?t=35089
    4. https://esp32.com/viewtopic.php?t=28962
9. ESP-IDF reserved
    1. https://docs.espressif.com/projects/esp-idf/en/v5.4/esp32s3/api-reference/protocols/esp_http_client.html
    2. https://docs.espressif.com/projects/esp-idf/en/v5.4/esp32/api-reference/protocols/esp_crt_bundle.html
10. https://cloud.google.com/blog/products/identity-security/logs-based-security-alerting-in-google-cloud
    1. https://stackoverflow.com/questions/64919257/how-to-log-failed-login-attempts-in-google-cloud-identity-platform
    2. https://stackoverflow.com/questions/77586390/how-to-retrieve-audit-logs-for-user-sign-ins-in-google-cloud
11. https://datatracker.ietf.org/doc/html/rfc7515