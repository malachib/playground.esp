// Guidance from
// https://github.com/espressif/esp-idf/blob/v5.4/examples/common_components/protocol_examples_common/eth_connect.c

#include <esp_eth.h>
#include <esp_log.h>
#include <esp_mac.h>
#include <esp_netif.h>

#if CONFIG_ETH_USE_OPENETH

static const char* TAG = "PGESP-75::poc1::eth";

static esp_eth_handle_t s_eth_handle = NULL;
static esp_eth_mac_t *s_mac = NULL;
static esp_eth_phy_t *s_phy = NULL;
static esp_eth_netif_glue_handle_t s_eth_glue = NULL;

esp_netif_t* init_openeth()
{
    ESP_LOGI(TAG, "init_openeth: entry");

    ESP_ERROR_CHECK(esp_netif_init());

	esp_netif_inherent_config_t esp_netif_config = ESP_NETIF_INHERENT_DEFAULT_ETH();
	esp_netif_config.if_desc = "openeth0";
    esp_netif_config.route_prio = 64;
    esp_netif_config_t netif_config = {
        .base = &esp_netif_config,
        .stack = ESP_NETIF_NETSTACK_DEFAULT_ETH
    };
    esp_netif_t* netif = esp_netif_new(&netif_config);
    assert(netif);

	eth_mac_config_t mac_config = ETH_MAC_DEFAULT_CONFIG();
    mac_config.rx_task_stack_size = 4096;
    eth_phy_config_t phy_config = ETH_PHY_DEFAULT_CONFIG();
    
	phy_config.autonego_timeout_ms = 100;
    s_mac = esp_eth_mac_new_openeth(&mac_config);
    s_phy = esp_eth_phy_new_dp83848(&phy_config);

    esp_eth_config_t config = ETH_DEFAULT_CONFIG(s_mac, s_phy);
    ESP_ERROR_CHECK(esp_eth_driver_install(&config, &s_eth_handle));

    // 06MAR25 MB - Not sure if this MAC portion really is needed
    uint8_t eth_mac[6] {};
    ESP_ERROR_CHECK(esp_read_mac(eth_mac, ESP_MAC_ETH));
    ESP_ERROR_CHECK(esp_eth_ioctl(s_eth_handle, ETH_CMD_S_MAC_ADDR, eth_mac));

    // combine driver with netif
    s_eth_glue = esp_eth_new_netif_glue(s_eth_handle);
    ESP_ERROR_CHECK(esp_netif_attach(netif, s_eth_glue));

    ESP_ERROR_CHECK(esp_eth_start(s_eth_handle));
    return netif;
}
#endif
