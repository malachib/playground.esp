#include <cstring>
#include <string_view>

#include <esp_crt_bundle.h>
#include <esp_log.h>

#include <mbedtls/pk.h>
#include <mbedtls/error.h>
#include <mbedtls/entropy.h>
#include <mbedtls/ctr_drbg.h>

#include "jwt.hpp"

static const char* TAG = "PGESP-75::poc1::jwt";

static char* mbedtlsError(int errnum) {
    static char buffer[200];
    mbedtls_strerror(errnum, buffer, sizeof(buffer));
    return buffer;
}

void* sign_jwt_style(const uint8_t* input, unsigned sz,
    const uint8_t* key, unsigned key_sz,
    uint8_t* out, size_t* out_sz)
{
    ESP_LOGD(TAG, "sign_jwt_style: entry: input=%p sz=%u key=%p key_sz=%u",
        input, sz, key, key_sz);

    uint8_t digest[32];
    int rc = mbedtls_md(mbedtls_md_info_from_type(MBEDTLS_MD_SHA256), input, sz, digest);
    if (rc != 0) {
        printf("Failed to mbedtls_md: %d (-0x%x): %s\n", rc, -rc, mbedtlsError(rc));
        return nullptr;
    }

    ESP_LOGV(TAG, "sign_jwt_style: init");

    mbedtls_pk_context pk_context;
    mbedtls_pk_init(&pk_context);
    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_ctr_drbg_init(&ctr_drbg);
    mbedtls_entropy_context entropy;
    mbedtls_entropy_init(&entropy);

    const char* pers="MyEntropy";
                
    mbedtls_ctr_drbg_seed(
        &ctr_drbg,
        mbedtls_entropy_func,
        &entropy,
        (const unsigned char*)pers,
        strlen(pers));
 
    auto key_copied = (uint8_t*) heap_caps_malloc(key_sz + 1, MALLOC_CAP_SPIRAM);

    assert(key_copied);

    // https://forums.mbed.com/t/parsing-a-private-key-only-throws-an-error-if-trying-to-decrypt-but-not-by-itself/4299/3
    // Looks like null termination is a requirement
    memcpy(key_copied, key, key_sz);
    key_copied[key_sz] = 0;

    for (char* result = (char*)key_copied; (result = std::strstr(result, "\\n")); ++result)
    {
        // Kludgey but it works
        result[0] = 10;
        result[1] = 10;
    }
    
    // Expecting PEM, which GCP JSON seems to contain
    // Has f_rng which example didn't have
    // https://arm-software.github.io/CMSIS-mbedTLS/latest/pk_8h.html#aad02107b63f2a47020e6e1ef328e4393
    rc = mbedtls_pk_parse_key(&pk_context, key_copied, key_sz + 1, NULL, 0, mbedtls_ctr_drbg_random, &ctr_drbg);
    free(key_copied);

    if (rc != 0) {
        printf("Failed to mbedtls_pk_parse_key: %d (-0x%x): %s\n", rc, -rc, mbedtlsError(rc));
        return nullptr;     // DEBT: free is implied as just a formality, but this still feels sloppy
    }

    ESP_LOGD(TAG, "sign_jwt_style: sign");

    rc = mbedtls_pk_sign(&pk_context, MBEDTLS_MD_SHA256, digest, sizeof(digest), out, *out_sz, out_sz, mbedtls_ctr_drbg_random, &ctr_drbg);
    if (rc != 0) {
        printf("Failed to mbedtls_pk_sign: %d (-0x%x): %s\n", rc, -rc, mbedtlsError(rc));
        return nullptr;     // DEBT: free is implied as just a formality, but this still feels sloppy
    }

    mbedtls_pk_free(&pk_context);

    return out;
}
