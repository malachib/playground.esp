#pragma once

#include <embr/json/encoder.hpp>
#include <jsmn.h>

// Be mindful may need to use JSMN_HEADER later down the line

template <unsigned N, class F>
void decode_json(const char* s, unsigned len, F&& f)
{
    jsmn_parser p;

    // NOTE: Not loving pre allocating a ton of tokens here.  Seems jsmn
    // will probably demand less though if I feed it smaller JSON data
    jsmntok_t tokens[N];

    jsmn_init(&p);
    int r = jsmn_parse(&p, s, len, tokens, N);

    //ESP_LOGI(TAG, "decode_json: %p %u %d", s, len, r);

    // DEBT: Probably not going to work so well for complex obects
    for(int i = 1; i < r; i++)
    {
        const jsmntok_t& key = tokens[i];
        // DEBT: Do extra bounds checking, just incase
        const jsmntok_t& value = tokens[++i];

        std::string_view key_s(s + key.start, key.end - key.start);
        std::string_view value_s(s + value.start, value.end - value.start);

        //ESP_LOGV(TAG, "decode_json: key=%.*s", key_s.size(), key_s.data());

        f(key_s, value_s);
    }
}

