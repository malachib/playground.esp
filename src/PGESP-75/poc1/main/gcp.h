#pragma once

#include <string_view>

struct gcp_info
{
    std::string_view
        project_id,
        private_key,
        client_email,
        token_uri,
        client_x509_cert_url;
};


void decode_gcp_json(const char* s, unsigned len, gcp_info* info);
void do_gcp_auth(const char* jwt);
