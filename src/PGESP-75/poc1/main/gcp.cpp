#include <esp_crt_bundle.h>
#include <esp_log.h>

#include <esp_http_client.h>

#include <estd/sstream.h>

#include "gcp.h"
#include "json.h"

const char* TAG = "PGESP-75::poc1::gcp";

void decode_gcp_json(const char* s, unsigned len, gcp_info* info)
{
    decode_json<40>(s, len, [&](std::string_view key, std::string_view value)
    {
        if(key == "project_id")
        {
            ESP_LOGD(TAG, "decode_gcp_json: project_id=%.*s", value.size(), value.data());

            info->project_id = value;
        }
        else if(key == "private_key")
        {
            //ESP_LOGI(TAG, "decode_gcp_json: private_key=%.*s", value.size(), value.data());

            info->private_key = value;
        }
        else if(key == "token_uri")
        {
            info->token_uri = value;
        }
        else if(key == "client_email")
        {
            info->client_email = value;
        }
        else if(key == "client_x509_cert_url")
        {
            info->client_x509_cert_url = value;
        }
    });
}

struct http_context
{
    estd::layer1::stringbuf<1024> received;
};

static esp_err_t http_event_handler(esp_http_client_event_t* evt)
{
    auto context = (http_context*) evt->user_data;

    ESP_LOGV(TAG, "http_event_handler: event_id=%d", evt->event_id);

    switch(evt->event_id)
    {
        case HTTP_EVENT_ERROR:
            break;

        case HTTP_EVENT_ON_CONNECTED:
            break;

        case HTTP_EVENT_ON_HEADER:
            ESP_LOGI(TAG, "http_event_handler: header %s=%s", evt->header_key, evt->header_value);
            break;
        
        case HTTP_EVENT_ON_DATA:
            //ESP_LOGI(TAG, "http_event_handler: len=%d", evt->data);
            //ESP_LOG_BUFFER_CHAR(TAG, (const char*)evt->data, evt->data_len);
            write(STDOUT_FILENO, evt->data, evt->data_len);

            // FIX: Buffer overrun, plus we don't want the ENTIRE thing anyway probably
            //context->received.sputn((char*)evt->data, evt->data_len);
            break;

        case HTTP_EVENT_ON_FINISH:
            break;

        default:    break;
    }

    return ESP_OK;
}

void do_gcp_auth(const char* grant_request_body)
{
    ESP_LOGI(TAG, "do_rest_auth: entry");

    esp_http_client_config_t config {};
    static http_context context;

    // TODO: Separate bearer acquisition vs Google sheets API calls
    //config.url = "https://oauth2.googleapis.com/token";
    // [4.2] experimentation
    config.url = "https://www.googleapis.com/oauth2/v4/token";
    config.event_handler = http_event_handler;
    config.user_data = &context;
    config.disable_auto_redirect = true;
    config.crt_bundle_attach = esp_crt_bundle_attach;
    // DEBT: Hate blocking, but will do it until things get online more
    config.is_async = false;

    esp_http_client_handle_t client = esp_http_client_init(&config);

    esp_http_client_set_method(client, HTTP_METHOD_POST);
    esp_http_client_set_header(client, "Content-Type", "application/x-www-form-urlencoded");
    // "this function will not copy the data" - cool!
    esp_http_client_set_post_field(client, grant_request_body, strlen(grant_request_body));
    
    // GET
    esp_err_t ret = esp_http_client_perform(client);

    esp_http_client_cleanup(client);
}


