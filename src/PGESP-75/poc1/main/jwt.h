#pragma once

void* sign_jwt_style(const uint8_t* input, unsigned sz,
    const uint8_t* key, unsigned key_sz,
    uint8_t* out, size_t* out_sz);