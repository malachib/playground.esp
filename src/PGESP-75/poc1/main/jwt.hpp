#pragma once

#include <estd/streambuf.h>

#include <embr/json/encoder.hpp>
#include <embr/text/base64.h>

#include "gcp.h"
#include "jwt.h"

// "The only signing algorithm supported by the Google OAuth 2.0 Authorization Server
//  is RSA using SHA-256 hashing algorithm. This is expressed as RS256 in the alg field
//  in the JWT header." [3.2]
template <ESTD_CPP_CONCEPT(estd::concepts::v1::OutStreambuf) Out>
void emit_jwt_header(Out& output)
{
    embr::text::out_base64_streambuf<Out&> out(output);

    constexpr const char header[] = R"({"alg":"RS256","type":"JWT"})";

    out.sputn(header, sizeof(header) - 1);
    out.finalize(false);
}

template <ESTD_CPP_CONCEPT(estd::concepts::v1::OutStreambuf) Out>
void create_jwt(unsigned iat, unsigned exp, const char* aud, Out& output)
{
    // TODO: Differenciate between Base64 and Base64url [3.2]
    estd::detail::basic_ostream<embr::text::out_base64_streambuf<Out&>> out(output);

    emit_jwt_header(output);

    output.sputc('.');

    embr::json::v1::make_fluent(out).
    begin()
        ("iat", iat)
        ("exp", exp)
        ("aud", aud)
    ();
    out.rdbuf()->finalize(false);
}

// Sheets scopes may be interesting from [3.3]
template <ESTD_CPP_CONCEPT(estd::concepts::v1::OutStreambuf) Out>
void create_jwt(unsigned iat, unsigned exp, const gcp_info& info, const char* scope, Out& output)
{
    estd::detail::basic_ostream<embr::text::out_base64_streambuf<Out&>> out(output);

    emit_jwt_header(output);

    output.sputc('.');

    embr::json::v1::make_fluent(out).
    begin()
        ("iss", info.client_email)
        ("scope", scope)
        ("iat", iat)
        ("exp", exp)
        
        // [4.2] experimentation
        ("aud", "https://www.googleapis.com/oauth2/v4/token")

        //("aud", info.token_uri)       // Seems to conform to [3.2]
    ();
    out.rdbuf()->finalize(false);
}

template <ESTD_CPP_CONCEPT(estd::concepts::v1::OutStreambuf) Out>
void append_signature(const uint8_t* sig, size_t sig_sz, Out& output)
{
    //ESP_LOGD(TAG, "append_signature: sig=%p sz=%u", sig, sig_sz);
    embr::text::out_base64_streambuf<Out&> out(output);

    output.sputc('.');
    out.sputn((const char*)sig, sig_sz);
    out.finalize(false);
}


template <ESTD_CPP_CONCEPT(estd::concepts::v1::OutStreambuf) Out>
void create_signed_jwt(
    unsigned iat, unsigned exp, const gcp_info& info, const char* scope,
    const uint8_t* key, size_t key_sz, Out& output)
{

}
