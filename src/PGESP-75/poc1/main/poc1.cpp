#include <esp_crt_bundle.h>
#include <esp_eth.h>
#include <esp_log.h>
#include <esp_netif_sntp.h>
#include <esp_wifi.h>

#include <nvs_flash.h>

#include <cstring>
#include <string_view>

#include <mbedtls/pk.h>
#include <mbedtls/error.h>
#include <mbedtls/entropy.h>
#include <mbedtls/ctr_drbg.h>

#include <estd/sstream.h>

#include <embr/json/encoder.hpp>

#include <esp_http_client.h>

#include "jwt.hpp"
#include "gcp.h"

static const char* TAG = "PGESP-75::poc1";

// https://developers.google.com/sheets/api/guides/concepts
// https://stackoverflow.com/questions/48918069/how-to-use-google-sheets-rest-api-directly-to-create-a-sheet

void init_wifi();
esp_netif_t* init_openeth();

// DEBT: Not obvious that this is picked up by do_gcp_auth
static const char* grant_request_body;

static void event_handler(void *arg, esp_event_base_t event_base,
    int32_t event_id, void *event_data)
{
    // Seeing WIFI_EVENT_HOME_CHANNEL_CHANGE at beginning, interesting
    ESP_LOGI(TAG, "event_handler");

    if(event_base == WIFI_EVENT)
    {
        switch(event_id)
        {
            case WIFI_EVENT_STA_START:
                esp_wifi_connect();
                break;

            default:
                break;
        }
    }
    else if (event_base == IP_EVENT)
    {
        switch(event_id)
        {
            case IP_EVENT_ETH_GOT_IP:
            {
                esp_netif_dns_info_t dnsinfo;
                esp_netif_get_dns_info(nullptr, ESP_NETIF_DNS_MAIN, &dnsinfo);
                [[fallthrough]];
            }

            case IP_EVENT_STA_GOT_IP:
                do_gcp_auth(grant_request_body);
                break;

            default:
                break;
        }
    }
}

extern const uint8_t secret_json_start[] asm("_binary_secret_json_start");
extern const uint8_t secret_json_end[]   asm("_binary_secret_json_end");

extern "C" void app_main(void)
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    ESP_ERROR_CHECK(esp_event_loop_create_default());

#if CONFIG_PGESP_WIFI_ON || CONFIG_ETH_USE_OPENETH
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(ETH_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL));

    esp_sntp_config_t config = ESP_NETIF_SNTP_DEFAULT_CONFIG("pool.ntp.org");
    esp_netif_sntp_init(&config);
#endif

    // DEBT: SPIRAM for these too
    static estd::layer1::stringbuf<2048> out;
    estd::detail::basic_ostream<decltype(out)&> ostream(out);
    static uint8_t _sig[1024];
    size_t sig_sz = sizeof(_sig);
    const char* raw = out.str().c_str();

    gcp_info gcpinfo {};

    ostream << "grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion=";
    ostream.flush();

    // DEBT: Depending on null-terminated string and some kind of tellp would be way better here
    const char* jwt = raw + strlen(raw);
    //jwt = out.pptr();

    grant_request_body = raw;

    decode_gcp_json((const char*) secret_json_start, secret_json_end - secret_json_start, &gcpinfo);

    // DEBT: embr::json doesn't take a std::string_view (I think), so temporarily copy him out
    char project_id[64] {};
    memcpy(project_id, gcpinfo.project_id.data(), gcpinfo.project_id.size());

    time_t now;
    time(&now);
    uint32_t iat = now;              // Set the time now.
    uint32_t exp = iat + 60*60;      // Set the expiry time.

    //create_jwt(iat, exp, project_id, out);
    // TODO: Need json encoder capable of std::string_view
    create_jwt(iat, exp, gcpinfo, "https://www.googleapis.com/auth/spreadsheets.readonly", out);

    printf("encoded=");
    puts(out.str().c_str());

    unsigned jwt_sz = strlen(jwt);

    const unsigned key_sz = gcpinfo.private_key.size() + 1;
    auto key_copied = (uint8_t*) heap_caps_malloc(key_sz, MALLOC_CAP_SPIRAM);

    assert(key_copied);

    // https://forums.mbed.com/t/parsing-a-private-key-only-throws-an-error-if-trying-to-decrypt-but-not-by-itself/4299/3
    // Looks like null termination is a requirement
    memcpy(key_copied, gcpinfo.private_key.data(), key_sz - 1);
    key_copied[key_sz] = 0;

    for (char* result = (char*)key_copied; (result = std::strstr(result, "\\n")); ++result)
    {
        // Kludgey but it works
        result[0] = 10;
        result[1] = 10;
    }

    uint8_t* sig = _sig;
    // NOTE: Almost works but remember we have the base64 encode phase too
    // Consider re-using key_copied.  In-place encoding won't work since base64 grows
    // quicker than the data it's replacing
    //uint8_t* sig = (uint8_t*)jwt + jwt_sz;
    //sig_sz = 2048 - jwt_sz;
    
    // Looks like signing post-encoded is the way to go.  Our buddy [7.1] signs post-base64-encoded too
    sign_jwt_style((uint8_t*)jwt, jwt_sz,
        key_copied, key_sz,
        sig, &sig_sz);

    // TODO: reuse key_copied buf for sig, since it's temporary too
    append_signature(sig, sig_sz, out);

    free(key_copied);

    printf("encoded_and_signed=");
    puts(out.str().c_str());

#if CONFIG_ETH_USE_OPENETH
	init_openeth();
#elif CONFIG_PGESP_WIFI_ON
    init_wifi();
#endif
}
