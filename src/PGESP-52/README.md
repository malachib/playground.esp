# Overview

# 1. Design Goals

To answer lingering questions of Bluedroid BLE stack:

## 1.1. Do we really need to memcpy handles?

## 1.2. What's the best way to update a characteristic value?

Contextually this applies to both notification as well as general background updates

# 2. Infrastructure

## 2.1. ESP-IDF

### 2.1.1. v5.1.3

### 2.1.2. v5.2.1

Paths:

1. `components/bt/host/bluedroid/`
	1. `components/bt/host/bluedroid/api`
	2. `components/bt/host/bluedroid/btc`
		1. `components/bt/host/bluedroid/btc/profile`
	3. `components/bt/host/bluedroid/bte`
	4. `components/bt/host/bluedroid/stack`
2. `components/bt/common/btc`


# 3. Opinions / Observations

## 3.1. esp_ble_gatts_set_attr_value

This feels like a very clumsy way to update attribute values.

Even fatter than I had feared.  A bunch of memcpy, osi_malloc and osi_free.
No magic signaling to indicate updated attribute, just the already-known ESP_GATTS_SET_ATTR_VAL_EVT
gatts_set_attribute_value (5.8.) appears to copy to internally-managed memory holding on to attr values.
That would explain how to initial config table pointers don't seem to be heeded.  But, if that's the case,
why weren't they const pointers?

## 3.2. Bluedroid

It's becoming evident there's no abstraction layer between Bluedroid and NimBLE.
It's also appearing the Bluedroid has a TON of layers, and if 3.1. is any indication, full of memcpys
and allocations.  Probably they are all relatively small, so I'd give a 6 out of 10 on the "yuck" scale.
Fatter than I'd like, tempered by clearly a well thought out and powerful infrastructure.

# 4. Troubleshooting

# 5. Conclusions & Findings

## 5.1. esp_ble_gatts_set_attr_value

A memory copy always happens, and it signals
the GATTS listener.  Furthermore, the memory copy *does not* update what originally appears in the config table.

Appears to be Bluedroid specific

Does a thunk via 5.2. resulting ultimately in BTA_SetAttributeValue (5.4.)

## 5.2. btc_transfer_context

"transfer an message to another module in the different task."

Not Bluedroid specific (located in 2.1.2. path #2)

## 5.3. btc_gatts_call_handler

Picks up thunk from 5.2. and is accompanied by a deep copy/deep free phase.

## 5.4. BTA_SetAttributeValue

This guy emits `BTA_GATTS_API_SET_ATTR_VAL_EVT` by way of `bta_sys_sendmsg`, finally resulting in `bta_gatts_set_attr_value` (5.6.)

## 5.5. bta_sys_sendmsg

## 5.6. bta_gatts_set_attr_value

"This function is used to set the attribute value."

Calls `GATTS_SetAttributeValue`

Operates on `tBTA_GATTS_RCB` and after the above call, does a callback:

```
(*p_rcb->p_cback)(BTA_GATTS_SET_ATTR_VAL_EVT, &cb_data);
```

Under 2.1.2. path #1.3.

## 5.7. GATTS_SetAttributeValue

"This function sends to set the attribute value"

Calls `gatts_set_attribute_value`

## 5.8. gatts_set_attribute_value

"This function add the attribute value in the database"

Rolls through a linked list of `tGATT_ATTR16` matching on handle, and doing memcpy per into each.
Has no other function, so nothing resembling side effects.

Prototype lives in `gatt_int.h` implying internal-only call.

## 5.9. btc_gatts_cb_handler

(Inferring) Sometimes picks up callbacks from 5.6.

Location in path 2.1.2. #1.2.1.

Only place in code which addresses BTA_GATTS_SET_ATTR_VAL_EVT

Constructs esp_ble_gatts_cb_param_t.  Emits ESP_GATTS_SET_ATTR_VAL_EVT by way of `btc_gatts_cb_to_app`

## 5.10. btc_gatts_cb_to_app

Local-only helper which retrieves fn ptr by way of btc_profile_cb_get(BTC_PID_GATTS)

# References

