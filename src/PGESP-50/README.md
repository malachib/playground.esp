# Overview

Document v0.1

# 1. Goals

This PGESP specifically explores the ESP-IDF v5.1.3 implementation of BLE mesh [1].

# 2. Infrastructure

## 2.1. ESP32

For Espressif's example bringups, required are "Three ESP32 boards" [1.1] [1.7]

## 2.2. 

# 3. Opinions / Observations

## 3.1. Mesh Architecture

There are a whole bunch of different architectures at play. 

At the top, we have Zephyr Bluetooth Mesh stack.

Zephyr stack layers on top of Bluedroid, as per [1.2. Section 2, Figure 2.1]

It is said this stack is portable [1.2. Section 2.2].  This implies the entire Zephyr stack can run on NimBLE without gutting / merging Zephyr itself.

On NimBLE: "Most features of NimBLE including BLE Mesh are supported" [1.5]

## 3.2. Beacons

Looks like BLE mesh has baked into it Beacon support as per [1.2. Section 1.3.]
Beware of imposters, like WiFi mesh beacons [1.3] or non-mesh BLE beacons [1.4]

Depth of beacon support is unclear [2]

## 3.3. Existing Examples

A very robust existing example exists [1.1] so we'll need to decide what, if any, value add doing our own has.

## 3.3.1. Console Mode

It's mentioned demo code has a console config [3], could be interesting

## 3.4. Roaming nodes

Canonically considered self-healing [4], the possibility for roaming nodes is strong.  It would come down
to how gracefully provisioning works in this context.

## 3.5. BLE Architecture

~~It seems that perhaps NimBLE and Bluedroid are abstracted under one unified API [1.6].  That would be quite cool.~~

Nope.  And as nearly as I can tell ESP-BLE-MESH may in fact do its own thing not using either of those directly.  Maybe.

## 3.6. Compliance

"ESP-BLE-MESH is implemented and certified based on the latest Mesh Profile v1.0.1" [1.1] [4.1]

## 3.7. Mesh States

A neat concept, a kind of higher order notification scheme formalized as to what variety of notification you're getting.

"For example, consider a light controlled by a dimmer switch. The light would possess the two states, Generic OnOff and Generic Level with each bound to the other. Reducing the brightness of the light until Generic Level has a value of zero (fully dimmed) results in Generic OnOff transitioning from On to Off." [4.2]

## 3.8. Models

### 3.8.1. Generics

# Terminology

* Elements = independently controlled parts of a node [4.2]
* IV = ?
* Node = "devices which are part of a mesh network" [4.2]
* SIG = Special Interest Group
* SIG model = aggregates properties, states, messages.  Kind of a higher level service
* Roaming node (my term) = Node which may come and go and without ceremony participate in the relay network
* Mesh Profile v1.0.1 = BLE standard [4.2]
* Scenes = stored collection of states [4.2]
* States = condition of and contained within an Element, can be formalized
* State Bindings = relationships between states
* State Transitions = may be instant or happen over time
* Unprovisioned Devices = Nodes not yet part of network [4.2]
* Virtual Address = 128-bit UUID "assigned to one or more elements" [4.2]

# References

1. https://docs.espressif.com/projects/esp-idf/en/v5.1.3/esp32/api-reference/bluetooth/esp-ble-mesh.html
	1. https://docs.espressif.com/projects/esp-idf/en/v5.1.3/esp32/api-guides/esp-ble-mesh/ble-mesh-index.html
	2. https://docs.espressif.com/projects/esp-idf/en/v5.1.3/esp32/api-guides/esp-ble-mesh/ble-mesh-architecture.html
	3. https://docs.espressif.com/projects/esp-idf/en/v5.1.3/esp32/api-guides/esp-wifi-mesh.html
	4. https://esphome.io/components/esp32_ble_beacon.html
	5. https://docs.espressif.com/projects/esp-idf/en/v5.1.3/esp32/api-reference/bluetooth/nimble/index.html
	6. https://docs.espressif.com/projects/esp-idf/en/v5.1.3/esp32/api-reference/bluetooth/index.html
	7. https://github.com/espressif/esp-idf/tree/v5.1.3/examples/bluetooth/esp_ble_mesh/ble_mesh_node/onoff_server
2. https://github.com/espressif/esp-matter/issues/226
3. https://esp32.com/viewtopic.php?t=11509
4. https://www.bluetooth.com/learn-about-bluetooth/feature-enhancements/mesh/mesh-faq/
	1. https://launchstudio.bluetooth.com/ListingDetails/94304
	2. https://www.bluetooth.com/wp-content/uploads/2019/03/Mesh-Technology-Overview.pdf 
	3. https://www.bluetooth.com/bluetooth-resources/bluetooth-mesh-models/
		1. 1903_Mesh-Models-Overview_FINAL.pdf
