# QEMU WiFi crash

Apparently WiFi is not supported in Espressif-provided QEMU [1].  Their recommendation is to use wired ethernet setting.  Enabling this is slightly
fiddly [1.4] and warrants this PGESP
Confusingly there's an official-looking ESP Qemu [1.2], however the *actual*
official one is elsewhere [1.3]

# References

1. https://github.com/espressif/esp-idf/issues/15087
    1. https://www.esp32.com/viewtopic.php?t=43311
    2. https://github.com/emb-team/esp-idf-qemu
    3. https://github.com/espressif/qemu/
    4. https://esp32.com/viewtopic.php?t=28962