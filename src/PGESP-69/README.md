# Overview

# 1. Scope

# 2. Infrastructure

Tests performed with ESP-IDF v5.3.2 on debian-hp

# 3. Opinions & Observations

## 3.1. Experimentation

I tried a ton of different things with ChatGPT but it's kind of useless

## 3.2. Journal

### 27JAN25

Fielding question to ESP32 forum [1]

### 01FEB25

No word so far [1]

### 09FEB25

No word so far [1]

### 10FEB25

Cross posted to ESP-IDF repo proper [2]

### 11FEB25

Espressif responded [2] and indicated a workaround is coming

# References

1. https://esp32.com/viewtopic.php?f=13&t=44125
2. https://github.com/espressif/esp-idf/issues/15369