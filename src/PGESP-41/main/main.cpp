#include <esp_log.h>
#include <esp_adc/adc_continuous.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

static const char* TAG = "PGESP-41";

#define ADC_UNIT                    ADC_UNIT_1
#define _ADC_UNIT_STR(unit)         #unit
#define ADC_UNIT_STR(unit)          _ADC_UNIT_STR(unit)
#define ADC_CONV_MODE               ADC_CONV_SINGLE_UNIT_1
#define ADC_ATTEN                   ADC_ATTEN_DB_0
#define ADC_BIT_WIDTH               SOC_ADC_DIGI_MAX_BITWIDTH

#if CONFIG_IDF_TARGET_ESP32 || CONFIG_IDF_TARGET_ESP32S2
#define ADC_OUTPUT_TYPE             ADC_DIGI_OUTPUT_FORMAT_TYPE1
#define ADC_GET_CHANNEL(p_data)     ((p_data)->type1.channel)
#define ADC_GET_DATA(p_data)        ((p_data)->type1.data)
#else
#define ADC_OUTPUT_TYPE             ADC_DIGI_OUTPUT_FORMAT_TYPE2
#define ADC_GET_CHANNEL(p_data)     ((p_data)->type2.channel)
#define ADC_GET_DATA(p_data)        ((p_data)->type2.data)
#endif

// As per simple math
// https://www.wolframalpha.com/input?i2d=true&i=Divide%5B1000%2C20000%5D*1024
// that's 12.8ms per frame/callback per channel if 32-bit frames.
// that would be 6.4ms since currently we're doing 2 channels
#define READ_LEN                    1024

#if CONFIG_IDF_TARGET_ESP32
static adc_channel_t channel[2] = {ADC_CHANNEL_6, ADC_CHANNEL_7};
#else
static adc_channel_t channel[2] = {ADC_CHANNEL_2, ADC_CHANNEL_3};
#endif

volatile static uint32_t average = 0;
volatile static unsigned isr_counter = 0;
volatile static unsigned overflow_counter = 0;

static bool IRAM_ATTR s_conv_done_cb(adc_continuous_handle_t handle, const adc_continuous_evt_data_t *edata, void *user_data)
{
    const uint8_t* data = edata->conv_frame_buffer;

    BaseType_t mustYield = pdFALSE;
    //Notify that ADC continuous driver has done enough number of conversions
    //vTaskNotifyGiveFromISR(s_task_handle, &mustYield);

    ++isr_counter;
    uint32_t average = 0;

    for (int i = 0; i < edata->size; i += SOC_ADC_DIGI_RESULT_BYTES)
    {
        auto p = (const adc_digi_output_data_t*)&data[i];
        //uint32_t chan_num = ADC_GET_CHANNEL(p);
        uint32_t data = ADC_GET_DATA(p);

        average += data;

        /* Check the channel number validation, the data is invalid if the channel num exceed the maximum channel */
        /*
        if (chan_num < SOC_ADC_CHANNEL_NUM(ADC_UNIT)) {
            ESP_LOGI(TAG, "Unit: %s, Channel: %" PRIu32 ", Value: %" PRIx32, unit, chan_num, data);
        } else {
            ESP_LOGW(TAG, "Invalid data [%s_%" PRIu32 "_%" PRIx32 "]", unit, chan_num, data);
        }   */
    }

    ::average = average;

    return (mustYield == pdTRUE);
}

static bool IRAM_ATTR s_pool_ovf_cb(adc_continuous_handle_t handle, const adc_continuous_evt_data_t *edata, void *user_data)
{
    BaseType_t mustYield = pdFALSE;

    overflow_counter = overflow_counter + 1;

    return (mustYield == pdTRUE);
}


static void continuous_adc_init(adc_channel_t *channel, uint8_t channel_num, adc_continuous_handle_t *out_handle)
{
    adc_continuous_handle_t handle = NULL;

    adc_continuous_handle_cfg_t adc_config =
    {
        .max_store_buf_size = READ_LEN * 4,
        .conv_frame_size = READ_LEN,
    };
    ESP_ERROR_CHECK(adc_continuous_new_handle(&adc_config, &handle));

    adc_digi_pattern_config_t adc_pattern[SOC_ADC_PATT_LEN_MAX] {};

    for (int i = 0; i < channel_num; i++)
    {
        adc_pattern[i].atten = ADC_ATTEN;
        adc_pattern[i].channel = channel[i] & 0x7;
        adc_pattern[i].unit = ADC_UNIT;
        adc_pattern[i].bit_width = ADC_BIT_WIDTH;

        ESP_LOGI(TAG, "adc_pattern[%d].atten is :%" PRIx8, i, adc_pattern[i].atten);
        ESP_LOGI(TAG, "adc_pattern[%d].channel is :%" PRIx8, i, adc_pattern[i].channel);
        ESP_LOGI(TAG, "adc_pattern[%d].unit is :%" PRIx8, i, adc_pattern[i].unit);
    }

    adc_continuous_config_t dig_cfg
    {
        .pattern_num = channel_num,
        .sample_freq_hz = 20 * 1000,
        .conv_mode = ADC_CONV_MODE,
        .format = ADC_OUTPUT_TYPE,
    };

    dig_cfg.adc_pattern = adc_pattern;
    ESP_ERROR_CHECK(adc_continuous_config(handle, &dig_cfg));

    *out_handle = handle;
}

static adc_continuous_handle_t handle = NULL;

void init_adc()
{
    continuous_adc_init(channel, sizeof(channel) / sizeof(adc_channel_t), &handle);

    // TODO: Also put in overflow callback to check its behavior

    adc_continuous_evt_cbs_t cbs =
    {
        .on_conv_done = s_conv_done_cb,
        .on_pool_ovf = s_pool_ovf_cb
    };

    ESP_ERROR_CHECK(adc_continuous_register_event_callbacks(handle, &cbs, NULL));
    ESP_ERROR_CHECK(adc_continuous_start(handle));
}

#define TEST1 1

static uint8_t dummy[READ_LEN];

extern "C" void app_main()
{
    static const char* TAG = "app_main";

    ESP_LOGI(TAG, "entry");

    init_adc();

    for(;;)
    {
        uint32_t actual_read;

#if TEST1
        while(adc_continuous_read(
            handle,
            dummy,
            READ_LEN,
            &actual_read,
            pdMS_TO_TICKS(250)) == ESP_OK);
#endif

        ESP_LOGI(TAG, "ov=%u isr=%u average=%" PRIu32, overflow_counter, isr_counter, average);

#if TEST1        
        vTaskDelay(pdMS_TO_TICKS(250));
#else
        vTaskDelay(pdMS_TO_TICKS(1000));
#endif
    }
}