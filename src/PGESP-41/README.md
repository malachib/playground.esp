# Overview

Document v0.1

# 1. RESERVED

# 2. Targets

## 2.1. Main App

Based on official reference [1.1]

### 2.1.1. Conclusions

Based on Section 3.1:

* For ultra-real-time processing, it appears the DMA buffers are handed to us in real time.  Nice
* xRingbuffer is the perfect tool for cross-task consumption so if processing from a separate task is your bag, you're set.  Also nice.

It makes no sense to roll our own buffer copy, since their xRingbuffer interaction is great as is.  

# 3. Observations

## 3.1. Direct esp-idf code review

`xRingbuffer` is indeed used by `adc_continuous_new_handle` [2.1] for `max_store_buf_size` pool.

Additionally, internal DMA buffers are allocated for the conversion frames themselves.  This is `conv_frame_size` x 5.  Noteworthy is that 5 is hardcoded (macro `INTERNAL_BUF_NUM`)

Aforementioned is tracked in internal `rx_dma_buf`.  That's accomanied by internal `hal.rx_desc` which denotes augmented size of said buf.

`s_adc_dma_intr` then responds to incoming ADC DMA events.  Setup for this varies
per ESP32 variant, but they all seem to land here.  This function independently:

* copies completed buffer into ringbuf via `xRingbufferSendFromISR`
* invokes `on_conv_done`

`xRingbuffer` isn't designed to auto-lose the data from its tail, so documentation's indication that new data is lost is 100% accurate.  However,
this does affect invocation of `on_conv_done`.

A number of different approachs set up this DMA channel:

* I2S0 DMA - ESP32 only
* SPI3 DMA - ESP32S2 only
* GDMA (default) - All other ESP32s, undocumented

I presume that DMA APIs operate in the traditional linked list/circular buffer way.

## 3.2. Consumption of DMA buffer outside ISR

It seems it MAY be possible to use DMA buffers directly from a separate consuming task outside the ISR.

Caveats:

* It's conceivable buffers will overrun before task can service them due to how fast DMA/ADC works
* There appear to be 5 'slots' total in RX DMA.  This further limits options for servicing before overrun occurs.

NOTE: `portYIELD_FROM_ISR` is a feature activated by wake/yield return bool.  This *might* speed along wakeups, but I am not 100% sure.

### 3.2.1. Example 1

At 20Khz with 256 byte frame (as provided by example [1.1]) that comes out to 50µs per sample.  Sample size is 16-bit (ESP32 & ESP32S2) or 32-bit (all others).
For 32-bit, That works out to 3.2ms per frame.

Expecting a ~10ms wait before RTOS notices it's time to do something, that means
we can expect 3 full frames before RTOS begins to service things.

### 3.2.2. Example 2

## 3.3. Hardware Limitations

ESP32 "ADC2 does not support DMA mode."

# References

1. https://docs.espressif.com/projects/esp-idf/en/v5.1.2/esp32/api-reference/peripherals/adc_continuous.html
    1. https://github.com/espressif/esp-idf/tree/v5.1.2/examples/peripherals/adc/continuous_read
2. https://github.com/espressif/esp-idf/tree/v5.1.2/components/esp_adc
    1. https://github.com/espressif/esp-idf/blob/v5.1.2/components/esp_adc/adc_continuous.c
