/*
 * libcoap.h -- platform specific header file for CoAP stack
 *
 * Copyright (C) 2015 Carsten Schoenert <c.schoenert@t-online.de>
 *
 * This file is part of the CoAP library libcoap. Please see README for terms
 * of use.
 */

#ifndef _LIBCOAP_H_
#define _LIBCOAP_H_

// shim

#endif /* _LIBCOAP_H_ */
