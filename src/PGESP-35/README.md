# PGESP-35: Deep sleep w/ WiFi reconnect

Code based on [4]

## Requirements

esp-idf v5.0

ESP32 or
ESP32 C3 

## Observations

### ESP32C3: "Configuring IOs"

We need to utilize an external pullup resistor.  Set config to INTERNAL_PULLUP_DISABLE

[1.2] indicates GPIOs in sleep mode have "pullups or pulldowns, which are enabled by default".  Furthermore, it indicates "will be in a high impedance state".

I read all that to mean we can configure an internal pullup for the GPIO.  

#### ESP32C3: Internal pullup

Despite above, there is no test in which I can reliabily wake the device on high signal when using an internal pullup.  Furthermore, regular non-sleep ISR/GPIO stops working after one timer-based return from deep sleep cycle


#### ESP32: EXT0

Valid RTC IO pins [1] (esp_sleep_enable_ext0_wakeup):

Variant | Range   
------- | --------
ESP32   | 0, 2, 4, 12-15, 25-27, 32-39
ESP32S2 | 0-21
ESP32S3 | 0-21



## Results

| Date     | Platform | Board      | Timer wake | Pin Wake | Notes 
| -------- | -------- | ---------- | ---------- | -------- | -------
| 14JAN23  | ESP32    | DevKit     | Pass       | Untested | Haven't measured current draw with meter yet
| 27JAN23  | ESP32    | Wemos D1   | Pass       | Pass     | 
| 27JAN23  | ESP32C3  | DevKitM-1  | Pass       | Pass     |


### ESP32C3 DevKitM-1

Current measurements:

* 1.1ma in deep sleep (measured going through 5V LDO)

### Wemos D1

Current measurements:

* 29ma on startup (WiFi 100% off)
* 1.2ma in deep sleep

Tests were done before GPIO code was fully enabled (sleep timer only), so RTC domain power consumption may increase these numbers.

# References

1. https://docs.espressif.com/projects/esp-idf/en/v5.0/esp32/api-reference/system/sleep_modes.html
   1. https://docs.espressif.com/projects/esp-idf/en/v5.0/esp32c3/api-reference/system/sleep_modes.html
   2. https://docs.espressif.com/projects/esp-idf/en/v5.0/esp32c3/api-reference/system/sleep_modes.html#configuring-ios
2. https://docs.espressif.com/projects/esp-idf/en/v5.0/esp32/api-guides/deep-sleep-stub.html
3. RESERVED
4. https://github.com/espressif/esp-idf/tree/v5.0/examples/system/deep_sleep
5. https://github.com/espressif/arduino-esp32/issues/6656
6. https://esp32.com/viewtopic.php?t=22622