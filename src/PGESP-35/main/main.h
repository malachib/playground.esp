#pragma once

#include <chrono>

#include "pin.h"

#include "driver/rtc_io.h"

// This flows out to 'gettimeofday' which is the recommended high level way to
// get at (among other clocks) an RTC-backed clock
using std::chrono::system_clock;

void initialize();
void wifi_resume();

extern system_clock::time_point sleep_enter_time;
extern int sleep_counter;

namespace experimental {

void test1();

}