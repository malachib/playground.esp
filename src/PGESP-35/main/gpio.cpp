#include <embr/platform/esp-idf/gpio.h>
#include <embr/platform/esp-idf/gptimer.h>
#include <embr/platform/esp-idf/pm.h>

#include "soc/soc_caps.h"
#include "driver/rtc_io.h"
#include "soc/rtc.h"

#include <esp-helper.h>

#include "esp_log.h"
#include "esp_sleep.h"
#include "esp_wifi.h"

#include "pin.h"

#if CONFIG_ENABLE_WAKEUP_PIN

static const char* TAG = "PGESP-35::gpio";

using namespace embr::esp_idf;

#if defined(CONFIG_SOC_GPIO_SUPPORT_DEEPSLEEP_WAKEUP)

// DEBT: No pullup support - either add it, or specify why we can'
// (didn't seem to activate last time we tried to use it)t

#if CONFIG_WAKEUP_HIGH_LEVEL
#define WAKEUP_MODE ESP_GPIO_WAKEUP_GPIO_HIGH
#else
#define WAKEUP_MODE ESP_GPIO_WAKEUP_GPIO_LOW
#endif


// NOTE: Observed error
// "sleep: invalid mask, please ensure gpio number is no more than 5"
void init_wakeup_pin()
{
    const uint64_t pin_mask = 1ULL << wake_pin;

    //esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_ON);

    // NOTE: Makes no difference - got from [5]
    //gpio_deep_sleep_hold_dis();
    //esp_sleep_config_gpio_isolate();

    // NOTE: Makes no difference
    //ESP_ERROR_CHECK(esp_sleep_enable_gpio_wakeup());

    // According to ESP32C3 doc [1.1] this works for deep or light sleep
    // NOTE: Disables ISR after one sleep cycle happens
    ESP_ERROR_CHECK(esp_deep_sleep_enable_gpio_wakeup(pin_mask, WAKEUP_MODE));

    // NOTE: Makes no difference
    //ESP_ERROR_CHECK(esp_sleep_enable_gpio_wakeup());

    ESP_LOGI(TAG, "init_wakeup_pin: GPIO mode %d / wakeup high=%u / mask=%llx",
        CONFIG_WAKEUP_PIN, WAKEUP_MODE == ESP_GPIO_WAKEUP_GPIO_HIGH, pin_mask);
}

#endif


static void isr_handler(void*)
{
    ESP_DRAM_LOGI(TAG, "isr: %u", wake_pin.level());
}

static void init_gpio(gpio pin)
{
    const uint64_t pin_mask = 1ULL << pin;

    const gpio_config_t config = {
        .pin_bit_mask = pin_mask,
        .mode = GPIO_MODE_INPUT,
#if CONFIG_INTERNAL_PULL_DOWN
        .pull_up_en = GPIO_PULLUP_DISABLE,
        .pull_down_en = GPIO_PULLDOWN_ENABLE,
#elif CONFIG_INTERNAL_PULL_UP
        .pull_up_en = GPIO_PULLUP_ENABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
#else
        .pull_up_en = GPIO_PULLUP_DISABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
#endif
        .intr_type = GPIO_INTR_ANYEDGE
        //.intr_type = GPIO_INTR_DISABLE
    };

    ESP_ERROR_CHECK(gpio_config(&config));
}


static void init_isr(gpio pin)
{
    ESP_LOGI(TAG, "init_isr: entry");
    
    ESP_ERROR_CHECK(gpio_install_isr_service(ESP_INTR_FLAG_LEVEL1));
    ESP_ERROR_CHECK(gpio_isr_handler_add(pin, isr_handler, nullptr));
}


void init_gpio()
{
    // This is only relevant for RTC domain, not innate GPIO sleep support
#if !defined(CONFIG_SOC_GPIO_SUPPORT_DEEPSLEEP_WAKEUP)
    rtc_gpio_deinit(wake_pin);
#endif

    init_gpio(wake_pin);
    init_isr(wake_pin);
}

#endif
