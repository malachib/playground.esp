#include <inttypes.h>

#include <estd/chrono.h>
#include <estd/thread.h>

#include <embr/platform/esp-idf/gpio.h>
#include <embr/platform/esp-idf/gptimer.h>
#include <embr/platform/esp-idf/pm.h>

#include "soc/soc_caps.h"
#include "driver/rtc_io.h"
#include "soc/rtc.h"

#include <esp-helper.h>

#include "esp_log.h"
#include "esp_sleep.h"
#include "esp_wifi.h"

#include "main.h"

using namespace embr::esp_idf;


// DEBT: Using EXT1 because it seems like EXT0 doesn't have access to all pins.  Not sure
// though.  If EXT0 *does* have full access, we prefer it because it takes less power to do
// pullup/pulldown

static const char* TAG = "PGESP-35";


RTC_DATA_ATTR system_clock::time_point sleep_enter_time;
RTC_DATA_ATTR int sleep_counter = 0;

static void analyzer()
{
    system_clock::duration elapsed = system_clock::now() - sleep_enter_time;

    auto boot_time = estd::chrono::esp_clock::now();
    // DEBT: Add conversion operator to convert estd -> std duration
    std::chrono::microseconds boot_duration(boot_time.time_since_epoch().count());

    // Only reports ~34ms, not ~345 ms which is what logger reports
    ESP_LOGV(TAG, "analyzer: boot_time=%llu us", boot_duration.count());

    //elapsed -= boot_time.time_since_epoch();
    elapsed -= boot_duration;

    switch (esp_sleep_get_wakeup_cause())
    {
        case ESP_SLEEP_WAKEUP_EXT0:
            ESP_LOGI(TAG, "analyzer: EXT0 wakeup");
            break;

#if !SOC_GPIO_SUPPORT_DEEPSLEEP_WAKEUP
        case ESP_SLEEP_WAKEUP_EXT1:
        {
            uint64_t wakeup_pin_mask = esp_sleep_get_ext1_wakeup_status();
            ESP_LOGI(TAG, "analyzer: EXT1 wakeup: %" PRIx64, wakeup_pin_mask);
            break;
        }
#else
        // "ESP_SLEEP_WAKEUP_GPIO in enum esp_sleep_source_t is for light sleep only." [5]
        // For ESP32C3 that is not the case, we're properly getting this message
        case ESP_SLEEP_WAKEUP_GPIO:
        {
            uint64_t mask = esp_sleep_get_gpio_wakeup_status();
            ESP_LOGI(TAG, "analyzer: GPIO wakeup");
            break;
        }
#endif

        case ESP_SLEEP_WAKEUP_TIMER:
            ESP_LOGI(TAG, "analyzer: timer wakeup");
            break;

        default:
            ESP_LOGI(TAG, "analyzer: Not a recognized sleep wakeup");
            break;
    }

    ESP_LOGI(TAG, "Slept for %llu ms / sleep counts=%d", 
        estd::chrono::milliseconds(elapsed).count(),
        ++sleep_counter);
}

void initialize()
{
    analyzer();

    // TODO: Probably only matters during WiFi config, if so move to WiFi only
    // area
    init_flash();

#if CONFIG_ENABLE_WAKEUP_PIN
    init_gpio();
#endif

    const int wakeup_time_sec = CONFIG_WAKEUP_TIMEOUT;
    ESP_LOGI(TAG, "initializer: Enabling timer wakeup, %ds", wakeup_time_sec);
    esp_sleep_enable_timer_wakeup(wakeup_time_sec * 1000000);

    // doesn't hurt anything - keeping commented until we can measure results
    //experimental::test1();
}