#include <estd/thread.h>
#include <estd/iostream.h>

#include <esp_log.h>
#include <esp_sleep.h>
#include <esp_system.h>

#include "main.h"

using namespace estd::chrono_literals;

extern "C" void app_main(void)
{
    static const char* TAG = "PGESP-35: app_main";

    ESP_LOGD(TAG, "Bringup");

    initialize();

#if CONFIG_ENABLE_WIFI
    wifi_resume();
#endif

    ESP_LOGI(TAG, "Entering deep sleep in 5s");
    // Because I'm nervous about deep sleep interrupting our flashing ability
    estd::this_thread::sleep_for(5s);
    
    ESP_LOGI(TAG, "Sleeping");
    sleep_enter_time = system_clock::now();
#if CONFIG_ENABLE_WAKEUP_PIN
    // Enabling wakeup pin at last second so that it doesn't interfere
    // with regular GPIO.  Not sure if that is truly necessary
    init_wakeup_pin();
#endif
    esp_deep_sleep_start();
}
