#include <sdkconfig.h>

#if CONFIG_ENABLE_WAKEUP_PIN
#include <embr/platform/esp-idf/gpio.h>

constexpr embr::esp_idf::gpio wake_pin((gpio_num_t)CONFIG_WAKEUP_PIN);

void init_gpio();
void init_wakeup_pin();
#endif