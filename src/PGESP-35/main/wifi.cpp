#include <esp_log.h>
#include <esp_system.h>
#include <esp_wifi.h>

#include <estd/port/freertos/timer.h>

#include <embr/platform/lwip/iostream.h>
#include <embr/platform/lwip/pbuf.h>
#include <embr/platform/lwip/udp.h>

#include <esp-helper.h>

#include "main.h"

using namespace estd::chrono_literals;

// UDP ECHO as per https://www.rfc-editor.org/rfc/rfc862

//int lwip_inet_pton(int af, const char *src, void *dst);

#if CONFIG_ENABLE_WIFI
static void emitter(TimerHandle_t);

static estd::freertos::timer<true> timer("UDP emitter", 1s, true,
    nullptr, emitter);

static embr::lwip::udp::Pcb pcb;
static RTC_DATA_ATTR int counter = 0;

static const char* TAG = "PGESP-35: wifi";

static void emitter(TimerHandle_t h)
{
    ESP_LOGD(TAG, "emitter: counter=%d", counter);

    estd::freertos::internal::timer timer(h);

    //embr::lwip::Pbuf pbuf(128);

    //uint32_t addr = ipaddr_addr(CONFIG_CALLBACK_HOST);
    //ip4_addr_t addr;
    //ip4addr_aton(CONFIG_CALLBACK_HOST, &addr);
    ip_addr_t addr;
    ipaddr_aton(CONFIG_CALLBACK_HOST, &addr);

    //embr::lwip::v1::Pbuf buffer(32);
    embr::lwip::opbufstream out(32);

    out << "Hello to you: " << counter;

    out.rdbuf()->shrink();

    pcb.send(out.rdbuf()->pbuf(), &addr, 7);

    if(++counter == 3)
    {
        timer.stop(100ms);
        ESP_LOGI(TAG, "emitter: done");
    }

    // TODO: free up pcb just as an excercise -- it has to be kind of a semaphore where
    // we receive back all the echos (or timeout) and have completed all our sends.  It doesn't
    // truly matter for this test since we reboot anyway
}

static void udp_receiver(void* arg,
    struct udp_pcb* pcb, struct pbuf* p, const ip_addr_t* addr, u16_t port)
{
    embr::lwip::v1::Pbuf buffer(p, false);
    char s[128];

    s[buffer.copy_partial(s, sizeof(s), 0)] = 0;

    ESP_LOGI(TAG, "udp_receiver: %s", s);
}


static void event_handler2(void* arg, esp_event_base_t event_base,
                            int32_t event_id, void* event_data)
{
    ESP_LOGD(TAG, "event_handler2: kicking off timer");

    timer.start(100ms);
}

void wifi_resume()
{
    if(strlen(CONFIG_CALLBACK_HOST) == 0)
    {
        ESP_LOGD(TAG, "wifi_resume: disabled due to no host config");
    }

    if(esp_reset_reason() == ESP_RST_POWERON || sleep_counter % 5 == 0)
    {
        pcb.alloc();

        pcb.bind(7);
        pcb.recv(udp_receiver);

        wifi_init_sta();

        // DEBT: Theoretical race condition where we get IP before this registration
        // occurs.  In reality, WiFi is never that fast
        ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler2, NULL));
    }
}
#endif