#include <embr/platform/esp-idf/gpio.h>
#include <embr/platform/esp-idf/gptimer.h>
#include <embr/platform/esp-idf/pm.h>

#include "soc/soc_caps.h"
#include "driver/rtc_io.h"
#include "soc/rtc.h"
#include "soc/rtc_cntl_reg.h"

#include "soc/gpio_periph.h"

namespace experimental {

void test1()
{
#if CONFIG_SOC_GPIO_SUPPORT_DEEPSLEEP_WAKEUP
    // EXPERIMENTAL from [6], Does not seem to interfere with GPIO wake
    for (int gpio_num = GPIO_NUM_0; gpio_num < GPIO_NUM_MAX; gpio_num++)
    {
        if (GPIO_IS_VALID_GPIO(gpio_num))
        {
            if (gpio_num <= GPIO_NUM_5) {
                REG_CLR_BIT(RTC_CNTL_PAD_HOLD_REG, BIT(gpio_num));
            } else {
                CLEAR_PERI_REG_MASK(RTC_CNTL_DIG_PAD_HOLD_REG, GPIO_HOLD_MASK[gpio_num]);
            }
        }
    }
#endif
}

}