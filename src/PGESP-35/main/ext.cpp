#include <embr/platform/esp-idf/gpio.h>
#include <embr/platform/esp-idf/gptimer.h>
#include <embr/platform/esp-idf/pm.h>

#include "soc/soc_caps.h"
#include "driver/rtc_io.h"
#include "soc/rtc.h"

#include <esp-helper.h>

#include "esp_log.h"
#include "esp_sleep.h"
#include "esp_wifi.h"

#include "pin.h"

#define CONFIG_ENABLE_EXT0 1

#if defined(CONFIG_ENABLE_WAKEUP_PIN) && !defined(CONFIG_SOC_GPIO_SUPPORT_DEEPSLEEP_WAKEUP)
using namespace embr::esp_idf;

static const char* TAG = "PGESP-35::ext";

static void rtc_internal_pullups(gpio_num_t pin)
{
#if CONFIG_INTERNAL_PULL_DOWN
    constexpr bool pullup = false;
    rtc_gpio_pullup_dis(pin);
    rtc_gpio_pulldown_en(pin);
#else
    constexpr bool pullup = true;
    rtc_gpio_pullup_en(pin);
    rtc_gpio_pulldown_dis(pin);
#endif

    ESP_LOGI(TAG, "rtc_internal_pullups: pullup=%u", pullup);
}

#if CONFIG_ENABLE_EXT0

#if CONFIG_WAKEUP_HIGH_LEVEL
#define WAKEUP_LEVEL 1
#else
#define WAKEUP_LEVEL 0
#endif

static void init_wakeup_ext0()
{
    esp_sleep_enable_ext0_wakeup(wake_pin, WAKEUP_LEVEL);

#if !CONFIG_INTERNAL_PULLUP_DISABLE
    // "No need to keep that power domain explicitly, unlike EXT1." - [4]
    rtc_internal_pullups(wake_pin);
#endif
}

#else

#if CONFIG_WAKEUP_HIGH_LEVEL
#define WAKEUP_LEVEL ESP_EXT1_WAKEUP_ANY_HIGH
#else
#define WAKEUP_LEVEL ESP_EXT1_WAKEUP_ALL_LOW
#endif

// NOTE: Not well tested
static void init_wakeup_ext1()
{
    const uint64_t pin_mask = 1ULL << wake_pin;

    esp_sleep_enable_ext1_wakeup(pin_mask, WAKEUP_LEVEL);

#if !CONFIG_INTERNAL_PULLUP_DISABLE
    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_ON);
    rtc_internal_pullups(wake_pin);
#endif
}

#endif

void init_wakeup_pin()
{
#if CONFIG_ENABLE_EXT0
    init_wakeup_ext0();
#else
    init_wakeup_ext1();
#endif
}



#endif