#include <esp_log.h>

#include <estd/string.h>

#include <embr/platform/esp-idf/allocator.h>

#include "lib.h"

static const char* TAG = "PGESP-40::lib";

namespace pgesp_40 {

Lib::Lib()
{
    ESP_LOGI(TAG, "Lib::Lib entry");
}

Lib::~Lib()
{
    ESP_LOGI(TAG, "Lib::~Lib entry");
}

}