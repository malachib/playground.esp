# Overview

Create a full "pure CMake" library consuming other components (including
via component manager)

# Detail

~~May be a non starter [1.2]~~
Proof of concept for "pure CMake" library usage - specifically, a CMakeLists.txt
which is usable by normal non ESP-IDF as well as directly by ESP-IDF

# 1. Targets

# 2. Reserved

# 3. Observations

## 3.1. App vs Lib

A great deal of effort seems required to make a pure CMake app [2] and may not even be possible [1.2].  Fortunately we're interested in a pure Cmake lib,
which so far seems to be unceremonious

## 3.2. estd

Despite the findings of 3.1. something still holds estd back from participating as a "pure CMake" library.  We get these errors:

```
CMake Error: install(EXPORT "estdTargets" ...) includes target "estd" which requires target "__idf_cxx" that is not in any export set.
CMake Error: install(EXPORT "estdTargets" ...) includes target "estd" which requires target "__idf_newlib" that is not in any export set.
CMake Error: install(EXPORT "estdTargets" ...) includes target "estd" which requires target "__idf_freertos" that is not in any export set.
CMake Error: install(EXPORT "estdTargets" ...) includes target "estd" which requires target "__idf_esp_hw_support" that is not in any export set.
CMake Error: install(EXPORT "estdTargets" ...) includes target "estd" which requires target "__idf_heap" that is not in any export set.
CMake Error: install(EXPORT "estdTargets" ...) includes target "estd" which requires target "__idf_log" that is not in any export set.
CMake Error: install(EXPORT "estdTargets" ...) includes target "estd" which requires target "__idf_soc" that is not in any export set.
CMake Error: install(EXPORT "estdTargets" ...) includes target "estd" which requires target "__idf_hal" that is not in any export set.
CMake Error: install(EXPORT "estdTargets" ...) includes target "estd" which requires target "__idf_esp_rom" that is not in any export set.
CMake Error: install(EXPORT "estdTargets" ...) includes target "estd" which requires target "__idf_esp_common" that is not in any export set.
CMake Error: install(EXPORT "estdTargets" ...) includes target "estd" which requires target "__idf_esp_system" that is not in any export set.
CMake Error: install(EXPORT "estdTargets" ...) includes target "estd" which requires target "__idf_xtensa" that is not in any export set.
```

It turns out `install(EXPORT` CMake phase somehow has an issue.  Since ESP-IDF has
its own package management, we can disable that mechanism.  However, in the greater
scope of "pure cmake" we really want that still, since it likely impedes any
"find_package" you may want to do.  DEBT, since likely you won't be with estd+ESP-IDF

## 3.3. "custom" CMake

I just stumbled on what appears to be an alternate/upgrade to "pure" CMake [1.4]
It seems `idf.cmake` has some wrapper-y things (ala Qt)

# References

1. https://docs.espressif.com/projects/esp-idf/en/v5.1.2/esp32/api-guides/build-system.html
    1. https://docs.espressif.com/projects/esp-idf/en/v5.1.2/esp32/api-guides/build-system.html#writing-pure-cmake-components
    2. https://github.com/espressif/esp-idf/issues/10579
    3. https://docs.espressif.com/projects/esp-idf/en/v5.1.2/esp32/api-guides/build-system.html#using-esp-idf-components-from-external-libraries 
    4. https://docs.espressif.com/projects/esp-idf/en/v5.2.2/esp32/api-guides/build-system.html#using-esp-idf-in-custom-cmake-projects
2. https://github.com/knicklux/eps-idf-pure-cmake/blob/master/CMakeLists.txt 
