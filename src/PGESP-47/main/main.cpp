#include <iostream>

#include <esp_log.h>
#include <esp_heap_caps.h>
#include <esp_camera.h>

#include "human_face_detect_msr01.hpp"
#include "human_face_detect_mnp01.hpp"
#include "dl_tool.hpp"


#include "init.h"

static const char* TAG = "PGESP-47::main";

// https://github.com/espressif/esp-dl/tree/master/examples/human_face_detect
// https://github.com/espressif/esp32-camera/tree/master/examples/camera_example

extern "C" void app_main()
{
    ESP_LOGI(TAG, "entry");

    init_camera();

    dl::tool::Latency latency;

    // initialize
#if TWO_STAGE
    HumanFaceDetectMSR01 s1(0.1F, 0.5F, 10, 0.2F);
    HumanFaceDetectMNP01 s2(0.5F, 0.3F, 5);
#else // ONE_STAGE
    HumanFaceDetectMSR01 s1(0.3F, 0.5F, 10, 0.2F);
#endif

    while(1)
    {
        static uint8_t image[75 * 1024];

        camera_fb_t* pic = esp_camera_fb_get();

        fmt2rgb888(pic->buf, pic->len, PIXFORMAT_JPEG, image);

        // inference
        latency.start();
#if TWO_STAGE
        std::list<dl::detect::result_t>& candidates = s1.infer(image, {(int)pic->height, (int)pic->width, 3});
        std::list<dl::detect::result_t>& results = s2.infer(image, {(int)pic->height, (int)pic->width, 3}, candidates);
#else // ONE_STAGE
        std::list<dl::detect::result_t>& results = s1.infer(image, {(int)pic->height, (int)pic->width, 3});
#endif
        latency.end();
        latency.print("Inference latency");

        esp_camera_fb_return(pic);
    }
}