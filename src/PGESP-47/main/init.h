#pragma once

// DEBT: Move all this to embr board traits

#ifdef CONFIG_BOARD_ESP32S3_SEEED_XIAO
#define CAM_PIN_PWDN -1  //power down is not used
#define CAM_PIN_RESET -1 //software reset will be performed
#define CAM_PIN_XCLK 10
#define CAM_PIN_SIOD 40
#define CAM_PIN_SIOC 39

#define CAM_PIN_D7 48       // DVP_Y9
#define CAM_PIN_D6 11       // DVP_Y8
#define CAM_PIN_D5 12       // DVP_Y7
#define CAM_PIN_D4 14       // DVP_Y6
#define CAM_PIN_D3 16       // DVP_Y5
#define CAM_PIN_D2 18       // DVP_Y4
#define CAM_PIN_D1 17       // DVP_Y3
#define CAM_PIN_D0 15       // DVP_Y2

#define CAM_PIN_VSYNC 38
#define CAM_PIN_HREF 47
#define CAM_PIN_PCLK 13
#else
#error Only Seeed supported
#endif


void init_camera();