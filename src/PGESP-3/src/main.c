#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

void task_i2cscanner(void*);

void task_i2cscanner_wrapper(void* ignore)
{
    for(;;)
    {
        xTaskCreate(task_i2cscanner, "_scanner", 1024, NULL, 2, NULL);
        vTaskDelay(60000 / portTICK_PERIOD_MS);
    }
}
void app_main()
{
    xTaskCreate(task_i2cscanner, "scanner", 2048, NULL, 2, NULL);
}