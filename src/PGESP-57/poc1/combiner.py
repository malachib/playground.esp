#!/bin/env python3

#root_path=".."
tool_version = "0.1"

# 25AUG24 MB
# TODO: Move this to somewhere like `useful_scripts`
# * Keep an eye on "mergeoverride" feature [1], bring that in for semver
# * Keep an eye on enhanced semver range comparisons [1.2]

import os
import sys
import logging
import hiyapyco
import argparse

logging.basicConfig(level=logging.INFO)
#logger = logging.getLogger(__name__)
logger = logging.getLogger("yaml-merge")
logger.debug("STARTUP")

# DEBT: Better to do __name__ == '__main__'
if __name__ != '__main__':
    sys.exit(0)

# DEBT: Can't quite get cmake to flow this through yet
#components = os.environ["COMPONENT_DIRS"]
#print(f"GOT HERE: {components}")

parser = argparse.ArgumentParser(
    prog=f'yaml-merge',
    description=f'v{tool_version} Combines YAML files together',
    epilog='Thank you')

parser.add_argument('-i', '--input',
    help="colon delimited list of input YML files")
parser.add_argument('--delim',
    default=':',
    help="specify delimiter for input list (default ':')")
parser.add_argument('-o', '--output',
# Somehow passing in a string here + store_const makes this unhappy
#    action='store_const',
#    const="output_file",
    help="specify output file instead of stdout")
parser.add_argument('--cwd',
    default="..",
    help="current working directory DEBT")

args = parser.parse_args();

root_path = args.cwd

logger.info(f'combiner: root_path={root_path}')

ymls = [
    f'{root_path}/components/poc1-lib/idf_component.yml',
    f'{root_path}/idf_component.in.yml',
]

if args.input:
    delim = args.delim
    logger.debug(f'inputs: {args.input}')
    ymls = args.input.split(delim)    # DEBT: We want ';'
    logger.debug(f'inputs: {ymls}')
    ymls = [f'{root_path}/{i}' for i in ymls]
    logger.info(f'inputs: {ymls}')

#print("hi2u combiner")
#conf = hiyapyco.load('f1.yml', 'f2.yml', method=hiyapyco.METHOD_MERGE)
# DEBT: Pass in proper path, don't use relative path
conf = hiyapyco.load(
    ymls,
    #f'{root_path}/components/poc1-lib/idf_component.yml',
    #f'{root_path}/idf_component.in.yml',
    method=hiyapyco.METHOD_MERGE)
yaml = hiyapyco.dump(conf, default_flow_style=False)

if args.output == None:
    print(yaml)
else:
    f = open(args.output, "w")
    f.write(yaml)
