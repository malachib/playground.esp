#!/bin/env python3

# Guidance from
# https://stackoverflow.com/questions/1471994/what-is-setup-py
# https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/

# 25AUG24 - "pip install ." fails with an oblique /tmp/egg error
# Everyone harps on about upgrading pip/setuptools, i.e.
# https://stackoverflow.com/questions/74440346/what-does-error-no-egg-info-directory-found-in-tmp-pi-p-pip-egg-info-kt94jna
# But that's not the issue

from setuptools import setup

import combiner

setup(
   name='yaml-merge',
   version=combiner.tool_version,
   description='idf_component.yml merger for ESP-IDF',
   author='Malachi Burke',
   #author_email='foomail@foo.example',
   py_modules=['combiner'],
   #packages=['yaml-merge'],
   # DEBT: Put specific version specifier in here
   install_requires=['hiyapyco'], #external packages as dependencies
)