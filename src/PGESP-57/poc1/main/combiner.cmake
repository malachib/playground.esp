# Doesn't like being in top level CMakeLists.txt for some reason

# Works pretty well except it doesn't run early enough in the build for esp component
# manager to pick it up when yaml files change

# Guidance from
# https://stackoverflow.com/questions/11971917/why-does-cmake-make-a-distinction-between-a-target-and-a-command
# for general add_custom_command behaviors
# Guidance from
# https://www.esp32.com/viewtopic.php?t=29962
# add_custom_command doesn't seem to work on its own, needs the custom target

#set($ENV{COMPONENT_DIRS} ${CMAKE_CURRENT_LIST_DIR}/../components)
message(STATUS "GOT HERE: COMPONENT_DIRS=${EXTRA_COMPONENT_DIRS}")
# DEBT: Doing this because regular COMPONENT_DIRS doesn't seem available.  Perhaps
# we need "idf_build_get_property"?
list(APPEND MY_COMPONENTS_DIRS ${EXTRA_COMPONENT_DIRS})
list(APPEND MY_COMPONENTS_DIRS ${PROJECT_DIR}/components)

#FILE(GLOB MY_COMPONENT_LIST RELATIVE ${PROJECT_DIR}/components ${PROJECT_DIR}/components/*)
foreach(MY_COMPONENT_DIR ${MY_COMPONENTS_DIRS})
    FILE(GLOB MY_COMPONENT_LIST
        #RELATIVE ${MY_COMPONENT_DIR}
        RELATIVE ${PROJECT_DIR}
        # Absolute path-force - but not sure this really is reliable
        #RELATIVE ""
        ${MY_COMPONENT_DIR}/*/idf_component.in.yml)
    message(STATUS "GOT HERE GLOB: ${MY_COMPONENT_LIST}")
    list(APPEND MY_YMLS ${MY_COMPONENT_LIST})
endforeach()

list(APPEND MY_YMLS idf_component.in.yml)

# DEBT: Somehow ';' evaporates on the way in to combiner.py
list(JOIN MY_YMLS ":" MY_NEW_YMLS)
list(TRANSFORM MY_YMLS PREPEND ${PROJECT_DIR}/ OUTPUT_VARIABLE MY_ABSOLUTE_YMLS)

message(STATUS "PROJECT_DIR: ${PROJECT_DIR}")
message(STATUS YMLs: "${MY_YMLS}")
message(STATUS Abs YMLs: "${MY_ABSOLUTE_YMLS}")
#message(STATUS "GOT HERE GLOB: ${MY_COMPONENT_LIST}")

set(YAML_OUTPUT ${PROJECT_DIR}/main/idf_component.yml)

#add_custom_command(OUTPUT /tmp/touched COMMAND echo touch COMMAND touch /tmp/touched)
add_custom_command(
    DEPENDS ${MY_ABSOLUTE_YMLS}
    OUTPUT ${YAML_OUTPUT}
    COMMAND ${PROJECT_DIR}/combiner.py
        -o "${YAML_OUTPUT}"
        --cwd "${PROJECT_DIR}"
        -i "${MY_NEW_YMLS}"
        )

add_custom_target(yaml_gen DEPENDS
#    /tmp/touched
    ${YAML_OUTPUT})

add_dependencies(${COMPONENT_LIB} yaml_gen)
