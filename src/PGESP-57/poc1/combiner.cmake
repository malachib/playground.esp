find_package(Python COMPONENTS Interpreter REQUIRED)

# Guidance from [1.1]
if(DEFINED ENV{IDF_PYTHON_ENV_PATH})
    set(_pip_args)
else()
    message(FATAL_ERROR "Needs ESP-IDF activated")
endif()

#set($ENV{COMPONENT_DIRS} ${CMAKE_CURRENT_LIST_DIR}/components)
#set($ENV{COMPONENT_DIRS} ${COMPONENT_DIRS})

# DEBT: Use add_custom_command to notice changes in yml inputs
# https://cmake.org/cmake/help/latest/command/add_custom_command.html
# Be advised redirecting stdout seems to be nontrivial, so we'll want script
# to output file directly

execute_process(COMMAND ${Python_EXECUTABLE} -m pip install hiyapyco)
#execute_process(COMMAND ${Python_EXECUTABLE} ${CMAKE_CURRENT_LIST_DIR}/combiner.py
#    OUTPUT_FILE ${CMAKE_CURRENT_LIST_DIR}/main/idf_component.yml)

add_custom_command(OUTPUT /tmp/touched COMMAND echo touch COMMAND touch /tmp/touched)
add_custom_target(touch_target DEPENDS /tmp/touched)

#idf_build_get_property(MY_PROJECT_NAME PROJECT_NAME)
#add_dependencies(${MY_PROJECT_NAME} touch_target)