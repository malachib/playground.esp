# Overview

# 1. Design Goals

## 1.1. POC1

Find a way to use idf_components.yml at the component level rather than the main app level.

Using `HiYaPyCo` [1] to combine YML files

## 1.2. POC2

Use FetchContent as depdendency for lib1
Use idf_component.yml for lib2 (echo of PGESP-62)

## 1.3. POC3

Knowing not about `set(COMPONENTS MAIN)` behavior, this poc specifically focuses on an elegant way to keep using that optimization.

## 1.4. POC4

Since POC2 got commandeered away from FetchContent testing, focus
FetchContent testing here.

## 1.5. POC5

Works remakeably well with v5.2.3
Note even when library is not linked in (lib2), component manager still activates for it (pulls down and compiles mDNS)

# References

1. https://pypi.org/project/HiYaPyCo/
    1. https://www.scivision.dev/cmake-install-python-package/
    2. https://github.com/python-semver/python-semver/issues/241
2. https://snyk.io/advisor/npm-package/semver/functions/semver.Range