cmake_minimum_required(VERSION 3.16)

# NOTE: This was a FetchContent test, but got commandeered

# It appears that our little optimization here interrupts idf component manager
#include(../../setvars.cmake)
#set(COMPONENTS MAIN)

include($ENV{IDF_PATH}/tools/cmake/project.cmake)

project(PGESP-57-poc2)
