# Overview

Document v0.1

# 1. Design Goals

Determines:

* How to measure time spent in various TCP/IP related routines
* Bottlenecks associated with FreeNove httpd

# 2. Targets

# 2.1. App

# 2.1.1. Prerequisites

## 2.1.1.1. OpenOCD

As per [1.1], OpenOCD is required for streaming trace feature

# 2.2. iperf

Using wifi iperf from esp-idf examples 5.1.2

# 3. Opinions / Observations

Performance analysis isn't interested in post-mortem dump, so trace mode seems preferred.  Unknown if trace mode can peer into places we haven't told it to, though (probably not)

# References

1. https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-guides/performance/index.html
    1. https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-guides/app_trace.html