#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"

// perhaps within https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/wifi/esp_wifi.html
// we do a esp_wifi_get_config() to pull various values out of NVS?

extern "C" void idle_task(void*)
{

}

extern "C" void app_main(void)
{
    ESP_ERROR_CHECK(nvs_flash_init());

    printf("SDK version:%s\n", system_get_sdk_version());

    xTaskCreate(idle_task, "idle_task", 4096, NULL, 4, NULL);    
}