#include <esp_log.h>

#include <button_gpio.h>
#include <iot_button.h>

static const char* TAG = "PGESP-72::poc1";

static void button_click_cb(void* button_handle, void* usr_data)
{
    // DEBT: Docs are not specific about this, find a canonical source
    // indicating this is safe
    auto bh = (button_handle_t) button_handle;
    button_event_t event = iot_button_get_event(bh);

    ESP_LOGI(TAG, "button_click_cb: %s", iot_button_get_event_str(event));
}


extern "C" void app_main(void)
{
    const button_config_t btn_cfg {};
    const button_gpio_config_t btn_gpio_cfg
    {
        .gpio_num = CONFIG_BUTTON_IO_NUM,
        .active_level = CONFIG_BUTTON_ACTIVE_LEVEL,
        .enable_power_save = false,
        .disable_pull = false
    };

    button_handle_t btn = NULL;

    ESP_ERROR_CHECK(iot_button_new_gpio_device(&btn_cfg, &btn_gpio_cfg, &btn));

    ESP_ERROR_CHECK(iot_button_register_cb(btn, BUTTON_SINGLE_CLICK, NULL, button_click_cb, NULL));
    ESP_ERROR_CHECK(iot_button_register_cb(btn, BUTTON_DOUBLE_CLICK, NULL, button_click_cb, NULL));
}
