# Conclusions

short_press_time must be big enough so that a button up can happen within it for repeat check and ultimately double click to activate

## Results

### poc1 & poc3

Tested are whether single and double click works

| Date      | Project   | ESP-IDF   | iot-button    | Result
| -         | -         | -         | -             | -
| 27JAN25   | poc1      | v5.3.1    | 4.0.0         | pass
| 27JAN25   | poc3      | v5.3.1    | 3.5.0         | pass

# References

1. https://components.espressif.com/components/espressif/button
