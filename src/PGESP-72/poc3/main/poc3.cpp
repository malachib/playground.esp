#include <esp_log.h>

#include <button_gpio.h>
#include <iot_button.h>

static const char* TAG = "PGESP-72::poc3";

static void button_click_cb(void* button_handle, void* usr_data)
{
    button_event_t event = iot_button_get_event(button_handle);

    ESP_LOGI(TAG, "button_click_cb: %s", iot_button_get_event_str(event));
}

extern "C" void app_main(void)
{
    /* Looks to be 4.x+ 
    const button_config_t btn_cfg {};
    const button_gpio_config_t btn_gpio_cfg
    {
        .gpio_num = CONFIG_BUTTON_IO_NUM,
        .active_level = CONFIG_BUTTON_ACTIVE_LEVEL,
    };

    button_handle_t btn = NULL;

    // MIA, probably not present in 3.x version
    //esp_err_t ret = iot_button_new_gpio_device(&btn_cfg, &btn_gpio_cfg, &btn);
    */

    button_config_t btn_cfg
    {
        .type = BUTTON_TYPE_GPIO,
        .long_press_time = CONFIG_BUTTON_LONG_PRESS_TIME_MS,
        .short_press_time = CONFIG_BUTTON_SHORT_PRESS_TIME_MS,
        .gpio_button_config = {
            .gpio_num = CONFIG_BUTTON_IO_NUM,
            .active_level = CONFIG_BUTTON_ACTIVE_LEVEL,
#if CONFIG_GPIO_BUTTON_SUPPORT_POWER_SAVE
            .enable_power_save = true,
#endif
        },
    };
    button_handle_t btn = iot_button_create(&btn_cfg);
    assert(btn);

    iot_button_register_cb(btn, BUTTON_SINGLE_CLICK, button_click_cb, NULL);
    iot_button_register_cb(btn, BUTTON_DOUBLE_CLICK, button_click_cb, NULL);
}
