// wolfssl specific file.  All contents are purely for the benefit of wolfssl

#define WOLFSSL_LWIP // otherwise falls back to POSIX-y stuff (we want LwIP anyway)
#define WOLFSSL_DTLS
#define HAVE_SOCKADDR // it seems DTLS code relies on this
#define FREERTOS

#define NO_WOLFSSL_DIR

#define HAVE_ECC
#define ECC_USER_CURVES

#define WC_RSA_BLINDING // not sure if this is good or not but appears alongside ECC a lot and suppresses harden-timing warnings
