EXTRA_LIBS := ../../../../ext

COMPONENT_PRIV_INCLUDEDIRS := ${EXTRA_LIBS}/wolfssl/
# . so that we pick up "user_settings.h"
COMPONENT_PRIV_INCLUDEDIRS += . 

COMPONENT_SRCDIRS += ${EXTRA_LIBS}/wolfssl/src