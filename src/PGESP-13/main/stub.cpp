#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"

extern "C" int wolfssl_main();

extern "C" void testTask(void*) 
{
    // with some guidance from https://community.blynk.cc/t/esp8266-light-sleep/13584/9
    wifi_set_sleep_type(LIGHT_SLEEP_T);

    for(;;)
    {
        wolfssl_main();
        
        taskYIELD();
    }
}