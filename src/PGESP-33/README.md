# PGESP-33: Power modes

## Requirements

esp-idf v5.0

# References

1. https://docs.espressif.com/projects/esp-idf/en/v5.0/esp32/api-reference/peripherals/gptimer.html
2. https://docs.espressif.com/projects/esp-idf/en/latest/esp32/hw-reference/esp32/get-started-wrover-kit.html
  1. https://www.espressif.com/sites/default/files/documentation/esp32-wrover-e_esp32-wrover-ie_datasheet_en.pdf