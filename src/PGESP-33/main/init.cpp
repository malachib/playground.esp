#include <embr/platform/esp-idf/gptimer.h>

#include "main.h"

using namespace embr;

// Code copy/pasted and adapted from [1]

esp_idf::v5::Timer timer;

estd::freertos::layer1::queue<queue_element_t, 10> queue;

static bool timer_on_alarm_cb(gptimer_handle_t _timer, const gptimer_alarm_event_data_t *edata, void *user_ctx)
{
    queue_element_t qe{edata->count_value};

    BaseType_t high_task_awoken = pdFALSE;

    queue.send_from_isr(qe, &high_task_awoken);

    return high_task_awoken == pdTRUE;
}

void initialize()
{
    gptimer_config_t timer_config = {
        .clk_src = GPTIMER_CLK_SRC_APB,

        // this one is recommended, but a bit murky - docs indicate no PM lock is needed here,
        // presumably because maybe XTAL keeps running regardless of power mode?

        // Most variants of ESP32 have XTAL gptimer support, but the root one doesn't.  
        // It appears the ones I'm testing with [2.1] are just that.  Surprising, since
        // XTAL support in general is present for this chip.
        
        //.clk_src = GPTIMER_CLK_SRC_XTAL,

        .direction = GPTIMER_COUNT_UP,
        .resolution_hz = 1 * 1000 * 1000, // 1MHz, 1 tick = 1us
        .flags = 0
    };

    ESP_ERROR_CHECK(timer.init(&timer_config));

    gptimer_alarm_config_t alarm_config =
    {
        .alarm_count = 1000000, // period = 1s @resolution 1MHz
        .reload_count = 0, // counter will reload with 0 on alarm event
        .flags
        {
            .auto_reload_on_alarm = true
        }
    };

    ESP_ERROR_CHECK(timer.set_alarm_action(&alarm_config));

    gptimer_event_callbacks_t cbs = {
        .on_alarm = timer_on_alarm_cb, // register user callback
    };
    ESP_ERROR_CHECK(timer.register_event_callbacks(&cbs));

    // NOTE: I believe register event somehow auto enables this, but not sure
    ESP_ERROR_CHECK(timer.enable());

    ESP_ERROR_CHECK(timer.start());
}
