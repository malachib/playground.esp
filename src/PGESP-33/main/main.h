#pragma once

#include <estd/port/freertos/queue.h>


struct queue_element_t
{
    uint64_t event_count;
};

extern estd::freertos::layer1::queue<queue_element_t, 10> queue;
