#include <estd/thread.h>
#include <estd/iostream.h>

#include "esp_system.h"
#include "esp_log.h"

#include "main.h"

using namespace estd::chrono_literals;

void initialize();

extern "C" void app_main(void)
{
    static const char* TAG = "PGESP-33: app_main";

    ESP_LOGI(TAG, "Bringup");

    initialize();

    for(;;)
    {
        queue_element_t qe;

        if(queue.receive(&qe, 5s))
            ESP_LOGD(TAG, "counter=%llu", qe.event_count);
        else
            ESP_LOGD(TAG, "Timed out");
    }
}
