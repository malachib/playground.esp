# https://docs.espressif.com/projects/esp-idf/en/v5.0/esp32/api-guides/build-system.html
# indicates a raw add_library would work, but it does not
#add_library(main STATIC main.cpp)

idf_component_register(SRCS init.cpp main.cpp)
