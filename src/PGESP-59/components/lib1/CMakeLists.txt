idf_component_register(SRCS "lib1.cpp"
    INCLUDE_DIRS "include"
    REQUIRES eigen)

# Try
# https://docs.espressif.com/projects/esp-idf/en/v5.2.2/esp32/api-guides/build-system.html#cmake-component-properties
# to acquire particular component's source dir/include property