#include <iostream>

#include <esp_log.h>
#include <esp_heap_caps.h>

#include <esp-helper.h>

#include <string>
#include <queue>
#include <set>

#include "allocator.h"

static const char* TAG = "pgesp-45";

#define ENABLE_STRING 1
#define ENABLE_VECTOR 1
#define ENABLE_SET 1

using String = std::basic_string<char, std::char_traits<char>, caps_allocator<char, MALLOC_CAP_INTERNAL> >;
template <class T>
using Vector = std::vector<T, caps_allocator<T, MALLOC_CAP_DEFAULT> >;
using Queue = std::priority_queue<int, Vector<int> >;
template <class T>
using Set = std::set<T, std::less<>, caps_allocator<T, MALLOC_CAP_SPIRAM> >;

static void test()
{
#if ENABLE_STRING
    // Fun fact, gcc implementation keeps 15 bytes onhand for local/non dynamic
    // behavior.
    String string;

    string += "hi2u";
    string += " my friend - 0123456789";

    std::cout << "Hello: " << string << std::endl;
#endif
#if ENABLE_VECTOR
    Vector<int> vector;
#endif
#if ENABLE_SET
    Set<int> set;

    set.insert(23);
    set.insert(11);
    set.insert(30);
    set.insert(5);

    for(int v : set)
        std::cout << "v: " << v << std::endl;
#endif

    heap_caps_print_heap_info(MALLOC_CAP_DEFAULT);// | MALLOC_CAP_INTERNAL);
}

extern "C" void app_main()
{
    ESP_LOGI(TAG, "entry");

    heap_caps_print_heap_info(MALLOC_CAP_DEFAULT);

    test();

    heap_caps_print_heap_info(MALLOC_CAP_DEFAULT);
}