# Overview

Document v0.1

# 1. Goals

While doing DUKU-CAM project, abysmal SD performance was observed, on the order of < 100 KB/s.
This project aims to get answers as to why

# 2. Targets

## 2.1. Main App

### 2.1.1. Sections

Since there are so many configurations, we try to organize via sections.  
At this time we only evaluate write performance.
These are intended to be cross cutting concerns

#### 2.1.1.1. Open + close overhead

See how much extra time is taken doing open/close operations

#### 2.1.1.2. Memory Source

See how speed is affected by RAM, ROM, PSRAM source

#### 2.1.1.3. Adhering to 2k, 4k, 8k write sizes

#### 2.1.1.4. Different Sector sizes on same card

#### 2.1.1.5. Same sector sizes on different card

#### 2.1.1.6. Using VFS-wrapped FAT vs FatFS API vs direct sector writes

#### 2.1.1.7. Camera actively acquiring images

## 2.1.2. Todo

### 2.1.2.1. Adjust newlib buffer size

Try

```
if(setvbuf(file, NULL, _IOFBF, 4096) != 0) {
  //handle error
}
```

As recommended by [3.7].  This looks like it can also be done with Kconfig `FATFS_VFS_FSTAT_BLKSIZE`

### 2.1.2.2. Internal RAM FATFS buffers

If it holds true that SPI access + cam is really a slowdown, this could help.  Configure via
`FATFS_ALLOC_PREFER_EXTRAM`

# 3. Observations

## 3.1. Conclusions

## 3.2. Seeed

PSRAM must be configured at Quad/80Mhz, otherwise part is unhappy

## 3.3. Discussions

FatFS benchmarks for writes are 2 MB/s on a good day and 0.5 MB/s for 4k block size [3.1].  Someone who did direct
sector writes got 9 MB/s [3.1].  People generally surprised by slowness, and it is suggested to write on 2-bit size boundaries [3.2]

It does seem sector size indeed makes a big difference, perhaps in part because of FatFS's buffer treatment as indicated at [3.1]

It's also suggested not using fprintf but instead POSIX write bypasses some buffering and can speed things up
I casually tried that with DUKU project and it didn't help, but what do you bet the 2-bit size boundary matters
4k buffers are again recommended [3.3]

One person got 1.5MB/s write speed with 8k blocks [3.4]

A few years ago someone got similar (though better) numbers as me (they ranged from ~200KB-500KB/s) [3.5]
One guy asked about getting 15MB/s and was told that's gonna be hard [3.6]

## 3.4. Dror Gluska

This guy went the extra mile.  A really great breakdown at [3.7].  Interestingly he recommends enabled exFAT which doesn't seem configurable directly from Kconfig.  Looks like he may have authored [3.5] a preceding reddit post.

He also has extensive speed charts at [3.7.1]

His charts are remarkeable in that at 32k write size, performance plummets to ~300KB/s.  If we can, ask him why he thinks that is.
It seems on the surface to be a cache thing, but if these cards are rated for well over 1MB/s write speed, what's the deal?

# 4. Troubleshooting

# 5. Results

## 5.1. Detailed

## 5.2. Summary

Evaluated are Seeed ESP32S3 Sense, FreeNove ESP32S3

| Date    | Board       | Section  | 
| -----   | -------     | -------
| 06FEB24 | Seeed       |


# References

1. https://docs.espressif.com/projects/esp-idf/en/v5.1.2/esp32s3/api-guides/memory-types.html
    1. https://docs.espressif.com/projects/esp-idf/en/v5.1.2/esp32s3/api-guides/external-ram.html
2. https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-guides/build-system.html#embedding-binary-data
3. https://github.com/micropython/micropython/issues/3782
    1. https://esp32.com/viewtopic.php?t=1942
    2. https://esp32.com/viewtopic.php?t=26152
    3. https://esp32.com/viewtopic.php?t=5979
    4. https://github.com/espressif/arduino-esp32/issues/1611 
    5. https://www.reddit.com/r/esp32/comments/d71es9/a_breakdown_of_my_experience_trying_to_talk_to_an/
    6. https://www.reddit.com/r/esp32/comments/16pr4rd/sd_card_writting_speed/
    7. https://blog.drorgluska.com/2022/06/esp32-sd-card-optimization.html
        1. https://github.com/drorgl/esp32-sd-card-benchmarks