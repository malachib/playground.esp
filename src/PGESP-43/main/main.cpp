#include <algorithm>
#include <span>

#include <esp_log.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "init.h"

static const char* TAG = "PGESP-43";

extern const uint8_t lorem_start[]  asm("_binary_lorem_txt_start");
extern const uint8_t lorem_end[]    asm("_binary_lorem_txt_end");
// Bummer, can't quite get a constexpr out of this probably due to link time realities
const std::size_t sz = lorem_end - lorem_start;

std::span<const uint8_t> lorem(lorem_start, sz);
std::span<uint8_t> psram_lorem;
std::span<uint8_t> ram_lorem;

extern "C" void app_main()
{
    ESP_LOGI(TAG, "entry");

    init_sdmmc();

    auto buf = (uint8_t*)heap_caps_malloc(sz, MALLOC_CAP_SPIRAM);
    auto buf2 = (uint8_t*)malloc(sz);

    ESP_LOGI(TAG, "psram malloc=%p, ram malloc=%p", buf, buf2);

    std::copy(lorem_start, lorem_end, buf);
    std::copy(lorem_start, lorem_end, buf2);

    ESP_LOGI(TAG, "data=\"%.40s\"...sz=%d", buf, sz);

    psram_lorem = { buf, sz };
    ram_lorem = { buf2, sz };

    for(;;)
    {
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}