#pragma once

#include <span>

// NOTE: We're not talking about electronic batteries here, we're talking about a rigerous excercise
// "an extensive series, sequence, or range of things"

#include <esp_log.h>

#include <string.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include "esp_vfs_fat.h"
#include "sdmmc_cmd.h"
#include "driver/sdmmc_host.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "sd.h"

extern std::span<const uint8_t> rom_lorem;
extern std::span<uint8_t> psram_lorem;
extern std::span<uint8_t> ram_lorem;

void battery1();
void battery2();
void battery3();