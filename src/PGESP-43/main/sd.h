#pragma once

#define PIN_NUM_CLK     (gpio_num_t)CONFIG_IO_SDMMC_CLK
#define PIN_NUM_CMD     (gpio_num_t)CONFIG_IO_SDMMC_CMD
#define PIN_NUM_D0      (gpio_num_t)CONFIG_IO_SDMMC_D0

#define MOUNT_POINT "/sd"
#define TEST_FOLDER MOUNT_POINT "/pgesp-43"
