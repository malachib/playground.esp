# Overview

https://github.com/esp-rs/esp-idf-template

## Caveats

`export-esp.sh` is here for convenience, but targets `c131-debian` machine
pathing.  Looks like it may work for other installs for me
(`/home/malachi/.espressif`)