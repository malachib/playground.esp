#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/gpio.h>


const gpio_num_t gpio = GPIO_NUM_13;

void blinkenTask(void *pvParameters)
{
    int counter = 0;

    gpio_pad_select_gpio(gpio);
    gpio_set_direction(gpio, GPIO_MODE_OUTPUT);
    
    while(1) {
        gpio_set_level(gpio, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        gpio_set_level(gpio, 0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        printf("Hello World %d\r\n", counter++);
    }
}
//int main()
extern "C" int app_main()
{
    xTaskCreate(blinkenTask, "blinkenTask", 2048, NULL, 2, NULL);

    return 0;
}