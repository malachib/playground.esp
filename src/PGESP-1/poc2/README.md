# Hello World Example

Starts a FreeRTOS task to print "Hello World"

See the README.md file in the upper level 'examples' directory for more information about examples.

## Malachi extensions

Bringing in `estdlib`

Lifted Kconfig.projbuild from https://github.com/espressif/esp-idf/blob/master/examples/get-started/blink/main/Kconfig.projbuild

NOTE: Our special ESP_VER mechanism not yet active for CMakeList flavor