#include <estd/chrono.h>
#include <estd/thread.h>

#include "freertos/timers.h"
#include "driver/gpio.h"
#include "rom/gpio.h"
#include "esp_log.h"

constexpr gpio_num_t gpio_blink = (gpio_num_t) CONFIG_BLINK_GPIO;

extern "C" void blink_start(const char* const TAG)
{
    // 10AUG24 MB Back in ~2018, our own ESTD versioning tool was useful.
    // Now, Espressif has their own https://github.com/espressif/esp-idf/blob/v5.3/components/esp_common/include/esp_idf_version.h
    ESP_LOGI(TAG, "Startup: IDF_VER=%d.%d.%d",
             ESTD_IDF_VER_MAJOR,
             ESTD_IDF_VER_MINOR,
             ESTD_IDF_VER_PATCH);
    ESP_LOGI(TAG, "Startup: gpio=%d", gpio_blink);

    gpio_pad_select_gpio(gpio_blink);
    gpio_set_direction(gpio_blink, GPIO_MODE_OUTPUT);
}

#if ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5, 0, 0)
extern "C" void blink_timer(xTimerHandle pxTimer)
#else
extern "C" void blink_timer(TimerHandle_t pxTimer)
#endif
{
    static bool led_state = true;
    static const char* TAG = "blink_timer"; // TODO: Do nameof() if such a thing exists

    ESP_LOGD(TAG, "Event: led_state=%d", led_state);

    gpio_set_level(gpio_blink, led_state = !led_state);
}

extern "C" void blink_task(void*)
{
    constexpr estd::chrono::milliseconds sleep_interval(500);
    static const char* TAG = "blink_task"; // TODO: Do nameof() if such a thing exists

    blink_start(TAG);

    bool led_state = true;

    for(;;)
    {
        ESP_LOGD(TAG, "Loop: led_state=%d", led_state);

        gpio_set_level(gpio_blink, led_state = !led_state);

        // Not quite ready apparently
        //estd::this_thread::sleep_for(5s);
        estd::this_thread::sleep_for(sleep_interval);
    }
}