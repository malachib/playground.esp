/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <inttypes.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "esp_system.h"
#if ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5, 0, 0)
#include "esp_spi_flash.h"
#else
#include "esp_flash.h"
#include "esp_chip_info.h"
#include "spi_flash_mmap.h"
#endif

#ifdef CONFIG_BLINK_GPIO
void blink_start(const char* const TAG);
void blink_task(void*);
#if ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5, 0, 0)
void blink_timer(xTimerHandle pxTimer);
#else
void blink_timer(TimerHandle_t pxTimer);
#endif
#endif

void app_main()
{
    printf("Hello world!\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);

#ifdef CONFIG_BLINK_GPIO
#ifdef CONFIG_USE_TIMER
    blink_start("blink_timer");

    // guidance from https://mcuoneclipse.com/2018/05/27/tutorial-understanding-and-using-freertos-software-timers/
    TimerHandle_t timer = xTimerCreate("blinkenTimer", pdMS_TO_TICKS(500), pdTRUE, 100, blink_timer);
    xTimerStart(timer,0);
#else
    xTaskCreate(blink_task, "blinkenTask", 2048, NULL, 2, NULL);
#endif
#endif

// https://github.com/espressif/esp-idf/issues/9382
#if ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5, 0, 0)
    uint32_t flash_size = spi_flash_get_chip_size() / (1024 * 1024);
#else
    uint32_t flash_size = 0;
    esp_flash_get_size(NULL, &flash_size);
#endif

    printf("%" PRIu32 "MB %s flash\n",
        flash_size,
        (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

    for (int i = 10; i >= 0; i--) {
        printf("Restarting in %d seconds...\n", i);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    printf("Restarting now.\n");
    fflush(stdout);
    esp_restart();
}
