# Overview

# 1. Design Goals

## 1.1. poc1

Using lmic lib

## 1.2. poc2

Using sx127x lib [2]
Kconfig/hardcoding of address, if applicable and useful, is OK

### 1.2.1. Phase 1

First, try discrete examples [2.1] and [2.2]

### 1.2.2. Phase 2

Next, try combining both into our own app setting tx or rx mode via Kconfig
or perhaps a button press switch roles.

### 1.2.3. Phase 3

Some kind of automated ping/pong.  Hopefully arbitration can be avoided

### 1.2.4. Phase 4

Security is attempted.  Perhaps FSK?

## 1.3. poc3

Using RadioLib [3]

# 2. Infrastructure

## 2.1. LoPy4

If memory servies I have two LoPy4 [4] laying around, which once we blast python off it, may be a decent starting place.

# 3. Opinions & Observations

## 3.1. sx127x library

Don't like the flimsy tagging/branching here on its github repo

Was expecting out of the gate for example to be troublesome.  So far however
looking OK, compiled [2.1] [2.2] without incident (Section 1.2.1)

Buildout of Section 1.2.2 is underway and so far so good.

The fact that I hit no weird, annoying errors so far is actually a huge vote of
confidence.

Don't like that the SPI pins are hardcoded in examples, but we'll see how much of an issue that really is.

## 3.2. RadioLib

I'm prejudice against this lib [3] due to its Arduino connections.  Prudence tells us that if Section 1.2. testing hits any major obstacles, we ought to look at RadioLib

# References

1. https://mynewt.apache.org/
2. https://components.espressif.com/components/dernasherbrezon/sx127x
    1. https://github.com/dernasherbrezon/sx127x/tree/main/examples/transmit_lora
    2. https://github.com/dernasherbrezon/sx127x/tree/main/examples/receive_lora 
3. https://components.espressif.com/components/jgromes/radiolib
4. https://docs.pycom.io/datasheets/development/lopy4/
