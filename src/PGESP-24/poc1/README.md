Requires esp-idf 3.3 or higher

# lmic

https://github.com/TobleMiner/lmic-esp-idf

leans heavily on `extern` linkages

# original (non esp adapted) LoRaWAN library:

https://github.com/lmic-lib/lmic

## ping example

https://github.com/lmic-lib/lmic/blob/test/examples/ping/main.c

# Targets LoPy:

## Flashing LoPy4

Be careful, esptool doesn't always detect the presence of LoPy.  Use
`-p` flag if so

Also, as usual for 'raw' ESP32, flashing for this device is as follows:

BOOT pin = P2 / G23 (pin 23) / GPIO0

1.  Attach GND to BOOT pin
2.  Reset or power on device
3.  Flash
4.  Detach GND from BOOT pin
5.  Reset

## Pin Mapping

| pin| gpio   | desc
| ---| -------| ----
| 34 | GPIO5  | CLK
| 16 | GPIO27 | MOSI
| 38 | GPIO19 | MISO
| 36 | GPIO23 | (LoRa Interrupt) / Multiplexed DIO
| 27 | GPIO18 | LoPy4 SS/NSS (LoRa Select) *or* Lopy1 RST (reset)
| ?? | GPIO17 | LoPy1 SS/NSS (LoRa Select)
| ?? | GPIO21 | WiFi antenna select

references:

https://pycom.github.io/pydocs/datasheets/development/lopy4.html
https://pycom.github.io/pydocs/.gitbook/assets/specsheets/Pycom_002_Specsheets_LoPy4_v2.pdf
https://forum.pycom.io/topic/3403/wiring-of-dio-pins-of-lora-sx127x-chip-to-esp32/5
https://www.thethingsnetwork.org/labs/story/program-your-lopy-from-the-arduino-ide-using-lmic
https://github.com/espressif/arduino-esp32/blob/master/variants/lopy4/pins_arduino.h

Note that DIO1, DIO2, DIO3 all seems to be multiplexed through GPIO23
