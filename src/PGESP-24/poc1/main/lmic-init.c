#include "main.h"

// Using reference
// https://www.thethingsnetwork.org/labs/story/program-your-lopy-from-the-arduino-ide-using-lmic
// Note that that looks like it's for LoPy, not LoPy4

// guidance from 
// https://github.com/lmic-lib/lmic/blob/1202c7b46dfca1f1d2317aa8a3410382a2c44769/target/arduino/examples/raw/raw.ino


// auto picked up by lmic lib (it declares an ext waiting for this)
const lmic_pinmap lmic_pins = {
    .spi = { /* MISO */ 19, /* MOSI */ 27, /* SCK */ 5 },
    .rxtx = LMIC_UNUSED_PIN,
#ifdef PGESP_LOPY1
    .nss = 17,
    .rst = 18,
#else
    .nss = 18,
    .rst = LMIC_UNUSED_PIN,
#endif
    .dio = {23, 23, 23} //workaround to use 1 pin for all 3 radio dio pins
};

static osjob_t txjob;

void app_lmic_init()
{
    os_init();

    // Maximum TX power 
    LMIC.txpow = 27;
    LMIC.datarate = DR_SF9;
    // This sets CR 4/5, BW125 (except for DR_SF7B, which uses BW250)
    LMIC.rps = updr2rps(LMIC.datarate);

    // setup initial job
    os_setCallback(&txjob, tx_func);
}