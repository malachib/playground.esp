/**
 * @file
 * Adaptation of
 * https://github.com/matthijskooijman/arduino-lmic/blob/master/examples/raw/raw.ino
 */
#include "main.h"

#include <random>

#include <estd/chrono.h>
//#include <estd/thread.h>
#include <thread>

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

// How often to send a packet. Note that this sketch bypasses the normal
// LMIC duty cycle limiting, so when you change anything in this sketch
// (payload length, frequency, spreading factor), be sure to check if
// this interval should not also be increased.
// See this spreadsheet for an easy airtime and duty cycle calculator:
// https://docs.google.com/spreadsheets/d/1voGAtQAjC1qBmaVuP1ApNKs1ekgUjavHuVQIXyYSvNc 
#define TX_INTERVAL 2000

std::random_device r;
std::default_random_engine gen(r());
std::uniform_int_distribution<int> uniform_dist(0, 500);

#define random(x) (uniform_dist(gen))
// rather do our ESTD one, not sure if the inbuilt std::this_thread plays nice with RTOS or not
//estd::this_thread::sleep_for(estd::chrono::microseconds(1));
//estd::chrono::esp_clock::now();
#define delay(x) std::this_thread::sleep_for(std::chrono::microseconds(x))

osjob_t txjob;
osjob_t timeoutjob;

static void tx(const char* str, osjobcb_t func)
{
    static const char* TAG = "tx";

    os_radio(RADIO_RST); // Stop RX first
    delay(1); // Wait a bit, without this os_radio below asserts, apparently because the state hasn't changed yet
    LMIC.dataLen = 0;
    while (*str)
        LMIC.frame[LMIC.dataLen++] = *str++;

    LMIC.osjob.func = func;
    os_radio(RADIO_TX);

    ESP_LOGI(TAG, "TX");
}

// Enable rx mode and call func when a packet is received
static void rx(osjobcb_t func)
{
    static const char* TAG = "rx";

    LMIC.osjob.func = func;
    LMIC.rxtime = os_getTime(); // RX _now_
    // Enable "continuous" RX (e.g. without a timeout, still stops after
    // receiving a packet)
    os_radio(RADIO_RXON);

    ESP_LOGI(TAG, "RX");
}

static void rxtimeout_func(osjob_t *job) {
    //digitalWrite(LED_BUILTIN, LOW); // off
}

void rx_func (osjob_t* job)
{
    static const char* TAG = "rx_func";
  // Blink once to confirm reception and then keep the led on
  /*
  digitalWrite(LED_BUILTIN, LOW); // off
  delay(10);
  digitalWrite(LED_BUILTIN, HIGH); // on */

    // Timeout RX (i.e. update led status) after 3 periods without RX
    os_setTimedCallback(&timeoutjob, os_getTime() + ms2osticks(3*TX_INTERVAL), rxtimeout_func);

    // Reschedule TX so that it should not collide with the other side's
    // next TX
    os_setTimedCallback(&txjob, os_getTime() + ms2osticks(TX_INTERVAL/2), tx_func);

    ESP_LOGI(TAG, "Got %d bytes", LMIC.dataLen);
/*
  Serial.print("Got ");
  Serial.print(LMIC.dataLen);
  Serial.println(" bytes");
  Serial.write(LMIC.frame, LMIC.dataLen);
  Serial.println(); */

    // Restart RX
    rx(rx_func);
}

static void txdone_func (osjob_t* job) {
    rx(rx_func);
}

extern "C" void tx_func(osjob_t* job)
{
    const char* TAG = "tx_func";

    ESP_LOGI(TAG, "entry");

    tx("Hello, world!", txdone_func);

    // reschedule job every TX_INTERVAL (plus a bit of random to prevent
    // systematic collisions), unless packets are received, then rx_func
    // will reschedule at half this time.
    os_setTimedCallback(job, os_getTime() + ms2osticks(TX_INTERVAL + random(500)), tx_func);
}

void app_lmic_task(void*)
{
    for(;;)
    {
        os_runloop_once();
        vTaskDelay(1);
    }
}

void app_lmic_start()
{
    xTaskCreate(app_lmic_task, "lmic_loop", 2048, NULL, 4, NULL);
}