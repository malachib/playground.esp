#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "lmic.h"

void rx_func(osjob_t* job);
void tx_func(osjob_t* job);

#ifdef __cplusplus
}
#endif