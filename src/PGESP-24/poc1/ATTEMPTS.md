# First attempt

Used alligator cables to bridge P2-GND to force entry into bootloader mode.

Flash worked and log messages on startup read as:

    I (547) lmic: Starting IO initialization
    I (567) gpio: GPIO[23]| InputEn: 1| OutputEn: 0| OpenDrain: 0| Pullup: 0| Pulldown: 0| Intr:0 
    I (577) lmic: Starting SPI initialization
    I (597) lmic: Finished initalisation of timer
    I (597) gpio: GPIO[18]| InputEn: 0| OutputEn: 1| OpenDrain: 0| Pullup: 0| Pulldown: 0| Intr:0 
    I (617) gpio: GPIO[18]| InputEn: 1| OutputEn: 0| OpenDrain: 0| Pullup: 0| Pulldown: 0| Intr:0 
    I (717) lmic: Done waiting until
    E (5717) task_wdt: Task watchdog got triggered. The following tasks did not reset the watchdog in time:
    E (5717) task_wdt:  - IDLE0 (CPU 0)
    E (5717) task_wdt: Tasks currently running:
    E (5717) task_wdt: CPU 0: main
    E (5717) task_wdt: CPU 1: IDLE1

# Second attempt

Noting on 
https://forum.pycom.io/topic/3403/wiring-of-dio-pins-of-lora-sx127x-chip-to-esp32/5
That the RST/NSS pin discrepency is due to LoPy4 vs LoPy1, and so I've switched to the
proper (hopefully) NSS pin 

Did a successful reflash, but now the board isn't recognized by the OS.  Switched USB ports, that helped.  Observing logs:

    I (   ) lmic: Starting IO initialization
    E (393) gpio: GPIO_PIN mask error 
    I (403) gpio: GPIO[23]| InputEn: 1| OutputEn: 0| OpenDrain: 0| Pullup: 0| Pulldown: 0| Intr:0 
    I (403) lmic: Finished IO initialization
    I (413) lmic: Starting SPI initialization
    I (413) lmic: Finished SPI initialization
    I (423) lmic: Starting initialisation of timer
    I (423) lmic: Finished initalisation of timer
    I (433) lmic: Wait until
    I (433) lmic: Done waiting until
    I (443) lmic: Wait until
    I (533) lmic: Done waiting until
    E (533) lmic: LMIC HAL failed (../components/lmic-esp-idf/src/lmic/radio.c:689)
    E (5533) task_wdt: Task watchdog got triggered. The following tasks did not reset the watchdog in time:
    E (5533) task_wdt:  - IDLE0 (CPU 0)
    E (5533) task_wdt: Tasks currently running:
    E (5533) task_wdt: CPU 0: main
    E (5533) task_wdt: CPU 1: IDLE1

That radio.c:689 is in `void radio_init ()`:

        // some sanity checks, e.g., read version number
        u1_t v = readReg(RegVersion);
    #ifdef CFG_sx1276_radio
        ASSERT(v == 0x12 );

Tried setting also dio to 23, UNUSED, UNUSED but that made no difference

`GPIO_PIN mask error` is generated actually from esp-idf itself

# Third Attempt

*Might* be working.  Logs are as follows:

    I (414) lmic: Starting IO initialization
    I (424) gpio: GPIO[18]| InputEn: 0| OutputEn: 1| OpenDrain: 0| Pullup: 0| Pulldown: 0| Intr:0 
    I (434) gpio: GPIO[23]| InputEn: 1| OutputEn: 0| OpenDrain: 0| Pullup: 0| Pulldown: 0| Intr:0 
    I (444) lmic: Finished IO initialization
    I (444) lmic: Starting SPI initialization
    I (454) lmic: Finished SPI initialization
    I (454) lmic: Starting initialisation of timer
    I (454) lmic: Finished initalisation of timer
    I (464) lmic: Wait until
    I (464) lmic: Done waiting until
    I (474) lmic: Wait until
    I (554) lmic: Done waiting until
    RXMODE_RSSI
    14355: Scheduled job 0x3ffb3994, cb 0x400d2898 ASAP
    0x400d2898: tx_func at /media/usb-ssd/malachi_home/Projects/playground/playground.esp/src/PGESP-24/poc1/build/../main/lmic.cpp:85

This is after I patched `lmic-esp-idf` with the PR here https://github.com/TobleMiner/lmic-esp-idf/pull/4