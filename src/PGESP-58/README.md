# Overview

Document v0.1

# 1. Scope

# 2. Infrastructure

Only ESP32S3 and ESP32C6 are tested at this time

# 3. Observations & Opinions

## 3.1. lp_core_startup.c "undefined reference to 'main'"

I didn't glean this right away, but since ULP is a separate MPU of sorts it does
need its own main()

# References

1. https://docs.espressif.com/projects/esp-idf/en/v5.2.2/esp32/api-reference/system/ulp.html
    1. https://docs.espressif.com/projects/esp-idf/en/v5.2.2/esp32s3/api-reference/system/ulp-risc-v.html
    2. https://github.com/espressif/esp-idf/tree/v5.2.2/examples/system/ulp/lp_core/gpio