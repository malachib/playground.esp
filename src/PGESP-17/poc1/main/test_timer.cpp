#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"

#include <utility>
#include <type_traits>
#include <estd/chrono.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include <chrono>

extern "C" void testTask(void* p)
{
    auto start = estd::chrono::steady_clock::now();
    auto interval = estd::chrono::seconds(2);

    for(;;)
    {
        taskYIELD();

        auto now = estd::chrono::steady_clock::now();

        if(estd::chrono::duration_cast<estd::chrono::seconds>(now - start).count() > 2)
        //if((now - start) > interval) // won't work yet, but soon
        {
            printf("Got here: tick count=%d\n", now.time_since_epoch().count());
        }
    }
}