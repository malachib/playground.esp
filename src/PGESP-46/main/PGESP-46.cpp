#include <esp_heap_caps.h>
#include <esp_log.h>
#include <esp_system.h>

#include <entt/entity/mixin.hpp>
#include <entt/entity/registry.hpp>
#include <entt/entity/storage.hpp>

static const char* TAG = "PGESP-46";

extern "C" void app_main(void)
{
    //heap_caps_dump(MALLOC_CAP_INTERNAL);
    heap_caps_print_heap_info(MALLOC_CAP_INTERNAL);

    ESP_LOGI(TAG, "app_main: phase 1");

    entt::registry registry;
    const std::array entity{registry.create()};

    //heap_caps_dump(MALLOC_CAP_INTERNAL);
    heap_caps_print_heap_info(MALLOC_CAP_INTERNAL);
}
