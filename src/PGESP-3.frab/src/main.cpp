#include <frab/i2c.h>

#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

namespace frab = framework_abstraction;

#define SDA_PIN GPIO_NUM_5
#define SCL_PIN GPIO_NUM_4

frab::layer1::i2c<I2C_NUM_0, SDA_PIN, SCL_PIN> i2c;

static char tag[] = "i2cscanner";

void task_i2cscanner(void *ignore) {
        ESP_LOGD(tag, ">> i2cScanner");

        i2c.config(100000);

        printf("     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f\n");
        printf("00:         ");

        for (int i=3; i< 0x78; i++) 
        {
            /* this works, but I don't like it for this scenario
            auto tx = i2c.tx_begin_experimental();

            tx.addr(i);

            bool write_succeeded = tx.commit(); */

            bool write_succeeded = i2c.write((i << 1) | I2C_MASTER_WRITE);

            if (i%16 == 0) {
                    printf("\n%.2x:", i);
            }
            if (write_succeeded) {
                    printf(" %.2x", i);
            } else {
                    printf(" --");
            }
            //ESP_LOGD(tag, "i=%d, rc=%d (0x%x)", i, espRc, espRc);
        }
        printf("\n");
        vTaskDelete(NULL);
}


extern "C" void app_main()
{
    xTaskCreate(task_i2cscanner, "scanner", 2048, NULL, 2, NULL);
}