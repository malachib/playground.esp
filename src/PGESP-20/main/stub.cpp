#include <estd/port/identify_platform.h>

#if ESTD_IDF_VER >= ESTD_IDF_VER_2_0_0_644
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#else
#include "esp_misc.h"
#include "esp_sta.h"
#include "esp_system.h"
#endif

extern "C" {
    
#include "freertos/task.h"
#include "freertos/FreeRTOS.h"

#include <lwip/apps/mdns.h>

}


extern "C" void test_task(void*)
{
    for(;;) 
    {
        // FIX: Something about taskYIELD causes a crash on wemos d1 esp8266
        vTaskDelay(1);
        //taskYIELD();
    }
}

extern "C" void got_ip_event()
{
#if !LWIP_MDNS_RESPONDER
#warning No mDNS responder
#else
    mdns_resp_init();
    mdns_resp_add_netif(netif_default, "PGESP-20", 100);
#endif
}